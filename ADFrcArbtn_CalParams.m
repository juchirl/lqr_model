function [] = ADFrcArbtn_CalParams
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filename : ADFrcArbtn_CalParams
%
% Created : Sunday 09 April 2017 - 9:26:07 PM (Eastern Daylight Time)
%
% Updated on: Thursday 04 May 2017 - 1:23:06 PM (Eastern Daylight Time)
%
% Created By : Juchirl Park 
%
% Description : Adaptive Damping tuning variables reside here. The control
% gains are calculated based on Linear Quadratic Regulator and state space
% model of the vehicle with 7-degree of freedom. These gains can be
% displayed on the MATLAB command window or stored in the excel file user
% needs to be define. It have 4 input variable with no return variable.
% Usually, this program will be called by "Driver_Controls_Main".
%
% [] = ADFrcArbtn_CalParams;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Code Start here
ADFrcArbtn_EnableCntrls = Simulink.Parameter;
ADFrcArbtn_EnableCntrls.Value = 1;
ADFrcArbtn_EnableCntrls.Max = 1;
ADFrcArbtn_EnableCntrls.Min = 0;
ADFrcArbtn_EnableCntrls.Description = 'Description goes here';
ADFrcArbtn_EnableCntrls.Unit = 'N/A';
ADFrcArbtn_EnableCntrls.DataType = 'boolean';

assignin('base','ADFrcArbtn_EnableCntrls',ADFrcArbtn_EnableCntrls);

return

% 
%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ADFrcArbtn_CalParams.m

