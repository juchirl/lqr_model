function [RePower] = AbsorbedPowerCalc(data)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filename : AbsorbedPowerCalc.m
%
% Created : Tuesday 26 September 2017 - 8:09:05 PM (Eastern Daylight Time)
%
% Last-Updated : Tuesday 26 September 2017 - 8:09:10 PM (Eastern Daylight % Time)
%
% Version : 
%
% Created By : Jay Park 
%
% Description : Calculate the absorbed power from the acceleration in g. It takes two
% variables, time in seconds, and vertical acceleration in g and it returns the resulting
% absorbed power in watt.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Code Start here

%Load the signals.
%data = load(FileName);
t = data.time;
DrvStAz = data.signals.values;

dt = t(2) - t(1);
g2ftpss = 32.1740486;
DrvStAz = DrvStAz.*g2ftpss;

B = [ 0.00227184  0.01817472  0.06361152  0.12722304  0.1590288   0.12722304 ...
  0.06361152  0.01817472  0.00227184];
A = [  1.00000000e+00  -1.59056650e+00   2.08381330e+00  -1.53262556e+00 ...
   8.69440915e-01  -3.19175943e-01   8.20901316e-02  -1.22466702e-02 ...
   8.61368381e-04];

%[B,A] = butter(8,2*100*dt);
DrvStAz = filter(B,A,DrvStAz);

% Start the FFT and Power Calculation
L = length(t);
Fs = 1/dt;

f = Fs/2*linspace(0,1,floor(L/2));

Zabs = 2*abs(fft(DrvStAz)./L);          % Find Peak Value
%Zabs = abs(fft(DrvStAz)./L);             % Find Peak Value
Zrms = Zabs./sqrt(2);                   % Find RMS value from peak

[M,N] = size(Zrms);

power = zeros(length(f),N);
iMin = 1;%max(find(f < .5));              % Consider only affecting frequency
iMax = length(f)-1;%max(find(f > 100))-1;

for j = 1:N,
  
  for i = iMin:iMax,
    
    power(i+1,j) = power(i,j) + Coef_Calc(3,f(i))*Zrms(i,j)^2;
    
  end
  
end

StringsRMS = {'RMS for Driver Seat',...
							'RMS for Rear Faced Passenger Seat',...
							'RMS for Front Faced Passenger Seat'};

StringsPower = {'Absorbed Power for Driver Seat',...
							'Absorbed Power for Rear Faced Passenger Seat',...
							'Absorbed Power for Front Faced Passenger Seat'};
%figure(1);
%for j = 1:N,
%	subplot(3,1,j)
% 	stem(f,Zrms(1:length(f),j));
%	grid on
%	axis([0 10 0 max(Zrms(1:length(f),j))+0.5]);
%	title(StringsRMS(j));
%end
%xlabel('Frequency (Hz)');

figure(1);

for j = 1:N,
	subplot(N,1,j)
	plot(f,power(:,j));
	grid on
	axis([0 45 0 max(power(:,j))+0.5]);
end

subplot(N,1,1);
title('Absorbed power calculation (watts)');
subplot(N,1,N);
xlabel('Frequency (Hz)');

RePower = power(iMax,:);

%======================================================================

function [C] = Coef_Calc(Flag,freq),
  
Omega = 2*pi*freq;

switch Flag,
    
 case 1,                               % For x-axis
  
  K0 = 4.3532;
  K1 = 1.356;
  
  F1 = 1.0;
  F2 = 0.219106;
  F3 = -0.0185309*Omega^2 + 1;
  F4 = -0.00061893*Omega^2 + 0.219106;
  
 case 2,                                % For y-axis
  
  K0 = 4.353;
  K1 = 1.356;
  
  F1 = 0.24052124e-3*Omega^4 - 0.066974483e-2*Omega^2 + 1;
  F2 = 0.57384538e-5*Omega^4 - 0.50170413e-2*Omega^2 + 0.33092592;
  F3 = -0.14979958e-5*Omega^6 + 0.0010088882*Omega^4 - 0.10108617^Omega^2 + 1;
  F4 = -0.1713749e-7*Omega^6 + 0.53137351e-4*Omega^4 - 0.011096507*Omega^2 + ...
           0.33092592;
 case 3,                              % For z-axis
  
  K0 = 4.3537;
  K1 = 1.356;
  
  F1 = -0.10245e-9*Omega^6 + 0.17583e-5*Omega^4 - 0.44601e-2*Omega^2 + 1;
  F2 = 0.12882e-7*Omega^4 - 0.93394e-4*Omega^2 + 0.10543;
  F3 = -0.45416e-9*Omega^6 + 0.37667e-5*Omega^4 - 0.56104e-2*Omega^2 + 1;
  F4 = -0.21179e-11*Omega^6 + 0.51728e-7*Omega^4 - 0.17947e-3*Omega^2 + ...
           0.10543;
 otherwise
  
  K0 = 4.3537;
  K1 = 1.356;
  disp('No correct type');
  
end

C = K1*K0*(F1*F4 - F2*F3)/(F3^2 + Omega^2*F4^2);
  

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FFT Magnitude calculation example
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [] = FFT_Mag_Ex
  
clc;
Fs=100;
t=[0:100]/Fs;
s1=5*sin(2*pi*5*t);
s2=10*sin(2*pi*10*t);
s=(s1+s2);
figure(1);
plot(t,s)
N=2560;
S=fft(s,N)*(2/Fs);
w=((0:((N/2)-1))/(N/2))*(Fs/2);
figure(2);
plot(w,abs([S(1:(N/2))']))
xlabel('FREQUENCY');
Ylabel('Magnitude');

%% 
%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% AbsorbedPowerCalc.m ends here
