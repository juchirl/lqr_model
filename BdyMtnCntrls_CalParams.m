function [] = BdyMtnCntrls_CalParams(Ka1,Ka2,Ka3,VehFlag)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filename : BdyMtnCntrls_CalParams
%
% Created : Sunday 09 April 2017 - 9:26:07 PM (Eastern Daylight Time)
%
% Updated on: Tuesday 02 May 2017 - 9:56:12 PM (Eastern Daylight Time)
%
% Created By : Juchirl Park 
%
% Description : Adaptive Damping tuning variables reside here. The control
% gains are calculated based on Linear Quadratic Regulator and state space
% model of the vehicle with 7-degree of freedom. These gains can be
% displayed on the MATLAB command window or stored in the excel file user
% needs to be define. It have 4 input variable with no return variable.
% Usually, this program will be called by "Driver_Controls_Main".
%
% [] = BdyMtnCntrls_CalParams(Ka1,Ka2,Ka3,VehFlag)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Code Start here
BdyMtnCntrls_MaxCntrlFrc = Simulink.Parameter;
BdyMtnCntrls_MaxCntrlFrc.Value = 70000;
BdyMtnCntrls_MaxCntrlFrc.Max = 100000;
BdyMtnCntrls_MaxCntrlFrc.Min = 0;
BdyMtnCntrls_MaxCntrlFrc.Description = 'Description goes here';
BdyMtnCntrls_MaxCntrlFrc.Unit = 'N';
BdyMtnCntrls_MaxCntrlFrc.DataType = 'double';

assignin('base','BdyMtnCntrls_MaxCntrlFrc',BdyMtnCntrls_MaxCntrlFrc);

BdyMtnCntrls_CmpDmpMaxLmtGnOffRd = Simulink.Parameter;
BdyMtnCntrls_CmpDmpMaxLmtGnOffRd.Value = 0.5;
BdyMtnCntrls_CmpDmpMaxLmtGnOffRd.Max = 10;
BdyMtnCntrls_CmpDmpMaxLmtGnOffRd.Min = 0;
BdyMtnCntrls_CmpDmpMaxLmtGnOffRd.Description = 'Description goes here';
BdyMtnCntrls_CmpDmpMaxLmtGnOffRd.Unit = 'N/A';
BdyMtnCntrls_CmpDmpMaxLmtGnOffRd.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpDmpMaxLmtGnOffRd',BdyMtnCntrls_CmpDmpMaxLmtGnOffRd);

BdyMtnCntrls_RebDmpMaxLmtGnOffRd = Simulink.Parameter;
BdyMtnCntrls_RebDmpMaxLmtGnOffRd.Value = 0.5;
BdyMtnCntrls_RebDmpMaxLmtGnOffRd.Max = 10;
BdyMtnCntrls_RebDmpMaxLmtGnOffRd.Min = 0;
BdyMtnCntrls_RebDmpMaxLmtGnOffRd.Description = 'Description goes here';
BdyMtnCntrls_RebDmpMaxLmtGnOffRd.Unit = 'N/A';
BdyMtnCntrls_RebDmpMaxLmtGnOffRd.DataType = 'double';

assignin('base','BdyMtnCntrls_RebDmpMaxLmtGnOffRd',BdyMtnCntrls_RebDmpMaxLmtGnOffRd);

BdyMtnCntrls_CmpDmpMaxLmtGnOnRd = Simulink.Parameter;
BdyMtnCntrls_CmpDmpMaxLmtGnOnRd.Value = 1.0;
BdyMtnCntrls_CmpDmpMaxLmtGnOnRd.Max = 10;
BdyMtnCntrls_CmpDmpMaxLmtGnOnRd.Min = 0;
BdyMtnCntrls_CmpDmpMaxLmtGnOnRd.Description = 'Description goes here';
BdyMtnCntrls_CmpDmpMaxLmtGnOnRd.Unit = 'N/A';
BdyMtnCntrls_CmpDmpMaxLmtGnOnRd.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpDmpMaxLmtGnOnRd',BdyMtnCntrls_CmpDmpMaxLmtGnOnRd);

BdyMtnCntrls_RebDmpMaxLmtGnOnRd = Simulink.Parameter;
BdyMtnCntrls_RebDmpMaxLmtGnOnRd.Value = 0.75;
BdyMtnCntrls_RebDmpMaxLmtGnOnRd.Max = 10;
BdyMtnCntrls_RebDmpMaxLmtGnOnRd.Min = 0;
BdyMtnCntrls_RebDmpMaxLmtGnOnRd.Description = 'Description goes here';
BdyMtnCntrls_RebDmpMaxLmtGnOnRd.Unit = 'N/A';
BdyMtnCntrls_RebDmpMaxLmtGnOnRd.DataType = 'double';

assignin('base','BdyMtnCntrls_RebDmpMaxLmtGnOnRd',BdyMtnCntrls_RebDmpMaxLmtGnOnRd);

BdyMtnCntrls_CmpDmpMaxLmtGnRst = Simulink.Parameter;
BdyMtnCntrls_CmpDmpMaxLmtGnRst.Value = 1.25;
BdyMtnCntrls_CmpDmpMaxLmtGnRst.Max = 10;
BdyMtnCntrls_CmpDmpMaxLmtGnRst.Min = 0;
BdyMtnCntrls_CmpDmpMaxLmtGnRst.Description = 'Description goes here';
BdyMtnCntrls_CmpDmpMaxLmtGnRst.Unit = 'N/A';
BdyMtnCntrls_CmpDmpMaxLmtGnRst.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpDmpMaxLmtGnRst',BdyMtnCntrls_CmpDmpMaxLmtGnRst);

BdyMtnCntrls_RebDmpMaxLmtGnRst = Simulink.Parameter;
BdyMtnCntrls_RebDmpMaxLmtGnRst.Value = 1.0;
BdyMtnCntrls_RebDmpMaxLmtGnRst.Max = 10;
BdyMtnCntrls_RebDmpMaxLmtGnRst.Min = 0;
BdyMtnCntrls_RebDmpMaxLmtGnRst.Description = 'Description goes here';
BdyMtnCntrls_RebDmpMaxLmtGnRst.Unit = 'N/A';
BdyMtnCntrls_RebDmpMaxLmtGnRst.DataType = 'double';

assignin('base','BdyMtnCntrls_RebDmpMaxLmtGnRst',BdyMtnCntrls_RebDmpMaxLmtGnRst);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% General Parameters Defined Here
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch VehFlag,
 case 'Curb',
  VehWghtMode = 1;
 case 'GVW',
  VehWghtMode = 2;
 otherwise
  VehWghtMode = 3;
end

BdyMtnCntrls_BdyCntrlFrcRcvryRt = Simulink.Parameter;
BdyMtnCntrls_BdyCntrlFrcRcvryRt.Value = 50;
BdyMtnCntrls_BdyCntrlFrcRcvryRt.Max = 255;
BdyMtnCntrls_BdyCntrlFrcRcvryRt.Min = 0;
BdyMtnCntrls_BdyCntrlFrcRcvryRt.Description = 'Description goes here';
BdyMtnCntrls_BdyCntrlFrcRcvryRt.Unit = 'N/A';
BdyMtnCntrls_BdyCntrlFrcRcvryRt.DataType = 'double';

assignin('base','BdyMtnCntrls_BdyCntrlFrcRcvryRt',BdyMtnCntrls_BdyCntrlFrcRcvryRt);

BdyMtnCntrls_HmnLngtdnlFrcRcvryRt = Simulink.Parameter;
BdyMtnCntrls_HmnLngtdnlFrcRcvryRt.Value = 50;
BdyMtnCntrls_HmnLngtdnlFrcRcvryRt.Max = 255;
BdyMtnCntrls_HmnLngtdnlFrcRcvryRt.Min = 0;
BdyMtnCntrls_HmnLngtdnlFrcRcvryRt.Description = 'Description goes here';
BdyMtnCntrls_HmnLngtdnlFrcRcvryRt.Unit = 'N/A';
BdyMtnCntrls_HmnLngtdnlFrcRcvryRt.DataType = 'double';

assignin('base','BdyMtnCntrls_HmnLngtdnlFrcRcvryRt',BdyMtnCntrls_HmnLngtdnlFrcRcvryRt);

BdyMtnCntrls_HmnLtrlFrcRcvryRt = Simulink.Parameter;
BdyMtnCntrls_HmnLtrlFrcRcvryRt.Value = 50;
BdyMtnCntrls_HmnLtrlFrcRcvryRt.Max = 255;
BdyMtnCntrls_HmnLtrlFrcRcvryRt.Min = 0;
BdyMtnCntrls_HmnLtrlFrcRcvryRt.Description = 'Description goes here';
BdyMtnCntrls_HmnLtrlFrcRcvryRt.Unit = 'N/A';
BdyMtnCntrls_HmnLtrlFrcRcvryRt.DataType = 'double';

assignin('base','BdyMtnCntrls_HmnLtrlFrcRcvryRt',BdyMtnCntrls_HmnLtrlFrcRcvryRt);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assign the control gains for the Curb Vehicle Weight Condition
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BdyMtnCntrls_UseAcclBsdCntrlFrc = Simulink.Parameter;
BdyMtnCntrls_UseAcclBsdCntrlFrc.Value = 0;
BdyMtnCntrls_UseAcclBsdCntrlFrc.Max = 1;
BdyMtnCntrls_UseAcclBsdCntrlFrc.Min = 0;
BdyMtnCntrls_UseAcclBsdCntrlFrc.Description = 'Description goes here';
BdyMtnCntrls_UseAcclBsdCntrlFrc.Unit = 'N/A';
BdyMtnCntrls_UseAcclBsdCntrlFrc.DataType = 'boolean';

assignin('base','BdyMtnCntrls_UseAcclBsdCntrlFrc',BdyMtnCntrls_UseAcclBsdCntrlFrc);


%BdyMtnCntrls_BdyAcclThrsCrb = Simulink.Parameter;
%BdyMtnCntrls_BdyAcclThrsCrb.Value = 0.05;
%BdyMtnCntrls_BdyAcclThrsCrb.Max = 5;
%BdyMtnCntrls_BdyAcclThrsCrb.Min = 0;
%BdyMtnCntrls_BdyAcclThrsCrb.Description = 'Description goes here';
%BdyMtnCntrls_BdyAcclThrsCrb.Unit = 'N/A';
%BdyMtnCntrls_BdyAcclThrsCrb.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_BdyAcclThrsCrb',BdyMtnCntrls_BdyAcclThrsCrb);

BdyMtnCntrls_PtchVelThrsCrb = Simulink.Parameter;
BdyMtnCntrls_PtchVelThrsCrb.Value = 2;
BdyMtnCntrls_PtchVelThrsCrb.Max = 5;
BdyMtnCntrls_PtchVelThrsCrb.Min = 0;
BdyMtnCntrls_PtchVelThrsCrb.Description = 'Description goes here';
BdyMtnCntrls_PtchVelThrsCrb.Unit = 'N/A';
BdyMtnCntrls_PtchVelThrsCrb.DataType = 'double';

assignin('base','BdyMtnCntrls_PtchVelThrsCrb',BdyMtnCntrls_PtchVelThrsCrb);

BdyMtnCntrls_RllVelThrsCrb = Simulink.Parameter;
BdyMtnCntrls_RllVelThrsCrb.Value = 2;
BdyMtnCntrls_RllVelThrsCrb.Max = 5;
BdyMtnCntrls_RllVelThrsCrb.Min = 0;
BdyMtnCntrls_RllVelThrsCrb.Description = 'Description goes here';
BdyMtnCntrls_RllVelThrsCrb.Unit = 'N/A';
BdyMtnCntrls_RllVelThrsCrb.DataType = 'double';

assignin('base','BdyMtnCntrls_RllVelThrsCrb',BdyMtnCntrls_RllVelThrsCrb);

BdyMtnCntrls_HvVelThrsCrb = Simulink.Parameter;
BdyMtnCntrls_HvVelThrsCrb.Value = 0.07;
BdyMtnCntrls_HvVelThrsCrb.Max = 5;
BdyMtnCntrls_HvVelThrsCrb.Min = 0;
BdyMtnCntrls_HvVelThrsCrb.Description = 'Description goes here';
BdyMtnCntrls_HvVelThrsCrb.Unit = 'N/A';
BdyMtnCntrls_HvVelThrsCrb.DataType = 'double';

assignin('base','BdyMtnCntrls_HvVelThrsCrb',BdyMtnCntrls_HvVelThrsCrb);

Res = 100;
MaxRatio = 1.5;
HvGain = round(abs(Ka1(:,1)')./Res).*Res;
PtchGain = round(abs(Ka1(:,2)')./Res).*Res;
RllGain = round(abs(Ka1(:,3)')./Res).*Res;

BdyMtnCntrls_HvVelGnCrb = Simulink.Parameter;
BdyMtnCntrls_HvVelGnCrb.Value = HvGain;
BdyMtnCntrls_HvVelGnCrb.Max = round((max(HvGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_HvVelGnCrb.Min = 0;
BdyMtnCntrls_HvVelGnCrb.Description = 'Description goes here';
BdyMtnCntrls_HvVelGnCrb.Unit = 'N/A';
BdyMtnCntrls_HvVelGnCrb.DataType = 'double';

assignin('base','BdyMtnCntrls_HvVelGnCrb',BdyMtnCntrls_HvVelGnCrb);

BdyMtnCntrls_PtchVelGnCrb = Simulink.Parameter;
BdyMtnCntrls_PtchVelGnCrb.Value = PtchGain;
BdyMtnCntrls_PtchVelGnCrb.Max = round((max(PtchGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_PtchVelGnCrb.Min = 0;
BdyMtnCntrls_PtchVelGnCrb.Description = 'Description goes here';
BdyMtnCntrls_PtchVelGnCrb.Unit = 'N/A';
BdyMtnCntrls_PtchVelGnCrb.DataType = 'double';

assignin('base','BdyMtnCntrls_PtchVelGnCrb',BdyMtnCntrls_PtchVelGnCrb);

BdyMtnCntrls_RllVelGnCrb = Simulink.Parameter;
BdyMtnCntrls_RllVelGnCrb.Value = RllGain;
BdyMtnCntrls_RllVelGnCrb.Max = round((max(RllGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_RllVelGnCrb.Min = 0;
BdyMtnCntrls_RllVelGnCrb.Description = 'Description goes here';
BdyMtnCntrls_RllVelGnCrb.Unit = 'N/A';
BdyMtnCntrls_RllVelGnCrb.DataType = 'double';

assignin('base','BdyMtnCntrls_RllVelGnCrb',BdyMtnCntrls_RllVelGnCrb);

BdyMtnCntrls_RebBdyFrcRtCrbOffRd = Simulink.Parameter;
BdyMtnCntrls_RebBdyFrcRtCrbOffRd.Value = 0.5;
BdyMtnCntrls_RebBdyFrcRtCrbOffRd.Max = 5;
BdyMtnCntrls_RebBdyFrcRtCrbOffRd.Min = 0;
BdyMtnCntrls_RebBdyFrcRtCrbOffRd.Description = 'Description goes here';
BdyMtnCntrls_RebBdyFrcRtCrbOffRd.Unit = 'N/A';
BdyMtnCntrls_RebBdyFrcRtCrbOffRd.DataType = 'double';

assignin('base','BdyMtnCntrls_RebBdyFrcRtCrbOffRd',BdyMtnCntrls_RebBdyFrcRtCrbOffRd);

BdyMtnCntrls_CmpBdyFrcRtCrbOffRd = Simulink.Parameter;
BdyMtnCntrls_CmpBdyFrcRtCrbOffRd.Value = 0.5;
BdyMtnCntrls_CmpBdyFrcRtCrbOffRd.Max = 5;
BdyMtnCntrls_CmpBdyFrcRtCrbOffRd.Min = 0;
BdyMtnCntrls_CmpBdyFrcRtCrbOffRd.Description = 'Description goes here';
BdyMtnCntrls_CmpBdyFrcRtCrbOffRd.Unit = 'N/A';
BdyMtnCntrls_CmpBdyFrcRtCrbOffRd.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpBdyFrcRtCrbOffRd',BdyMtnCntrls_CmpBdyFrcRtCrbOffRd);

BdyMtnCntrls_RebBdyFrcRtCrbOnRd = Simulink.Parameter;
BdyMtnCntrls_RebBdyFrcRtCrbOnRd.Value = 0.5;
BdyMtnCntrls_RebBdyFrcRtCrbOnRd.Max = 5;
BdyMtnCntrls_RebBdyFrcRtCrbOnRd.Min = 0;
BdyMtnCntrls_RebBdyFrcRtCrbOnRd.Description = 'Description goes here';
BdyMtnCntrls_RebBdyFrcRtCrbOnRd.Unit = 'N/A';
BdyMtnCntrls_RebBdyFrcRtCrbOnRd.DataType = 'double';

assignin('base','BdyMtnCntrls_RebBdyFrcRtCrbOnRd',BdyMtnCntrls_RebBdyFrcRtCrbOnRd);

BdyMtnCntrls_CmpBdyFrcRtCrbOnRd = Simulink.Parameter;
BdyMtnCntrls_CmpBdyFrcRtCrbOnRd.Value = 0.5;
BdyMtnCntrls_CmpBdyFrcRtCrbOnRd.Max = 5;
BdyMtnCntrls_CmpBdyFrcRtCrbOnRd.Min = 0;
BdyMtnCntrls_CmpBdyFrcRtCrbOnRd.Description = 'Description goes here';
BdyMtnCntrls_CmpBdyFrcRtCrbOnRd.Unit = 'N/A';
BdyMtnCntrls_CmpBdyFrcRtCrbOnRd.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpBdyFrcRtCrbOnRd',BdyMtnCntrls_CmpBdyFrcRtCrbOnRd);

BdyMtnCntrls_RebBdyFrcRtCrbRst = Simulink.Parameter;
BdyMtnCntrls_RebBdyFrcRtCrbRst.Value = 0.5;
BdyMtnCntrls_RebBdyFrcRtCrbRst.Max = 5;
BdyMtnCntrls_RebBdyFrcRtCrbRst.Min = 0;
BdyMtnCntrls_RebBdyFrcRtCrbRst.Description = 'Description goes here';
BdyMtnCntrls_RebBdyFrcRtCrbRst.Unit = 'N/A';
BdyMtnCntrls_RebBdyFrcRtCrbRst.DataType = 'double';

assignin('base','BdyMtnCntrls_RebBdyFrcRtCrbRst',BdyMtnCntrls_RebBdyFrcRtCrbRst);

BdyMtnCntrls_CmpBdyFrcRtCrbRst = Simulink.Parameter;
BdyMtnCntrls_CmpBdyFrcRtCrbRst.Value = 0.5;
BdyMtnCntrls_CmpBdyFrcRtCrbRst.Max = 5;
BdyMtnCntrls_CmpBdyFrcRtCrbRst.Min = 0;
BdyMtnCntrls_CmpBdyFrcRtCrbRst.Description = 'Description goes here';
BdyMtnCntrls_CmpBdyFrcRtCrbRst.Unit = 'N/A';
BdyMtnCntrls_CmpBdyFrcRtCrbRst.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpBdyFrcRtCrbRst',BdyMtnCntrls_CmpBdyFrcRtCrbRst);

%BdyMtnCntrls_PtchLowSpdAmpThrsCrb = Simulink.Parameter;
%BdyMtnCntrls_PtchLowSpdAmpThrsCrb.Value = 15.5;
%BdyMtnCntrls_PtchLowSpdAmpThrsCrb.Max = 50;
%BdyMtnCntrls_PtchLowSpdAmpThrsCrb.Min = 0;
%BdyMtnCntrls_PtchLowSpdAmpThrsCrb.Description = 'Description goes here';
%BdyMtnCntrls_PtchLowSpdAmpThrsCrb.Unit = 'N/A';
%BdyMtnCntrls_PtchLowSpdAmpThrsCrb.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_PtchLowSpdAmpThrsCrb',BdyMtnCntrls_PtchLowSpdAmpThrsCrb);
%
%BdyMtnCntrls_RllLowSpdAmpThrsCrb = Simulink.Parameter;
%BdyMtnCntrls_RllLowSpdAmpThrsCrb.Value = 25.5;
%BdyMtnCntrls_RllLowSpdAmpThrsCrb.Max = 50;
%BdyMtnCntrls_RllLowSpdAmpThrsCrb.Min = 0;
%BdyMtnCntrls_RllLowSpdAmpThrsCrb.Description = 'Description goes here';
%BdyMtnCntrls_RllLowSpdAmpThrsCrb.Unit = 'N/A';
%BdyMtnCntrls_RllLowSpdAmpThrsCrb.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_RllLowSpdAmpThrsCrb',BdyMtnCntrls_RllLowSpdAmpThrsCrb);
%
%BdyMtnCntrls_HvLowSpdAmpThrsCrb = Simulink.Parameter;
%BdyMtnCntrls_HvLowSpdAmpThrsCrb.Value = 0.25;
%BdyMtnCntrls_HvLowSpdAmpThrsCrb.Max = 5;
%BdyMtnCntrls_HvLowSpdAmpThrsCrb.Min = 0;
%BdyMtnCntrls_HvLowSpdAmpThrsCrb.Description = 'Description goes here';
%BdyMtnCntrls_HvLowSpdAmpThrsCrb.Unit = 'N/A';
%BdyMtnCntrls_HvLowSpdAmpThrsCrb.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_HvLowSpdAmpThrsCrb',BdyMtnCntrls_HvLowSpdAmpThrsCrb);
%
%BdyMtnCntrls_PtchLowSpdAmpSlpCrb = Simulink.Parameter;
%BdyMtnCntrls_PtchLowSpdAmpSlpCrb.Value = 2.5;
%BdyMtnCntrls_PtchLowSpdAmpSlpCrb.Max = 50;
%BdyMtnCntrls_PtchLowSpdAmpSlpCrb.Min = 0;
%BdyMtnCntrls_PtchLowSpdAmpSlpCrb.Description = 'Description goes here';
%BdyMtnCntrls_PtchLowSpdAmpSlpCrb.Unit = 'N/A';
%BdyMtnCntrls_PtchLowSpdAmpSlpCrb.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_PtchLowSpdAmpSlpCrb',BdyMtnCntrls_PtchLowSpdAmpSlpCrb);
%
%BdyMtnCntrls_RllLowSpdAmpSlpCrb = Simulink.Parameter;
%BdyMtnCntrls_RllLowSpdAmpSlpCrb.Value = 2.5;
%BdyMtnCntrls_RllLowSpdAmpSlpCrb.Max = 50;
%BdyMtnCntrls_RllLowSpdAmpSlpCrb.Min = 0;
%BdyMtnCntrls_RllLowSpdAmpSlpCrb.Description = 'Description goes here';
%BdyMtnCntrls_RllLowSpdAmpSlpCrb.Unit = 'N/A';
%BdyMtnCntrls_RllLowSpdAmpSlpCrb.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_RllLowSpdAmpSlpCrb',BdyMtnCntrls_RllLowSpdAmpSlpCrb);
%
%BdyMtnCntrls_HvLowSpdAmpSlpCrb = Simulink.Parameter;
%BdyMtnCntrls_HvLowSpdAmpSlpCrb.Value = 3.0;
%BdyMtnCntrls_HvLowSpdAmpSlpCrb.Max = 5;
%BdyMtnCntrls_HvLowSpdAmpSlpCrb.Min = 0;
%BdyMtnCntrls_HvLowSpdAmpSlpCrb.Description = 'Description goes here';
%BdyMtnCntrls_HvLowSpdAmpSlpCrb.Unit = 'N/A';
%BdyMtnCntrls_HvLowSpdAmpSlpCrb.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_HvLowSpdAmpSlpCrb',BdyMtnCntrls_HvLowSpdAmpSlpCrb);
%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assign the control gains and define parameters for GVW Vehicle Weight Condition
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

BdyMtnCntrls_PtchVelThrsGVW = Simulink.Parameter;
BdyMtnCntrls_PtchVelThrsGVW.Value = 2;
BdyMtnCntrls_PtchVelThrsGVW.Max = 5;
BdyMtnCntrls_PtchVelThrsGVW.Min = 0;
BdyMtnCntrls_PtchVelThrsGVW.Description = 'Description goes here';
BdyMtnCntrls_PtchVelThrsGVW.Unit = 'N/A';
BdyMtnCntrls_PtchVelThrsGVW.DataType = 'double';

assignin('base','BdyMtnCntrls_PtchVelThrsGVW',BdyMtnCntrls_PtchVelThrsGVW);

BdyMtnCntrls_RllVelThrsGVW = Simulink.Parameter;
BdyMtnCntrls_RllVelThrsGVW.Value = 2;
BdyMtnCntrls_RllVelThrsGVW.Max = 5;
BdyMtnCntrls_RllVelThrsGVW.Min = 0;
BdyMtnCntrls_RllVelThrsGVW.Description = 'Description goes here';
BdyMtnCntrls_RllVelThrsGVW.Unit = 'N/A';
BdyMtnCntrls_RllVelThrsGVW.DataType = 'double';

assignin('base','BdyMtnCntrls_RllVelThrsGVW',BdyMtnCntrls_RllVelThrsGVW);

BdyMtnCntrls_HvVelThrsGVW = Simulink.Parameter;
BdyMtnCntrls_HvVelThrsGVW.Value = 0.07;
BdyMtnCntrls_HvVelThrsGVW.Max = 5;
BdyMtnCntrls_HvVelThrsGVW.Min = 0;
BdyMtnCntrls_HvVelThrsGVW.Description = 'Description goes here';
BdyMtnCntrls_HvVelThrsGVW.Unit = 'N/A';
BdyMtnCntrls_HvVelThrsGVW.DataType = 'double';

assignin('base','BdyMtnCntrls_HvVelThrsGVW',BdyMtnCntrls_HvVelThrsGVW);

Res = 100;
MaxRatio = 1.5;
HvGain = round(abs(Ka2(:,1)')./Res).*Res;
PtchGain = round(abs(Ka2(:,2)')./Res).*Res;
RllGain = round(abs(Ka2(:,3)')./Res).*Res;

BdyMtnCntrls_HvVelGnGVW = Simulink.Parameter;
BdyMtnCntrls_HvVelGnGVW.Value = HvGain;
BdyMtnCntrls_HvVelGnGVW.Max = round((max(HvGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_HvVelGnGVW.Min = 0;
BdyMtnCntrls_HvVelGnGVW.Description = 'Description goes here';
BdyMtnCntrls_HvVelGnGVW.Unit = 'N/A';
BdyMtnCntrls_HvVelGnGVW.DataType = 'double';

assignin('base','BdyMtnCntrls_HvVelGnGVW',BdyMtnCntrls_HvVelGnGVW);

BdyMtnCntrls_PtchVelGnGVW = Simulink.Parameter;
BdyMtnCntrls_PtchVelGnGVW.Value = PtchGain;
BdyMtnCntrls_PtchVelGnGVW.Max = round((max(PtchGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_PtchVelGnGVW.Min = 0;
BdyMtnCntrls_PtchVelGnGVW.Description = 'Description goes here';
BdyMtnCntrls_PtchVelGnGVW.Unit = 'N/A';
BdyMtnCntrls_PtchVelGnGVW.DataType = 'double';

assignin('base','BdyMtnCntrls_PtchVelGnGVW',BdyMtnCntrls_PtchVelGnGVW);

BdyMtnCntrls_RllVelGnGVW = Simulink.Parameter;
BdyMtnCntrls_RllVelGnGVW.Value = RllGain;
BdyMtnCntrls_RllVelGnGVW.Max = round((max(RllGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_RllVelGnGVW.Min = 0;
BdyMtnCntrls_RllVelGnGVW.Description = 'Description goes here';
BdyMtnCntrls_RllVelGnGVW.Unit = 'N/A';
BdyMtnCntrls_RllVelGnGVW.DataType = 'double';

assignin('base','BdyMtnCntrls_RllVelGnGVW',BdyMtnCntrls_RllVelGnGVW);

HvPosGain = round(abs(Ka2(:,12)')./Res).*Res;
PtchPosGain = round(abs(Ka2(:,13)')./Res).*Res;
RllPosGain = round(abs(Ka2(:,14)')./Res).*Res;

BdyMtnCntrls_HvPosGnGVW = Simulink.Parameter;
BdyMtnCntrls_HvPosGnGVW.Value = HvPosGain;
BdyMtnCntrls_HvPosGnGVW.Max = round((max(HvPosGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_HvPosGnGVW.Min = 0;
BdyMtnCntrls_HvPosGnGVW.Description = 'Description goes here';
BdyMtnCntrls_HvPosGnGVW.Unit = 'N/A';
BdyMtnCntrls_HvPosGnGVW.DataType = 'double';

assignin('base','BdyMtnCntrls_HvPosGnGVW',BdyMtnCntrls_HvPosGnGVW);

BdyMtnCntrls_PtchPosGnGVW = Simulink.Parameter;
BdyMtnCntrls_PtchPosGnGVW.Value = PtchPosGain;
BdyMtnCntrls_PtchPosGnGVW.Max = round((max(PtchPosGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_PtchPosGnGVW.Min = 0;
BdyMtnCntrls_PtchPosGnGVW.Description = 'Description goes here';
BdyMtnCntrls_PtchPosGnGVW.Unit = 'N/A';
BdyMtnCntrls_PtchPosGnGVW.DataType = 'double';

assignin('base','BdyMtnCntrls_PtchPosGnGVW',BdyMtnCntrls_PtchPosGnGVW);

BdyMtnCntrls_RllPosGnGVW = Simulink.Parameter;
BdyMtnCntrls_RllPosGnGVW.Value = RllPosGain;
BdyMtnCntrls_RllPosGnGVW.Max = round((max(RllPosGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_RllPosGnGVW.Min = 0;
BdyMtnCntrls_RllPosGnGVW.Description = 'Description goes here';
BdyMtnCntrls_RllPosGnGVW.Unit = 'N/A';
BdyMtnCntrls_RllPosGnGVW.DataType = 'double';

assignin('base','BdyMtnCntrls_RllPosGnGVW',BdyMtnCntrls_RllPosGnGVW);

BdyMtnCntrls_EnablePosCntrls = Simulink.Parameter;
BdyMtnCntrls_EnablePosCntrls.Value = 1;
BdyMtnCntrls_EnablePosCntrls.Max = 1;
BdyMtnCntrls_EnablePosCntrls.Min = 0;
BdyMtnCntrls_EnablePosCntrls.Description = 'Description goes here';
BdyMtnCntrls_EnablePosCntrls.Unit = 'N/A';
BdyMtnCntrls_EnablePosCntrls.DataType = 'boolean';

assignin('base','BdyMtnCntrls_EnablePosCntrls',BdyMtnCntrls_EnablePosCntrls);

Res = 10;

BdyMtnCntrls_RebBdyFrcRtGVWOffRd = Simulink.Parameter;
BdyMtnCntrls_RebBdyFrcRtGVWOffRd.Value = 0.7;
BdyMtnCntrls_RebBdyFrcRtGVWOffRd.Max = 5;
BdyMtnCntrls_RebBdyFrcRtGVWOffRd.Min = 0;
BdyMtnCntrls_RebBdyFrcRtGVWOffRd.Description = 'Description goes here';
BdyMtnCntrls_RebBdyFrcRtGVWOffRd.Unit = 'N/A';
BdyMtnCntrls_RebBdyFrcRtGVWOffRd.DataType = 'double';

assignin('base','BdyMtnCntrls_RebBdyFrcRtGVWOffRd',BdyMtnCntrls_RebBdyFrcRtGVWOffRd);

BdyMtnCntrls_CmpBdyFrcRtGVWOffRd = Simulink.Parameter;
BdyMtnCntrls_CmpBdyFrcRtGVWOffRd.Value = 0.7;
BdyMtnCntrls_CmpBdyFrcRtGVWOffRd.Max = 5;
BdyMtnCntrls_CmpBdyFrcRtGVWOffRd.Min = 0;
BdyMtnCntrls_CmpBdyFrcRtGVWOffRd.Description = 'Description goes here';
BdyMtnCntrls_CmpBdyFrcRtGVWOffRd.Unit = 'N/A';
BdyMtnCntrls_CmpBdyFrcRtGVWOffRd.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpBdyFrcRtGVWOffRd',BdyMtnCntrls_CmpBdyFrcRtGVWOffRd);

BdyMtnCntrls_RebBdyFrcRtGVWOnRd = Simulink.Parameter;
BdyMtnCntrls_RebBdyFrcRtGVWOnRd.Value = 1.0;
BdyMtnCntrls_RebBdyFrcRtGVWOnRd.Max = 5;
BdyMtnCntrls_RebBdyFrcRtGVWOnRd.Min = 0;
BdyMtnCntrls_RebBdyFrcRtGVWOnRd.Description = 'Description goes here';
BdyMtnCntrls_RebBdyFrcRtGVWOnRd.Unit = 'N/A';
BdyMtnCntrls_RebBdyFrcRtGVWOnRd.DataType = 'double';

assignin('base','BdyMtnCntrls_RebBdyFrcRtGVWOnRd',BdyMtnCntrls_RebBdyFrcRtGVWOnRd);

BdyMtnCntrls_CmpBdyFrcRtGVWOnRd = Simulink.Parameter;
BdyMtnCntrls_CmpBdyFrcRtGVWOnRd.Value = 1.0;
BdyMtnCntrls_CmpBdyFrcRtGVWOnRd.Max = 5;
BdyMtnCntrls_CmpBdyFrcRtGVWOnRd.Min = 0;
BdyMtnCntrls_CmpBdyFrcRtGVWOnRd.Description = 'Description goes here';
BdyMtnCntrls_CmpBdyFrcRtGVWOnRd.Unit = 'N/A';
BdyMtnCntrls_CmpBdyFrcRtGVWOnRd.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpBdyFrcRtGVWOnRd',BdyMtnCntrls_CmpBdyFrcRtGVWOnRd);

BdyMtnCntrls_RebBdyFrcRtGVWRst = Simulink.Parameter;
BdyMtnCntrls_RebBdyFrcRtGVWRst.Value = 1.5;
BdyMtnCntrls_RebBdyFrcRtGVWRst.Max = 5;
BdyMtnCntrls_RebBdyFrcRtGVWRst.Min = 0;
BdyMtnCntrls_RebBdyFrcRtGVWRst.Description = 'Description goes here';
BdyMtnCntrls_RebBdyFrcRtGVWRst.Unit = 'N/A';
BdyMtnCntrls_RebBdyFrcRtGVWRst.DataType = 'double';

assignin('base','BdyMtnCntrls_RebBdyFrcRtGVWRst',BdyMtnCntrls_RebBdyFrcRtGVWRst);

BdyMtnCntrls_CmpBdyFrcRtGVWRst = Simulink.Parameter;
BdyMtnCntrls_CmpBdyFrcRtGVWRst.Value = 1.5;
BdyMtnCntrls_CmpBdyFrcRtGVWRst.Max = 5;
BdyMtnCntrls_CmpBdyFrcRtGVWRst.Min = 0;
BdyMtnCntrls_CmpBdyFrcRtGVWRst.Description = 'Description goes here';
BdyMtnCntrls_CmpBdyFrcRtGVWRst.Unit = 'N/A';
BdyMtnCntrls_CmpBdyFrcRtGVWRst.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpBdyFrcRtGVWRst',BdyMtnCntrls_CmpBdyFrcRtGVWRst);

%BdyMtnCntrls_PtchLowSpdAmpThrsGVW = Simulink.Parameter;
%BdyMtnCntrls_PtchLowSpdAmpThrsGVW.Value = 35.5;
%BdyMtnCntrls_PtchLowSpdAmpThrsGVW.Max = 50;
%BdyMtnCntrls_PtchLowSpdAmpThrsGVW.Min = 0;
%BdyMtnCntrls_PtchLowSpdAmpThrsGVW.Description = 'Description goes here';
%BdyMtnCntrls_PtchLowSpdAmpThrsGVW.Unit = 'N/A';
%BdyMtnCntrls_PtchLowSpdAmpThrsGVW.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_PtchLowSpdAmpThrsGVW',BdyMtnCntrls_PtchLowSpdAmpThrsGVW);
%
%BdyMtnCntrls_RllLowSpdAmpThrsGVW = Simulink.Parameter;
%BdyMtnCntrls_RllLowSpdAmpThrsGVW.Value = 25.5;
%BdyMtnCntrls_RllLowSpdAmpThrsGVW.Max = 50;
%BdyMtnCntrls_RllLowSpdAmpThrsGVW.Min = 0;
%BdyMtnCntrls_RllLowSpdAmpThrsGVW.Description = 'Description goes here';
%BdyMtnCntrls_RllLowSpdAmpThrsGVW.Unit = 'N/A';
%BdyMtnCntrls_RllLowSpdAmpThrsGVW.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_RllLowSpdAmpThrsGVW',BdyMtnCntrls_RllLowSpdAmpThrsGVW);
%
%BdyMtnCntrls_HvLowSpdAmpThrsGVW = Simulink.Parameter;
%BdyMtnCntrls_HvLowSpdAmpThrsGVW.Value = 0.75;
%BdyMtnCntrls_HvLowSpdAmpThrsGVW.Max = 5;
%BdyMtnCntrls_HvLowSpdAmpThrsGVW.Min = 0;
%BdyMtnCntrls_HvLowSpdAmpThrsGVW.Description = 'Description goes here';
%BdyMtnCntrls_HvLowSpdAmpThrsGVW.Unit = 'N/A';
%BdyMtnCntrls_HvLowSpdAmpThrsGVW.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_HvLowSpdAmpThrsGVW',BdyMtnCntrls_HvLowSpdAmpThrsGVW);
%
%BdyMtnCntrls_PtchLowSpdAmpSlpGVW = Simulink.Parameter;
%BdyMtnCntrls_PtchLowSpdAmpSlpGVW.Value = 3.5;
%BdyMtnCntrls_PtchLowSpdAmpSlpGVW.Max = 50;
%BdyMtnCntrls_PtchLowSpdAmpSlpGVW.Min = 0;
%BdyMtnCntrls_PtchLowSpdAmpSlpGVW.Description = 'Description goes here';
%BdyMtnCntrls_PtchLowSpdAmpSlpGVW.Unit = 'N/A';
%BdyMtnCntrls_PtchLowSpdAmpSlpGVW.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_PtchLowSpdAmpSlpGVW',BdyMtnCntrls_PtchLowSpdAmpSlpGVW);
%
%BdyMtnCntrls_RllLowSpdAmpSlpGVW = Simulink.Parameter;
%BdyMtnCntrls_RllLowSpdAmpSlpGVW.Value = 3.5;
%BdyMtnCntrls_RllLowSpdAmpSlpGVW.Max = 50;
%BdyMtnCntrls_RllLowSpdAmpSlpGVW.Min = 0;
%BdyMtnCntrls_RllLowSpdAmpSlpGVW.Description = 'Description goes here';
%BdyMtnCntrls_RllLowSpdAmpSlpGVW.Unit = 'N/A';
%BdyMtnCntrls_RllLowSpdAmpSlpGVW.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_RllLowSpdAmpSlpGVW',BdyMtnCntrls_RllLowSpdAmpSlpGVW);
%
%BdyMtnCntrls_HvLowSpdAmpSlpGVW = Simulink.Parameter;
%BdyMtnCntrls_HvLowSpdAmpSlpGVW.Value = 4.0;
%BdyMtnCntrls_HvLowSpdAmpSlpGVW.Max = 5;
%BdyMtnCntrls_HvLowSpdAmpSlpGVW.Min = 0;
%BdyMtnCntrls_HvLowSpdAmpSlpGVW.Description = 'Description goes here';
%BdyMtnCntrls_HvLowSpdAmpSlpGVW.Unit = 'N/A';
%BdyMtnCntrls_HvLowSpdAmpSlpGVW.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_HvLowSpdAmpSlpGVW',BdyMtnCntrls_HvLowSpdAmpSlpGVW);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assign default gains
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

BdyMtnCntrls_BdyAcclThrsRst = Simulink.Parameter;
BdyMtnCntrls_BdyAcclThrsRst.Value = 0.05;
BdyMtnCntrls_BdyAcclThrsRst.Max = 5;
BdyMtnCntrls_BdyAcclThrsRst.Min = 0;
BdyMtnCntrls_BdyAcclThrsRst.Description = 'Description goes here';
BdyMtnCntrls_BdyAcclThrsRst.Unit = 'N/A';
BdyMtnCntrls_BdyAcclThrsRst.DataType = 'double';

assignin('base','BdyMtnCntrls_BdyAcclThrsRst',BdyMtnCntrls_BdyAcclThrsRst);

BdyMtnCntrls_PtchVelThrsRst = Simulink.Parameter;
BdyMtnCntrls_PtchVelThrsRst.Value = 2;
BdyMtnCntrls_PtchVelThrsRst.Max = 5;
BdyMtnCntrls_PtchVelThrsRst.Min = 0;
BdyMtnCntrls_PtchVelThrsRst.Description = 'Description goes here';
BdyMtnCntrls_PtchVelThrsRst.Unit = 'N/A';
BdyMtnCntrls_PtchVelThrsRst.DataType = 'double';

assignin('base','BdyMtnCntrls_PtchVelThrsRst',BdyMtnCntrls_PtchVelThrsRst);

BdyMtnCntrls_RllVelThrsRst = Simulink.Parameter;
BdyMtnCntrls_RllVelThrsRst.Value = 2;
BdyMtnCntrls_RllVelThrsRst.Max = 5;
BdyMtnCntrls_RllVelThrsRst.Min = 0;
BdyMtnCntrls_RllVelThrsRst.Description = 'Description goes here';
BdyMtnCntrls_RllVelThrsRst.Unit = 'N/A';
BdyMtnCntrls_RllVelThrsRst.DataType = 'double';

assignin('base','BdyMtnCntrls_RllVelThrsRst',BdyMtnCntrls_RllVelThrsRst);

BdyMtnCntrls_HvVelThrsRst = Simulink.Parameter;
BdyMtnCntrls_HvVelThrsRst.Value = 0.07;
BdyMtnCntrls_HvVelThrsRst.Max = 5;
BdyMtnCntrls_HvVelThrsRst.Min = 0;
BdyMtnCntrls_HvVelThrsRst.Description = 'Description goes here';
BdyMtnCntrls_HvVelThrsRst.Unit = 'N/A';
BdyMtnCntrls_HvVelThrsRst.DataType = 'double';

assignin('base','BdyMtnCntrls_HvVelThrsRst',BdyMtnCntrls_HvVelThrsRst);

Res = 100;
MaxRatio = 1.5;
HvGain = round(abs(Ka2(:,1)')./Res).*Res;
PtchGain = round(abs(Ka2(:,2)')./Res).*Res;
RllGain = round(abs(Ka2(:,3)')./Res).*Res;

BdyMtnCntrls_HvVelGnRst = Simulink.Parameter;
BdyMtnCntrls_HvVelGnRst.Value = HvGain;
BdyMtnCntrls_HvVelGnRst.Max = round((max(HvGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_HvVelGnRst.Min = 0;
BdyMtnCntrls_HvVelGnRst.Description = 'Description goes here';
BdyMtnCntrls_HvVelGnRst.Unit = 'N/A';
BdyMtnCntrls_HvVelGnRst.DataType = 'double';

assignin('base','BdyMtnCntrls_HvVelGnRst',BdyMtnCntrls_HvVelGnRst);

BdyMtnCntrls_PtchVelGnRst = Simulink.Parameter;
BdyMtnCntrls_PtchVelGnRst.Value = PtchGain;
BdyMtnCntrls_PtchVelGnRst.Max = round((max(PtchGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_PtchVelGnRst.Min = 0;
BdyMtnCntrls_PtchVelGnRst.Description = 'Description goes here';
BdyMtnCntrls_PtchVelGnRst.Unit = 'N/A';
BdyMtnCntrls_PtchVelGnRst.DataType = 'double';

assignin('base','BdyMtnCntrls_PtchVelGnRst',BdyMtnCntrls_PtchVelGnRst);

BdyMtnCntrls_RllVelGnRst = Simulink.Parameter;
BdyMtnCntrls_RllVelGnRst.Value = RllGain;
BdyMtnCntrls_RllVelGnRst.Max = round((max(RllGain)*MaxRatio)./Res).*Res;
BdyMtnCntrls_RllVelGnRst.Min = 0;
BdyMtnCntrls_RllVelGnRst.Description = 'Description goes here';
BdyMtnCntrls_RllVelGnRst.Unit = 'N/A';
BdyMtnCntrls_RllVelGnRst.DataType = 'double';

assignin('base','BdyMtnCntrls_RllVelGnRst',BdyMtnCntrls_RllVelGnRst);

BdyMtnCntrls_RebBdyFrcRtRstOffRd = Simulink.Parameter;
BdyMtnCntrls_RebBdyFrcRtRstOffRd.Value = 0.7;
BdyMtnCntrls_RebBdyFrcRtRstOffRd.Max = 5;
BdyMtnCntrls_RebBdyFrcRtRstOffRd.Min = 0;
BdyMtnCntrls_RebBdyFrcRtRstOffRd.Description = 'Description goes here';
BdyMtnCntrls_RebBdyFrcRtRstOffRd.Unit = 'N/A';
BdyMtnCntrls_RebBdyFrcRtRstOffRd.DataType = 'double';

assignin('base','BdyMtnCntrls_RebBdyFrcRtRstOffRd',BdyMtnCntrls_RebBdyFrcRtRstOffRd);

BdyMtnCntrls_CmpBdyFrcRtRstOffRd = Simulink.Parameter;
BdyMtnCntrls_CmpBdyFrcRtRstOffRd.Value = 0.7;
BdyMtnCntrls_CmpBdyFrcRtRstOffRd.Max = 5;
BdyMtnCntrls_CmpBdyFrcRtRstOffRd.Min = 0;
BdyMtnCntrls_CmpBdyFrcRtRstOffRd.Description = 'Description goes here';
BdyMtnCntrls_CmpBdyFrcRtRstOffRd.Unit = 'N/A';
BdyMtnCntrls_CmpBdyFrcRtRstOffRd.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpBdyFrcRtRstOffRd',BdyMtnCntrls_CmpBdyFrcRtRstOffRd);

BdyMtnCntrls_RebBdyFrcRtRstOnRd = Simulink.Parameter;
BdyMtnCntrls_RebBdyFrcRtRstOnRd.Value = 2.5;
BdyMtnCntrls_RebBdyFrcRtRstOnRd.Max = 5;
BdyMtnCntrls_RebBdyFrcRtRstOnRd.Min = 0;
BdyMtnCntrls_RebBdyFrcRtRstOnRd.Description = 'Description goes here';
BdyMtnCntrls_RebBdyFrcRtRstOnRd.Unit = 'N/A';
BdyMtnCntrls_RebBdyFrcRtRstOnRd.DataType = 'double';

assignin('base','BdyMtnCntrls_RebBdyFrcRtRstOnRd',BdyMtnCntrls_RebBdyFrcRtRstOnRd);

BdyMtnCntrls_CmpBdyFrcRtRstOnRd = Simulink.Parameter;
BdyMtnCntrls_CmpBdyFrcRtRstOnRd.Value = 2.5;
BdyMtnCntrls_CmpBdyFrcRtRstOnRd.Max = 5;
BdyMtnCntrls_CmpBdyFrcRtRstOnRd.Min = 0;
BdyMtnCntrls_CmpBdyFrcRtRstOnRd.Description = 'Description goes here';
BdyMtnCntrls_CmpBdyFrcRtRstOnRd.Unit = 'N/A';
BdyMtnCntrls_CmpBdyFrcRtRstOnRd.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpBdyFrcRtRstOnRd',BdyMtnCntrls_CmpBdyFrcRtRstOnRd);

BdyMtnCntrls_RebBdyFrcRtRstRst = Simulink.Parameter;
BdyMtnCntrls_RebBdyFrcRtRstRst.Value = 1.5;
BdyMtnCntrls_RebBdyFrcRtRstRst.Max = 5;
BdyMtnCntrls_RebBdyFrcRtRstRst.Min = 0;
BdyMtnCntrls_RebBdyFrcRtRstRst.Description = 'Description goes here';
BdyMtnCntrls_RebBdyFrcRtRstRst.Unit = 'N/A';
BdyMtnCntrls_RebBdyFrcRtRstRst.DataType = 'double';

assignin('base','BdyMtnCntrls_RebBdyFrcRtRstRst',BdyMtnCntrls_RebBdyFrcRtRstRst);

BdyMtnCntrls_CmpBdyFrcRtRstRst = Simulink.Parameter;
BdyMtnCntrls_CmpBdyFrcRtRstRst.Value = 1.5;
BdyMtnCntrls_CmpBdyFrcRtRstRst.Max = 5;
BdyMtnCntrls_CmpBdyFrcRtRstRst.Min = 0;
BdyMtnCntrls_CmpBdyFrcRtRstRst.Description = 'Description goes here';
BdyMtnCntrls_CmpBdyFrcRtRstRst.Unit = 'N/A';
BdyMtnCntrls_CmpBdyFrcRtRstRst.DataType = 'double';

assignin('base','BdyMtnCntrls_CmpBdyFrcRtRstRst',BdyMtnCntrls_CmpBdyFrcRtRstRst);

%BdyMtnCntrls_PtchLowSpdAmpThrsRst = Simulink.Parameter;
%BdyMtnCntrls_PtchLowSpdAmpThrsRst.Value = 35.5;
%BdyMtnCntrls_PtchLowSpdAmpThrsRst.Max = 50;
%BdyMtnCntrls_PtchLowSpdAmpThrsRst.Min = 0;
%BdyMtnCntrls_PtchLowSpdAmpThrsRst.Description = 'Description goes here';
%BdyMtnCntrls_PtchLowSpdAmpThrsRst.Unit = 'N/A';
%BdyMtnCntrls_PtchLowSpdAmpThrsRst.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_PtchLowSpdAmpThrsRst',BdyMtnCntrls_PtchLowSpdAmpThrsRst);
%
%BdyMtnCntrls_RllLowSpdAmpThrsRst = Simulink.Parameter;
%BdyMtnCntrls_RllLowSpdAmpThrsRst.Value = 25.5;
%BdyMtnCntrls_RllLowSpdAmpThrsRst.Max = 50;
%BdyMtnCntrls_RllLowSpdAmpThrsRst.Min = 0;
%BdyMtnCntrls_RllLowSpdAmpThrsRst.Description = 'Description goes here';
%BdyMtnCntrls_RllLowSpdAmpThrsRst.Unit = 'N/A';
%BdyMtnCntrls_RllLowSpdAmpThrsRst.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_RllLowSpdAmpThrsRst',BdyMtnCntrls_RllLowSpdAmpThrsRst);
%
%BdyMtnCntrls_HvLowSpdAmpThrsRst = Simulink.Parameter;
%BdyMtnCntrls_HvLowSpdAmpThrsRst.Value = 0.75;
%BdyMtnCntrls_HvLowSpdAmpThrsRst.Max = 5;
%BdyMtnCntrls_HvLowSpdAmpThrsRst.Min = 0;
%BdyMtnCntrls_HvLowSpdAmpThrsRst.Description = 'Description goes here';
%BdyMtnCntrls_HvLowSpdAmpThrsRst.Unit = 'N/A';
%BdyMtnCntrls_HvLowSpdAmpThrsRst.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_HvLowSpdAmpThrsRst',BdyMtnCntrls_HvLowSpdAmpThrsRst);
%
%BdyMtnCntrls_PtchLowSpdAmpSlpRst = Simulink.Parameter;
%BdyMtnCntrls_PtchLowSpdAmpSlpRst.Value = 3.5;
%BdyMtnCntrls_PtchLowSpdAmpSlpRst.Max = 50;
%BdyMtnCntrls_PtchLowSpdAmpSlpRst.Min = 0;
%BdyMtnCntrls_PtchLowSpdAmpSlpRst.Description = 'Description goes here';
%BdyMtnCntrls_PtchLowSpdAmpSlpRst.Unit = 'N/A';
%BdyMtnCntrls_PtchLowSpdAmpSlpRst.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_PtchLowSpdAmpSlpRst',BdyMtnCntrls_PtchLowSpdAmpSlpRst);
%
%BdyMtnCntrls_RllLowSpdAmpSlpRst = Simulink.Parameter;
%BdyMtnCntrls_RllLowSpdAmpSlpRst.Value = 3.5;
%BdyMtnCntrls_RllLowSpdAmpSlpRst.Max = 50;
%BdyMtnCntrls_RllLowSpdAmpSlpRst.Min = 0;
%BdyMtnCntrls_RllLowSpdAmpSlpRst.Description = 'Description goes here';
%BdyMtnCntrls_RllLowSpdAmpSlpRst.Unit = 'N/A';
%BdyMtnCntrls_RllLowSpdAmpSlpRst.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_RllLowSpdAmpSlpRst',BdyMtnCntrls_RllLowSpdAmpSlpRst);
%
%BdyMtnCntrls_HvLowSpdAmpSlpRst = Simulink.Parameter;
%BdyMtnCntrls_HvLowSpdAmpSlpRst.Value = 4.0;
%BdyMtnCntrls_HvLowSpdAmpSlpRst.Max = 5;
%BdyMtnCntrls_HvLowSpdAmpSlpRst.Min = 0;
%BdyMtnCntrls_HvLowSpdAmpSlpRst.Description = 'Description goes here';
%BdyMtnCntrls_HvLowSpdAmpSlpRst.Unit = 'N/A';
%BdyMtnCntrls_HvLowSpdAmpSlpRst.DataType = 'double';
%
%assignin('base','BdyMtnCntrls_HvLowSpdAmpSlpRst',BdyMtnCntrls_HvLowSpdAmpSlpRst);
%
%
return

% 
%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% BdyMtnCntrls_CalParams.m

