function [] = ChangeConNames(Model,OldPrefix,NewPrefix)


ConBlks = find_system(Model,'BlockType','Constant');

OldPrefix = ['^',OldPrefix];
NewPrefix = [NewPrefix];

for i = 1:length(ConBlks)
	
	ValueStr = get_param(ConBlks{i},'Value');
	NameStr = get_param(ConBlks{i},'Name');

	if ~isempty(regexp(ValueStr,OldPrefix)),
		ValueStr = regexprep(ValueStr,OldPrefix,NewPrefix);
		set_param(ConBlks{i},'Value',ValueStr);
	end

	if ~isempty(regexp(NameStr,OldPrefix)),
		NameStr = regexprep(NameStr,OldPrefix,NewPrefix);
		set_param(ConBlks{i},'Name',NameStr);
	end

end
