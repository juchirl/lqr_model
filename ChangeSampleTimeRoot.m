function [] = ChangeSampleTimeRoot(Model,OldPrefix,NewPrefix)


PortBlks = find_system(Model,'SearchDepth',1,'regexp','on','BlockType','port')

OldPrefix = ['^',OldPrefix];
NewPrefix = [NewPrefix];

for i = 1:length(PortBlks)
	
	ValueStr = get_param(PortBlks{i},'SampleTime');

	if ~isempty(regexp(ValueStr,OldPrefix)),
		ValueStr = regexprep(ValueStr,OldPrefix,NewPrefix);
		set_param(PortBlks{i},'SampleTime',ValueStr);
	end

end
