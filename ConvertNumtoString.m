function [ValStr] = ConvertNumtoString(ValNum)

ValStr = [];

Leng = length(ValNum);

if Leng <= 1,

	ValStr = num2str(ValNum);

else

	[M,N] = size(ValNum);

	if M < 2 || N < 2, %if matrix, skip,

		ValStr = '[';
		
		for i = 1:Leng-1,

			ValStr = [ValStr,num2str(ValNum(i)),','];

		end

		ValStr = [ValStr,num2str(ValNum(end)),']'];

	end

end

