function [] = DomainBdyInfo_CalParams()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filename : DomainBdyInfo_CalParams
%
% Created : Friday 14 April 2017 - 9:11:21 PM (Eastern Daylight Time)
%
% Updated on: Tuesday 02 May 2017 - 8:37:35 PM (Eastern Daylight Time)
%
% Modified by: Juchirl Park
%
% Created By : Juchirl Park 
%
% Description : Sensor characterization and domain parameters are calculated
% and upload into SIMULINK model directly. This file is mostly called by the
% main m-function file, "Driver_Controls_Main" with appropriate variables.
%
% [] = DomainBdyInfo_CalParams
%	
% Usage example:
%
% DomainBdyInfo_CalParams
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Code Start here

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Domain - suspension status
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Resolution = 0.001; %1 mili-
Max = 3.0;
Min = -3.0;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OnCenterCalc
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DomainBdyInfo_NewDfltRdHght = Simulink.Parameter;
DomainBdyInfo_NewDfltRdHght.Value = [192 182 196 181];
DomainBdyInfo_NewDfltRdHght.Max = 500;
DomainBdyInfo_NewDfltRdHght.Min = 0;
DomainBdyInfo_NewDfltRdHght.Description = 'Description goes here';
DomainBdyInfo_NewDfltRdHght.Unit = 'mm';
DomainBdyInfo_NewDfltRdHght.DataType = 'double';

assignin('base','DomainBdyInfo_NewDfltRdHght',DomainBdyInfo_NewDfltRdHght);

DomainBdyInfo_OnCntrVehSpdMax = Simulink.Parameter;
DomainBdyInfo_OnCntrVehSpdMax.Value = 70;
DomainBdyInfo_OnCntrVehSpdMax.Max = 100;
DomainBdyInfo_OnCntrVehSpdMax.Min = 0;
DomainBdyInfo_OnCntrVehSpdMax.Description = 'Description goes here';
DomainBdyInfo_OnCntrVehSpdMax.Unit = 'km/h';
DomainBdyInfo_OnCntrVehSpdMax.DataType = 'double';

assignin('base','DomainBdyInfo_OnCntrVehSpdMax',DomainBdyInfo_OnCntrVehSpdMax);

DomainBdyInfo_OnCntrVehSpdMin = Simulink.Parameter;
DomainBdyInfo_OnCntrVehSpdMin.Value = 5;
DomainBdyInfo_OnCntrVehSpdMin.Max = 100;
DomainBdyInfo_OnCntrVehSpdMin.Min = 0;
DomainBdyInfo_OnCntrVehSpdMin.Description = 'Description goes here';
DomainBdyInfo_OnCntrVehSpdMin.Unit = 'km/h';
DomainBdyInfo_OnCntrVehSpdMin.DataType = 'double';

assignin('base','DomainBdyInfo_OnCntrVehSpdMin',DomainBdyInfo_OnCntrVehSpdMin);

DomainBdyInfo_OnCntrYRThrs = Simulink.Parameter;
DomainBdyInfo_OnCntrYRThrs.Value = 0.05;
DomainBdyInfo_OnCntrYRThrs.Max = 10;
DomainBdyInfo_OnCntrYRThrs.Min = 0;
DomainBdyInfo_OnCntrYRThrs.Description = 'Description goes here';
DomainBdyInfo_OnCntrYRThrs.Unit = 'rad/s';
DomainBdyInfo_OnCntrYRThrs.DataType = 'double';

assignin('base','DomainBdyInfo_OnCntrYRThrs',DomainBdyInfo_OnCntrYRThrs);

DomainBdyInfo_OnCntrLngAcclThrs = Simulink.Parameter;
DomainBdyInfo_OnCntrLngAcclThrs.Value = 0.05;
DomainBdyInfo_OnCntrLngAcclThrs.Max = 1;
DomainBdyInfo_OnCntrLngAcclThrs.Min = 0;
DomainBdyInfo_OnCntrLngAcclThrs.Description = 'Description goes here';
DomainBdyInfo_OnCntrLngAcclThrs.Unit = 'g';
DomainBdyInfo_OnCntrLngAcclThrs.DataType = 'double';

assignin('base','DomainBdyInfo_OnCntrLngAcclThrs',DomainBdyInfo_OnCntrLngAcclThrs);

DomainBdyInfo_OnCntrLatAcclThrs = Simulink.Parameter;
DomainBdyInfo_OnCntrLatAcclThrs.Value = 0.05;
DomainBdyInfo_OnCntrLatAcclThrs.Max = 1;
DomainBdyInfo_OnCntrLatAcclThrs.Min = 0;
DomainBdyInfo_OnCntrLatAcclThrs.Description = 'Description goes here';
DomainBdyInfo_OnCntrLatAcclThrs.Unit = 'g';
DomainBdyInfo_OnCntrLatAcclThrs.DataType = 'double';

assignin('base','DomainBdyInfo_OnCntrLatAcclThrs',DomainBdyInfo_OnCntrLatAcclThrs);

DomainBdyInfo_OnCntrRelVelThrs = Simulink.Parameter;
DomainBdyInfo_OnCntrRelVelThrs.Value = 250;
DomainBdyInfo_OnCntrRelVelThrs.Max = 1000;
DomainBdyInfo_OnCntrRelVelThrs.Min = 0;
DomainBdyInfo_OnCntrRelVelThrs.Description = 'Description goes here';
DomainBdyInfo_OnCntrRelVelThrs.Unit = 'mm/s';
DomainBdyInfo_OnCntrRelVelThrs.DataType = 'double';

assignin('base','DomainBdyInfo_OnCntrRelVelThrs',DomainBdyInfo_OnCntrRelVelThrs);

DomainBdyInfo_OnCntrMaxWtTime = Simulink.Parameter;
DomainBdyInfo_OnCntrMaxWtTime.Value = 5;
DomainBdyInfo_OnCntrMaxWtTime.Max = 20;
DomainBdyInfo_OnCntrMaxWtTime.Min = 0;
DomainBdyInfo_OnCntrMaxWtTime.Description = 'Description goes here';
DomainBdyInfo_OnCntrMaxWtTime.Unit = 's';
DomainBdyInfo_OnCntrMaxWtTime.DataType = 'double';

assignin('base','DomainBdyInfo_OnCntrMaxWtTime',DomainBdyInfo_OnCntrMaxWtTime);

DomainBdyInfo_OnCntrResetDfltRdHght = Simulink.Parameter;
DomainBdyInfo_OnCntrResetDfltRdHght.Value = 0;
DomainBdyInfo_OnCntrResetDfltRdHght.Max = 1;
DomainBdyInfo_OnCntrResetDfltRdHght.Min = 0;
DomainBdyInfo_OnCntrResetDfltRdHght.Description = 'Description goes here';
DomainBdyInfo_OnCntrResetDfltRdHght.Unit = 'N/A';
DomainBdyInfo_OnCntrResetDfltRdHght.DataType = 'boolean';

assignin('base','DomainBdyInfo_OnCntrResetDfltRdHght',DomainBdyInfo_OnCntrResetDfltRdHght);

DomainBdyInfo_AvgCalcMaxCnt = Simulink.Parameter;
DomainBdyInfo_AvgCalcMaxCnt.Value = 5000;
DomainBdyInfo_AvgCalcMaxCnt.Max = 50000;
DomainBdyInfo_AvgCalcMaxCnt.Min = 0;
DomainBdyInfo_AvgCalcMaxCnt.Description = 'Description goes here';
DomainBdyInfo_AvgCalcMaxCnt.Unit = 'N/A';
DomainBdyInfo_AvgCalcMaxCnt.DataType = 'double';

assignin('base','DomainBdyInfo_AvgCalcMaxCnt',DomainBdyInfo_AvgCalcMaxCnt);

DomainBdyInfo_AvgCalcMinCnt = Simulink.Parameter;
DomainBdyInfo_AvgCalcMinCnt.Value = 1;
DomainBdyInfo_AvgCalcMinCnt.Max = 500;
DomainBdyInfo_AvgCalcMinCnt.Min = 0;
DomainBdyInfo_AvgCalcMinCnt.Description = 'Description goes here';
DomainBdyInfo_AvgCalcMinCnt.Unit = 'N/A';
DomainBdyInfo_AvgCalcMinCnt.DataType = 'double';

assignin('base','DomainBdyInfo_AvgCalcMinCnt',DomainBdyInfo_AvgCalcMinCnt);

DomainBdyInfo_RDSSDCOffFltrCoef = Simulink.Parameter;
DomainBdyInfo_RDSSDCOffFltrCoef.Value = 0.1;
DomainBdyInfo_RDSSDCOffFltrCoef.Max = 80;
DomainBdyInfo_RDSSDCOffFltrCoef.Min = 0;
DomainBdyInfo_RDSSDCOffFltrCoef.Description = 'Description goes here';
DomainBdyInfo_RDSSDCOffFltrCoef.Unit = 'Hz';
DomainBdyInfo_RDSSDCOffFltrCoef.DataType = 'double';

assignin('base','DomainBdyInfo_RDSSDCOffFltrCoef',DomainBdyInfo_RDSSDCOffFltrCoef);

DomainBdyInfo_RDSSFltrLPFCoef = Simulink.Parameter;
DomainBdyInfo_RDSSFltrLPFCoef.Value = 0.05;
DomainBdyInfo_RDSSFltrLPFCoef.Max = 80;
DomainBdyInfo_RDSSFltrLPFCoef.Min = 0;
DomainBdyInfo_RDSSFltrLPFCoef.Description = 'Description goes here';
DomainBdyInfo_RDSSFltrLPFCoef.Unit = 'Hz';
DomainBdyInfo_RDSSFltrLPFCoef.DataType = 'double';

assignin('base','DomainBdyInfo_RDSSFltrLPFCoef',DomainBdyInfo_RDSSFltrLPFCoef);

DomainBdyInfo_RHSSFltrCntrbtn = Simulink.Parameter;
DomainBdyInfo_RHSSFltrCntrbtn.Value = 0.3;
DomainBdyInfo_RHSSFltrCntrbtn.Max = 1;
DomainBdyInfo_RHSSFltrCntrbtn.Min = 0;
DomainBdyInfo_RHSSFltrCntrbtn.Description = 'Description goes here';
DomainBdyInfo_RHSSFltrCntrbtn.Unit = 'N/A';
DomainBdyInfo_RHSSFltrCntrbtn.DataType = 'double';

assignin('base','DomainBdyInfo_RHSSFltrCntrbtn',DomainBdyInfo_RHSSFltrCntrbtn);

%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Parameters for the HWA calculation
%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DomainBdyInfo_HWADCRmvlLPFCoef = Simulink.Parameter;
DomainBdyInfo_HWADCRmvlLPFCoef.Value = 0.35;
DomainBdyInfo_HWADCRmvlLPFCoef.Max = 80;
DomainBdyInfo_HWADCRmvlLPFCoef.Min = 0;
DomainBdyInfo_HWADCRmvlLPFCoef.Description = 'Description goes here';
DomainBdyInfo_HWADCRmvlLPFCoef.Unit = 'Hz';
DomainBdyInfo_HWADCRmvlLPFCoef.DataType = 'double';

assignin('base','DomainBdyInfo_HWADCRmvlLPFCoef',DomainBdyInfo_HWADCRmvlLPFCoef);

DomainBdyInfo_CalcHWARLPFCoef = Simulink.Parameter;
DomainBdyInfo_CalcHWARLPFCoef.Value = 2.5;
DomainBdyInfo_CalcHWARLPFCoef.Max = 80;
DomainBdyInfo_CalcHWARLPFCoef.Min = 0;
DomainBdyInfo_CalcHWARLPFCoef.Description = 'Description goes here';
DomainBdyInfo_CalcHWARLPFCoef.Unit = 'Hz';
DomainBdyInfo_CalcHWARLPFCoef.DataType = 'double';

assignin('base','DomainBdyInfo_CalcHWARLPFCoef',DomainBdyInfo_CalcHWARLPFCoef);

DomainBdyInfo_CalcLngAcclLPFCoef = Simulink.Parameter;
DomainBdyInfo_CalcLngAcclLPFCoef.Value = 3.5;
DomainBdyInfo_CalcLngAcclLPFCoef.Max = 80;
DomainBdyInfo_CalcLngAcclLPFCoef.Min = 0;
DomainBdyInfo_CalcLngAcclLPFCoef.Description = 'Description goes here';
DomainBdyInfo_CalcLngAcclLPFCoef.Unit = 'Hz';
DomainBdyInfo_CalcLngAcclLPFCoef.DataType = 'double';

assignin('base','DomainBdyInfo_CalcLngAcclLPFCoef',DomainBdyInfo_CalcLngAcclLPFCoef);

%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Common variables for vehicle state estimation
%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DomainBdyInfo_CGVelDCRmvlCoef = Simulink.Parameter;
DomainBdyInfo_CGVelDCRmvlCoef.Value = [0.05, 0.01, 0.05];
DomainBdyInfo_CGVelDCRmvlCoef.Max = 80;
DomainBdyInfo_CGVelDCRmvlCoef.Min = 0;
DomainBdyInfo_CGVelDCRmvlCoef.Description = 'Description goes here';
DomainBdyInfo_CGVelDCRmvlCoef.Unit = 'Hz';
DomainBdyInfo_CGVelDCRmvlCoef.DataType = 'double';

assignin('base','DomainBdyInfo_CGVelDCRmvlCoef',DomainBdyInfo_CGVelDCRmvlCoef);

DomainBdyInfo_CGAcclDCRmvlCoef = Simulink.Parameter;
DomainBdyInfo_CGAcclDCRmvlCoef.Value = [0.05,0.01, 0.05];
DomainBdyInfo_CGAcclDCRmvlCoef.Max = 80;
DomainBdyInfo_CGAcclDCRmvlCoef.Min = 0;
DomainBdyInfo_CGAcclDCRmvlCoef.Description = 'Description goes here';
DomainBdyInfo_CGAcclDCRmvlCoef.Unit = 'Hz';
DomainBdyInfo_CGAcclDCRmvlCoef.DataType = 'double';

assignin('base','DomainBdyInfo_CGAcclDCRmvlCoef',DomainBdyInfo_CGAcclDCRmvlCoef);

% ---------------------------------------------------------------------------
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % For Curb weight vehicle parameter upload
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


[A,Av,B,Bv,C,Ca,D,E,param] = Veh_Full_Sys('Curb');  % Get parameters

Con_DstncCrnr2CGCurb = Simulink.Parameter;
Con_DstncCrnr2CGCurb.Value = round([param.a,param.c,param.d,param.b].*100)./100;
Con_DstncCrnr2CGCurb.Max = 10;
Con_DstncCrnr2CGCurb.Min = 0;
Con_DstncCrnr2CGCurb.Description = 'Description goes here';
Con_DstncCrnr2CGCurb.Unit = 'N/A';
Con_DstncCrnr2CGCurb.DataType = 'double';

assignin('base','Con_DstncCrnr2CGCurb',Con_DstncCrnr2CGCurb);

% Domain - slow running system

Accel_inv = pinv(param.Accel_CG);

Accel_inv = round(Accel_inv.*100)./100;      % Res = 0.01

Con_CrnrAz2CGAxCurb = Simulink.Parameter;
Con_CrnrAz2CGAxCurb.Value = Accel_inv(1,:);
Con_CrnrAz2CGAxCurb.Max = 1;
Con_CrnrAz2CGAxCurb.Min = -1;
Con_CrnrAz2CGAxCurb.Description = 'Description goes here';
Con_CrnrAz2CGAxCurb.Unit = 'N/A';
Con_CrnrAz2CGAxCurb.DataType = 'double';

assignin('base','Con_CrnrAz2CGAxCurb',Con_CrnrAz2CGAxCurb);

Con_CrnrAz2CGAyCurb = Simulink.Parameter;
Con_CrnrAz2CGAyCurb.Value = Accel_inv(2,:);
Con_CrnrAz2CGAyCurb.Max = 1;
Con_CrnrAz2CGAyCurb.Min = -1;
Con_CrnrAz2CGAyCurb.Description = 'Description goes here';
Con_CrnrAz2CGAyCurb.Unit = 'N/A';
Con_CrnrAz2CGAyCurb.DataType = 'double';

assignin('base','Con_CrnrAz2CGAyCurb',Con_CrnrAz2CGAyCurb);

Con_CrnrAz2CGAzCurb = Simulink.Parameter;
Con_CrnrAz2CGAzCurb.Value = Accel_inv(3,:);
Con_CrnrAz2CGAzCurb.Max = 1;
Con_CrnrAz2CGAzCurb.Min = -1;
Con_CrnrAz2CGAzCurb.Description = 'Description goes here';
Con_CrnrAz2CGAzCurb.Unit = 'N/A';
Con_CrnrAz2CGAzCurb.DataType = 'double';

assignin('base','Con_CrnrAz2CGAzCurb',Con_CrnrAz2CGAzCurb);

% Linear Quadratic Gaussian Observer Parameters

TotalMass = param.ms;
Ixx = param.ms*param.rx^2;
Iyy = param.ms*param.ry^2;
Kx = param.ksfl+param.ksfr + param.ksrr+param.ksrl;
Ky = Kx*0.95;
Kx = Kx*0.95;
Cx =param.Bf*2 + param.Br*2;
Cy = Cx;

RollQ = [1,0;0,50];
RollR = 0.1;
A_Roll = [0,1;-Kx/Ixx,-Cx/Ixx];
B_Roll = [1, 0;0, 1];
C_Roll = [1 0];
[P2,L2,Kc2] = care(A_Roll, C_Roll', RollQ, RollR);

Con_CGRdHghtCurb = Simulink.Parameter;
Con_CGRdHghtCurb.Value =  0.65;
Con_CGRdHghtCurb.Max = 1;
Con_CGRdHghtCurb.Min = 0;
Con_CGRdHghtCurb.Description = 'Description goes here';
Con_CGRdHghtCurb.Unit = 'N/A';
Con_CGRdHghtCurb.DataType = 'double';

assignin('base','Con_CGRdHghtCurb',Con_CGRdHghtCurb);

Con_CGRllHghtCurb = Simulink.Parameter;
Con_CGRllHghtCurb.Value =  0.42;
Con_CGRllHghtCurb.Max = 1;
Con_CGRllHghtCurb.Min = 0;
Con_CGRllHghtCurb.Description = 'Description goes here';
Con_CGRllHghtCurb.Unit = 'm';
Con_CGRllHghtCurb.DataType = 'double';

assignin('base','Con_CGRllHghtCurb',Con_CGRllHghtCurb);

Con_CGRllInrtiaCurb = Simulink.Parameter;
Con_CGRllInrtiaCurb.Value = round(Ixx);
Con_CGRllInrtiaCurb.Max = 20000;
Con_CGRllInrtiaCurb.Min = 0;
Con_CGRllInrtiaCurb.Description = 'Description goes here';
Con_CGRllInrtiaCurb.Unit = 'N/A';
Con_CGRllInrtiaCurb.DataType = 'double';

assignin('base','Con_CGRllInrtiaCurb',Con_CGRllInrtiaCurb);

DomainBdyInfo_LQGRllEstGnCurb = Simulink.Parameter;
DomainBdyInfo_LQGRllEstGnCurb.Value = round(Kc2.*100)./100;
DomainBdyInfo_LQGRllEstGnCurb.Max = 100;
DomainBdyInfo_LQGRllEstGnCurb.Min = -100;
DomainBdyInfo_LQGRllEstGnCurb.Description = 'Description goes here';
DomainBdyInfo_LQGRllEstGnCurb.Unit = 'N/A';
DomainBdyInfo_LQGRllEstGnCurb.DataType = 'double';

assignin('base','DomainBdyInfo_LQGRllEstGnCurb',DomainBdyInfo_LQGRllEstGnCurb);

DomainBdyInfo_LQGRllEstA2Curb = Simulink.Parameter;
DomainBdyInfo_LQGRllEstA2Curb.Value = round([-Kx/Ixx,-Cx/Ixx].*100)./100; 
DomainBdyInfo_LQGRllEstA2Curb.Max = 100;
DomainBdyInfo_LQGRllEstA2Curb.Min = -100;
DomainBdyInfo_LQGRllEstA2Curb.Description = 'Description goes here';
DomainBdyInfo_LQGRllEstA2Curb.Unit = 'N/A';
DomainBdyInfo_LQGRllEstA2Curb.DataType = 'double';

assignin('base','DomainBdyInfo_LQGRllEstA2Curb',DomainBdyInfo_LQGRllEstA2Curb);

Con_VehWhlBase = Simulink.Parameter;
Con_VehWhlBase.Value = round(param.WhlBase);
Con_VehWhlBase.Max = round(param.WhlBase) + 100;
Con_VehWhlBase.Min = 0;
Con_VehWhlBase.Description = 'Description goes here';
Con_VehWhlBase.Unit = 'N/A';
Con_VehWhlBase.DataType = 'double';

assignin('base','Con_VehWhlBase',Con_VehWhlBase);

Con_TtlVehMassCurb = Simulink.Parameter;
Con_TtlVehMassCurb.Value = round(TotalMass);
Con_TtlVehMassCurb.Max = round(TotalMass) + 100;
Con_TtlVehMassCurb.Min = 0;
Con_TtlVehMassCurb.Description = 'Description goes here';
Con_TtlVehMassCurb.Unit = 'N/A';
Con_TtlVehMassCurb.DataType = 'double';

assignin('base','Con_TtlVehMassCurb',Con_TtlVehMassCurb);

Con_FrntTrckWdth = Simulink.Parameter;
Con_FrntTrckWdth.Value = round(param.TrackWidthFront.*100)./100; 
Con_FrntTrckWdth.Max = 5;
Con_FrntTrckWdth.Min = 0;
Con_FrntTrckWdth.Description = 'Description goes here';
Con_FrntTrckWdth.Unit = 'N/A';
Con_FrntTrckWdth.DataType = 'double';

assignin('base','Con_FrntTrckWdth',Con_FrntTrckWdth);

Con_RrTrckWdth = Simulink.Parameter;
Con_RrTrckWdth.Value = round(param.TrackWidthRear.*100)./100; 
Con_RrTrckWdth.Max = 5;
Con_RrTrckWdth.Min = 0;
Con_RrTrckWdth.Description = 'Description goes here';
Con_RrTrckWdth.Unit = 'N/A';
Con_RrTrckWdth.DataType = 'double';

assignin('base','Con_RrTrckWdth',Con_RrTrckWdth);

DomainBdyInfo_RllRtClcltnLPFCoef = Simulink.Parameter;
DomainBdyInfo_RllRtClcltnLPFCoef.Value = 2.5;
DomainBdyInfo_RllRtClcltnLPFCoef.Max = 85;
DomainBdyInfo_RllRtClcltnLPFCoef.Min = 0;
DomainBdyInfo_RllRtClcltnLPFCoef.Description = 'Description goes here';
DomainBdyInfo_RllRtClcltnLPFCoef.Unit = 'Hz';
DomainBdyInfo_RllRtClcltnLPFCoef.DataType = 'double';

assignin('base','DomainBdyInfo_RllRtClcltnLPFCoef',DomainBdyInfo_RllRtClcltnLPFCoef);

DomainBdyInfo_RllRtEstmtnDCOffLPFCoef = Simulink.Parameter;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Value = 0.1;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Max = 85;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Min = 0;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Description = 'Description goes here';
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Unit = 'Hz';
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.DataType = 'double';

assignin('base','DomainBdyInfo_RllRtEstmtnDCOffLPFCoef',DomainBdyInfo_RllRtEstmtnDCOffLPFCoef);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For pitch motion
PitchQ = [0.1,0;0,50];
PitchR = 0.1;
A_Pitch = [0,1;-Ky/Iyy,-Cy/Iyy];
B_Pitch = [1, 0;0, 1];
C_Pitch = [1, 0];
D = [0, 0];

[P2,L2,Kc2] = care(A_Pitch, C_Pitch', PitchQ, PitchR);

Con_CGPtchHghtCurb = Simulink.Parameter;
Con_CGPtchHghtCurb.Value =  0.25;
Con_CGPtchHghtCurb.Max = 1;
Con_CGPtchHghtCurb.Min = 0;
Con_CGPtchHghtCurb.Description = 'Description goes here';
Con_CGPtchHghtCurb.Unit = 'm';
Con_CGPtchHghtCurb.DataType = 'double';

assignin('base','Con_CGPtchHghtCurb',Con_CGPtchHghtCurb);

Con_CGPtchInrtiaCurb = Simulink.Parameter;
Con_CGPtchInrtiaCurb.Value = round(Iyy);
Con_CGPtchInrtiaCurb.Max = 50000;
Con_CGPtchInrtiaCurb.Min = 0;
Con_CGPtchInrtiaCurb.Description = 'Description goes here';
Con_CGPtchInrtiaCurb.Unit = 'N/A';
Con_CGPtchInrtiaCurb.DataType = 'double';

assignin('base','Con_CGPtchInrtiaCurb',Con_CGPtchInrtiaCurb);

DomainBdyInfo_LQGPtchEstGnCurb = Simulink.Parameter;
DomainBdyInfo_LQGPtchEstGnCurb.Value = round(Kc2.*100)./100;
DomainBdyInfo_LQGPtchEstGnCurb.Max = 100;
DomainBdyInfo_LQGPtchEstGnCurb.Min = -100;
DomainBdyInfo_LQGPtchEstGnCurb.Description = 'Description goes here';
DomainBdyInfo_LQGPtchEstGnCurb.Unit = 'N/A';
DomainBdyInfo_LQGPtchEstGnCurb.DataType = 'double';

assignin('base','DomainBdyInfo_LQGPtchEstGnCurb',DomainBdyInfo_LQGPtchEstGnCurb);

DomainBdyInfo_LQGPtchEstA2Curb = Simulink.Parameter;
DomainBdyInfo_LQGPtchEstA2Curb.Value = round([-Ky/Iyy,-Cy/Iyy].*100)./100;
DomainBdyInfo_LQGPtchEstA2Curb.Max = 100;
DomainBdyInfo_LQGPtchEstA2Curb.Min = -100;
DomainBdyInfo_LQGPtchEstA2Curb.Description = 'Description goes here';
DomainBdyInfo_LQGPtchEstA2Curb.Unit = 'N/A';
DomainBdyInfo_LQGPtchEstA2Curb.DataType = 'double';

assignin('base','DomainBdyInfo_LQGPtchEstA2Curb',DomainBdyInfo_LQGPtchEstA2Curb);

Con_DmprLngtdnlDstnc = Simulink.Parameter;
Con_DmprLngtdnlDstnc.Value = round(param.DamperLongDist*100)./100;
Con_DmprLngtdnlDstnc.Max = 100;
Con_DmprLngtdnlDstnc.Min = -100;
Con_DmprLngtdnlDstnc.Description = 'Description goes here';
Con_DmprLngtdnlDstnc.Unit = 'N/A';
Con_DmprLngtdnlDstnc.DataType = 'double';

assignin('base','Con_DmprLngtdnlDstnc',Con_DmprLngtdnlDstnc);

%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 
%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % For GVW weight vehicle parameter upload
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[A,Av,B,Bv,C,Ca,D,E,param] = Veh_Full_Sys('GVW');  % Get parameters

Con_DstncCrnr2CGGVW = Simulink.Parameter;
Con_DstncCrnr2CGGVW.Value = round([param.a,param.c,param.d,param.b].*100)./100;
Con_DstncCrnr2CGGVW.Max = 10;
Con_DstncCrnr2CGGVW.Min = 0;
Con_DstncCrnr2CGGVW.Description = 'Description goes here';
Con_DstncCrnr2CGGVW.Unit = 'N/A';
Con_DstncCrnr2CGGVW.DataType = 'double';

assignin('base','Con_DstncCrnr2CGGVW',Con_DstncCrnr2CGGVW);

% Domain - slow running system

Accel_inv = pinv(param.Accel_CG);

Accel_inv = round(Accel_inv.*100)./100;      % Res = 0.01

Con_CrnrAz2CGAxGVW = Simulink.Parameter;
Con_CrnrAz2CGAxGVW.Value = Accel_inv(1,:);
Con_CrnrAz2CGAxGVW.Max = 1;
Con_CrnrAz2CGAxGVW.Min = -1;
Con_CrnrAz2CGAxGVW.Description = 'Description goes here';
Con_CrnrAz2CGAxGVW.Unit = 'N/A';
Con_CrnrAz2CGAxGVW.DataType = 'double';

assignin('base','Con_CrnrAz2CGAxGVW',Con_CrnrAz2CGAxGVW);

Con_CrnrAz2CGAyGVW = Simulink.Parameter;
Con_CrnrAz2CGAyGVW.Value = Accel_inv(2,:);
Con_CrnrAz2CGAyGVW.Max = 1;
Con_CrnrAz2CGAyGVW.Min = -1;
Con_CrnrAz2CGAyGVW.Description = 'Description goes here';
Con_CrnrAz2CGAyGVW.Unit = 'N/A';
Con_CrnrAz2CGAyGVW.DataType = 'double';

assignin('base','Con_CrnrAz2CGAyGVW',Con_CrnrAz2CGAyGVW);

Con_CrnrAz2CGAzGVW = Simulink.Parameter;
Con_CrnrAz2CGAzGVW.Value = Accel_inv(3,:);
Con_CrnrAz2CGAzGVW.Max = 1;
Con_CrnrAz2CGAzGVW.Min = -1;
Con_CrnrAz2CGAzGVW.Description = 'Description goes here';
Con_CrnrAz2CGAzGVW.Unit = 'N/A';
Con_CrnrAz2CGAzGVW.DataType = 'double';

assignin('base','Con_CrnrAz2CGAzGVW',Con_CrnrAz2CGAzGVW);

% Linear Quadratic Gaussian Observer Parameters


TotalMass = param.ms;
Ixx = param.ms*param.rx^2;
Iyy = param.ms*param.ry^2;
Kx = param.ksfl+param.ksfr + param.ksrr+param.ksrl;
Ky = Kx*0.95;
Kx = Kx*0.95;
Cx =param.Bf*2 + param.Br*2;
Cy = Cx;

RollQ = [1,0;0,50];
RollR = 0.1;
A_Roll = [0,1;-Kx/Ixx,-Cx/Ixx];
B_Roll = [1, 0;0, 1];
C_Roll = [1 0];
[P2,L2,Kc2] = care(A_Roll, C_Roll', RollQ, RollR);

Con_CGRdHghtGVW = Simulink.Parameter;
Con_CGRdHghtGVW.Value =  0.65;
Con_CGRdHghtGVW.Max = 1;
Con_CGRdHghtGVW.Min = 0;
Con_CGRdHghtGVW.Description = 'Description goes here';
Con_CGRdHghtGVW.Unit = 'N/A';
Con_CGRdHghtGVW.DataType = 'double';

assignin('base','Con_CGRdHghtGVW',Con_CGRdHghtGVW);

Con_CGRllHghtGVW = Simulink.Parameter;
Con_CGRllHghtGVW.Value =  0.42;
Con_CGRllHghtGVW.Max = 1;
Con_CGRllHghtGVW.Min = 0;
Con_CGRllHghtGVW.Description = 'Description goes here';
Con_CGRllHghtGVW.Unit = 'm';
Con_CGRllHghtGVW.DataType = 'double';

assignin('base','Con_CGRllHghtGVW',Con_CGRllHghtGVW);

Con_CGRllInrtiaGVW = Simulink.Parameter;
Con_CGRllInrtiaGVW.Value = round(Ixx);
Con_CGRllInrtiaGVW.Max = 20000;
Con_CGRllInrtiaGVW.Min = 0;
Con_CGRllInrtiaGVW.Description = 'Description goes here';
Con_CGRllInrtiaGVW.Unit = 'N/A';
Con_CGRllInrtiaGVW.DataType = 'double';

assignin('base','Con_CGRllInrtiaGVW',Con_CGRllInrtiaGVW);

DomainBdyInfo_LQGRllEstGnGVW = Simulink.Parameter;
DomainBdyInfo_LQGRllEstGnGVW.Value = round(Kc2.*100)./100;
DomainBdyInfo_LQGRllEstGnGVW.Max = 300;
DomainBdyInfo_LQGRllEstGnGVW.Min = -300;
DomainBdyInfo_LQGRllEstGnGVW.Description = 'Description goes here';
DomainBdyInfo_LQGRllEstGnGVW.Unit = 'N/A';
DomainBdyInfo_LQGRllEstGnGVW.DataType = 'double';

assignin('base','DomainBdyInfo_LQGRllEstGnGVW',DomainBdyInfo_LQGRllEstGnGVW);

DomainBdyInfo_LQGRllEstA2GVW = Simulink.Parameter;
DomainBdyInfo_LQGRllEstA2GVW.Value = round([-Kx/Ixx,-Cx/Ixx].*100)./100; 
DomainBdyInfo_LQGRllEstA2GVW.Max = 300;
DomainBdyInfo_LQGRllEstA2GVW.Min = -300;
DomainBdyInfo_LQGRllEstA2GVW.Description = 'Description goes here';
DomainBdyInfo_LQGRllEstA2GVW.Unit = 'N/A';
DomainBdyInfo_LQGRllEstA2GVW.DataType = 'double';

assignin('base','DomainBdyInfo_LQGRllEstA2GVW',DomainBdyInfo_LQGRllEstA2GVW);

Con_TtlVehMassGVW = Simulink.Parameter;
Con_TtlVehMassGVW.Value = round(TotalMass);
Con_TtlVehMassGVW.Max = round(TotalMass) + 100;
Con_TtlVehMassGVW.Min = 0;
Con_TtlVehMassGVW.Description = 'Description goes here';
Con_TtlVehMassGVW.Unit = 'N/A';
Con_TtlVehMassGVW.DataType = 'double';

assignin('base','Con_TtlVehMassGVW',Con_TtlVehMassGVW);

Con_FrntTrckWdth = Simulink.Parameter;
Con_FrntTrckWdth.Value = round(param.TrackWidthFront.*100)./100; 
Con_FrntTrckWdth.Max = 5;
Con_FrntTrckWdth.Min = 0;
Con_FrntTrckWdth.Description = 'Description goes here';
Con_FrntTrckWdth.Unit = 'N/A';
Con_FrntTrckWdth.DataType = 'double';

assignin('base','Con_FrntTrckWdth',Con_FrntTrckWdth);

Con_RrTrckWdth = Simulink.Parameter;
Con_RrTrckWdth.Value = round(param.TrackWidthRear.*100)./100; 
Con_RrTrckWdth.Max = 5;
Con_RrTrckWdth.Min = 0;
Con_RrTrckWdth.Description = 'Description goes here';
Con_RrTrckWdth.Unit = 'N/A';
Con_RrTrckWdth.DataType = 'double';

assignin('base','Con_RrTrckWdth',Con_RrTrckWdth);

DomainBdyInfo_RllRtClcltnLPFCoef = Simulink.Parameter;
DomainBdyInfo_RllRtClcltnLPFCoef.Value = 2.5;
DomainBdyInfo_RllRtClcltnLPFCoef.Max = 85;
DomainBdyInfo_RllRtClcltnLPFCoef.Min = 0;
DomainBdyInfo_RllRtClcltnLPFCoef.Description = 'Description goes here';
DomainBdyInfo_RllRtClcltnLPFCoef.Unit = 'Hz';
DomainBdyInfo_RllRtClcltnLPFCoef.DataType = 'double';

assignin('base','DomainBdyInfo_RllRtClcltnLPFCoef',DomainBdyInfo_RllRtClcltnLPFCoef);

DomainBdyInfo_RllRtEstmtnDCOffLPFCoef = Simulink.Parameter;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Value = 0.1;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Max = 85;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Min = 0;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Description = 'Description goes here';
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Unit = 'Hz';
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.DataType = 'double';

assignin('base','DomainBdyInfo_RllRtEstmtnDCOffLPFCoef',DomainBdyInfo_RllRtEstmtnDCOffLPFCoef);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For pitch motion
PitchQ = [0.1,0;0,50];
PitchR = 0.1;
A_Pitch = [0,1;-Ky/Iyy,-Cy/Iyy];
B_Pitch = [1, 0;0, 1];
C_Pitch = [1, 0];
D = [0, 0];

[P2,L2,Kc2] = care(A_Pitch, C_Pitch', PitchQ, PitchR);

Con_CGPtchHghtGVW = Simulink.Parameter;
Con_CGPtchHghtGVW.Value =  0.25;
Con_CGPtchHghtGVW.Max = 1;
Con_CGPtchHghtGVW.Min = 0;
Con_CGPtchHghtGVW.Description = 'Description goes here';
Con_CGPtchHghtGVW.Unit = 'm';
Con_CGPtchHghtGVW.DataType = 'double';

assignin('base','Con_CGPtchHghtGVW',Con_CGPtchHghtGVW);

Con_CGPtchInrtiaGVW = Simulink.Parameter;
Con_CGPtchInrtiaGVW.Value = round(Iyy);
Con_CGPtchInrtiaGVW.Max = 50000;
Con_CGPtchInrtiaGVW.Min = 0;
Con_CGPtchInrtiaGVW.Description = 'Description goes here';
Con_CGPtchInrtiaGVW.Unit = 'N/A';
Con_CGPtchInrtiaGVW.DataType = 'double';

assignin('base','Con_CGPtchInrtiaGVW',Con_CGPtchInrtiaGVW);

DomainBdyInfo_LQGPtchEstGnGVW = Simulink.Parameter;
DomainBdyInfo_LQGPtchEstGnGVW.Value = round(Kc2.*100)./100;
DomainBdyInfo_LQGPtchEstGnGVW.Max = 100;
DomainBdyInfo_LQGPtchEstGnGVW.Min = -100;
DomainBdyInfo_LQGPtchEstGnGVW.Description = 'Description goes here';
DomainBdyInfo_LQGPtchEstGnGVW.Unit = 'N/A';
DomainBdyInfo_LQGPtchEstGnGVW.DataType = 'double';

assignin('base','DomainBdyInfo_LQGPtchEstGnGVW',DomainBdyInfo_LQGPtchEstGnGVW);

DomainBdyInfo_LQGPtchEstA2GVW = Simulink.Parameter;
DomainBdyInfo_LQGPtchEstA2GVW.Value = round([-Ky/Iyy,-Cy/Iyy].*100)./100;
DomainBdyInfo_LQGPtchEstA2GVW.Max = 100;
DomainBdyInfo_LQGPtchEstA2GVW.Min = -100;
DomainBdyInfo_LQGPtchEstA2GVW.Description = 'Description goes here';
DomainBdyInfo_LQGPtchEstA2GVW.Unit = 'N/A';
DomainBdyInfo_LQGPtchEstA2GVW.DataType = 'double';

assignin('base','DomainBdyInfo_LQGPtchEstA2GVW',DomainBdyInfo_LQGPtchEstA2GVW);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % For Dflt weight vehicle parameter upload
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[A,Av,B,Bv,C,Ca,D,E,param] = Veh_Full_Sys('Dflt');  % Get parameters

Con_DstncCrnr2CGDflt = Simulink.Parameter;
Con_DstncCrnr2CGDflt.Value = round([param.a,param.c,param.d,param.b].*100)./100;
Con_DstncCrnr2CGDflt.Max = 10;
Con_DstncCrnr2CGDflt.Min = 0;
Con_DstncCrnr2CGDflt.Description = 'Description goes here';
Con_DstncCrnr2CGDflt.Unit = 'N/A';
Con_DstncCrnr2CGDflt.DataType = 'double';

assignin('base','Con_DstncCrnr2CGDflt',Con_DstncCrnr2CGDflt);

% Domain - slow running system

Accel_inv = pinv(param.Accel_CG);

Accel_inv = round(Accel_inv.*100)./100;      % Res = 0.01

Con_CrnrAz2CGAxDflt = Simulink.Parameter;
Con_CrnrAz2CGAxDflt.Value = Accel_inv(1,:);
Con_CrnrAz2CGAxDflt.Max = 1;
Con_CrnrAz2CGAxDflt.Min = -1;
Con_CrnrAz2CGAxDflt.Description = 'Description goes here';
Con_CrnrAz2CGAxDflt.Unit = 'N/A';
Con_CrnrAz2CGAxDflt.DataType = 'double';

assignin('base','Con_CrnrAz2CGAxDflt',Con_CrnrAz2CGAxDflt);

Con_CrnrAz2CGAyDflt = Simulink.Parameter;
Con_CrnrAz2CGAyDflt.Value = Accel_inv(2,:);
Con_CrnrAz2CGAyDflt.Max = 1;
Con_CrnrAz2CGAyDflt.Min = -1;
Con_CrnrAz2CGAyDflt.Description = 'Description goes here';
Con_CrnrAz2CGAyDflt.Unit = 'N/A';
Con_CrnrAz2CGAyDflt.DataType = 'double';

assignin('base','Con_CrnrAz2CGAyDflt',Con_CrnrAz2CGAyDflt);

Con_CrnrAz2CGAzDflt = Simulink.Parameter;
Con_CrnrAz2CGAzDflt.Value = Accel_inv(3,:);
Con_CrnrAz2CGAzDflt.Max = 1;
Con_CrnrAz2CGAzDflt.Min = -1;
Con_CrnrAz2CGAzDflt.Description = 'Description goes here';
Con_CrnrAz2CGAzDflt.Unit = 'N/A';
Con_CrnrAz2CGAzDflt.DataType = 'double';

assignin('base','Con_CrnrAz2CGAzDflt',Con_CrnrAz2CGAzDflt);

% Linear Quadratic Gaussian Observer Parameters


TotalMass = param.ms;
Ixx = param.ms*param.rx^2;
Iyy = param.ms*param.ry^2;
Kx = param.ksfl+param.ksfr + param.ksrr+param.ksrl;
Ky = Kx*0.95;
Kx = Kx*0.95;
Cx =param.Bf*2 + param.Br*2;
Cy = Cx;

RollQ = [1,0;0,50];
RollR = 0.1;
A_Roll = [0,1;-Kx/Ixx,-Cx/Ixx];
B_Roll = [1, 0;0, 1];
C_Roll = [1 0];
[P2,L2,Kc2] = care(A_Roll, C_Roll', RollQ, RollR);

Con_CGRdHghtDflt = Simulink.Parameter;
Con_CGRdHghtDflt.Value =  0.65;
Con_CGRdHghtDflt.Max = 1;
Con_CGRdHghtDflt.Min = 0;
Con_CGRdHghtDflt.Description = 'Description goes here';
Con_CGRdHghtDflt.Unit = 'N/A';
Con_CGRdHghtDflt.DataType = 'double';

assignin('base','Con_CGRdHghtDflt',Con_CGRdHghtDflt);

Con_CGRllHghtDflt = Simulink.Parameter;
Con_CGRllHghtDflt.Value =  0.42;
Con_CGRllHghtDflt.Max = 1;
Con_CGRllHghtDflt.Min = 0;
Con_CGRllHghtDflt.Description = 'Description goes here';
Con_CGRllHghtDflt.Unit = 'm';
Con_CGRllHghtDflt.DataType = 'double';

assignin('base','Con_CGRllHghtDflt',Con_CGRllHghtDflt);

Con_CGRllInrtiaDflt = Simulink.Parameter;
Con_CGRllInrtiaDflt.Value = round(Ixx);
Con_CGRllInrtiaDflt.Max = 20000;
Con_CGRllInrtiaDflt.Min = 0;
Con_CGRllInrtiaDflt.Description = 'Description goes here';
Con_CGRllInrtiaDflt.Unit = 'N/A';
Con_CGRllInrtiaDflt.DataType = 'double';

assignin('base','Con_CGRllInrtiaDflt',Con_CGRllInrtiaDflt);

DomainBdyInfo_LQGRllEstGnDflt = Simulink.Parameter;
DomainBdyInfo_LQGRllEstGnDflt.Value = round(Kc2.*100)./100;
DomainBdyInfo_LQGRllEstGnDflt.Max = 100;
DomainBdyInfo_LQGRllEstGnDflt.Min = -100;
DomainBdyInfo_LQGRllEstGnDflt.Description = 'Description goes here';
DomainBdyInfo_LQGRllEstGnDflt.Unit = 'N/A';
DomainBdyInfo_LQGRllEstGnDflt.DataType = 'double';

assignin('base','DomainBdyInfo_LQGRllEstGnDflt',DomainBdyInfo_LQGRllEstGnDflt);

DomainBdyInfo_LQGRllEstA2Dflt = Simulink.Parameter;
DomainBdyInfo_LQGRllEstA2Dflt.Value = round([-Kx/Ixx,-Cx/Ixx].*100)./100; 
DomainBdyInfo_LQGRllEstA2Dflt.Max = 100;
DomainBdyInfo_LQGRllEstA2Dflt.Min = -100;
DomainBdyInfo_LQGRllEstA2Dflt.Description = 'Description goes here';
DomainBdyInfo_LQGRllEstA2Dflt.Unit = 'N/A';
DomainBdyInfo_LQGRllEstA2Dflt.DataType = 'double';

assignin('base','DomainBdyInfo_LQGRllEstA2Dflt',DomainBdyInfo_LQGRllEstA2Dflt);

Con_TtlVehMassDflt = Simulink.Parameter;
Con_TtlVehMassDflt.Value = round(TotalMass);
Con_TtlVehMassDflt.Max = round(TotalMass) + 100;
Con_TtlVehMassDflt.Min = 0;
Con_TtlVehMassDflt.Description = 'Description goes here';
Con_TtlVehMassDflt.Unit = 'N/A';
Con_TtlVehMassDflt.DataType = 'double';

assignin('base','Con_TtlVehMassDflt',Con_TtlVehMassDflt);

Con_FrntTrckWdth = Simulink.Parameter;
Con_FrntTrckWdth.Value = round(param.TrackWidthFront.*100)./100; 
Con_FrntTrckWdth.Max = 5;
Con_FrntTrckWdth.Min = 0;
Con_FrntTrckWdth.Description = 'Description goes here';
Con_FrntTrckWdth.Unit = 'N/A';
Con_FrntTrckWdth.DataType = 'double';

assignin('base','Con_FrntTrckWdth',Con_FrntTrckWdth);

Con_RrTrckWdth = Simulink.Parameter;
Con_RrTrckWdth.Value = round(param.TrackWidthRear.*100)./100; 
Con_RrTrckWdth.Max = 5;
Con_RrTrckWdth.Min = 0;
Con_RrTrckWdth.Description = 'Description goes here';
Con_RrTrckWdth.Unit = 'N/A';
Con_RrTrckWdth.DataType = 'double';

assignin('base','Con_RrTrckWdth',Con_RrTrckWdth);

DomainBdyInfo_RllRtClcltnLPFCoef = Simulink.Parameter;
DomainBdyInfo_RllRtClcltnLPFCoef.Value = 2.5;
DomainBdyInfo_RllRtClcltnLPFCoef.Max = 85;
DomainBdyInfo_RllRtClcltnLPFCoef.Min = 0;
DomainBdyInfo_RllRtClcltnLPFCoef.Description = 'Description goes here';
DomainBdyInfo_RllRtClcltnLPFCoef.Unit = 'Hz';
DomainBdyInfo_RllRtClcltnLPFCoef.DataType = 'double';

assignin('base','DomainBdyInfo_RllRtClcltnLPFCoef',DomainBdyInfo_RllRtClcltnLPFCoef);

DomainBdyInfo_RllRtEstmtnDCOffLPFCoef = Simulink.Parameter;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Value = 0.1;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Max = 85;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Min = 0;
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Description = 'Description goes here';
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.Unit = 'Hz';
DomainBdyInfo_RllRtEstmtnDCOffLPFCoef.DataType = 'double';

assignin('base','DomainBdyInfo_RllRtEstmtnDCOffLPFCoef',DomainBdyInfo_RllRtEstmtnDCOffLPFCoef);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For pitch motion
PitchQ = [0.1,0;0,50];
PitchR = 0.1;
A_Pitch = [0,1;-Ky/Iyy,-Cy/Iyy];
B_Pitch = [1, 0;0, 1];
C_Pitch = [1, 0];
D = [0, 0];

[P2,L2,Kc2] = care(A_Pitch, C_Pitch', PitchQ, PitchR);

Con_CGPtchHghtDflt = Simulink.Parameter;
Con_CGPtchHghtDflt.Value =  0.25;
Con_CGPtchHghtDflt.Max = 1;
Con_CGPtchHghtDflt.Min = 0;
Con_CGPtchHghtDflt.Description = 'Description goes here';
Con_CGPtchHghtDflt.Unit = 'm';
Con_CGPtchHghtDflt.DataType = 'double';

assignin('base','Con_CGPtchHghtDflt',Con_CGPtchHghtDflt);

Con_CGPtchInrtiaDflt = Simulink.Parameter;
Con_CGPtchInrtiaDflt.Value = round(Iyy);
Con_CGPtchInrtiaDflt.Max = 50000;
Con_CGPtchInrtiaDflt.Min = 0;
Con_CGPtchInrtiaDflt.Description = 'Description goes here';
Con_CGPtchInrtiaDflt.Unit = 'N/A';
Con_CGPtchInrtiaDflt.DataType = 'double';

assignin('base','Con_CGPtchInrtiaDflt',Con_CGPtchInrtiaDflt);

DomainBdyInfo_LQGPtchEstGnDflt = Simulink.Parameter;
DomainBdyInfo_LQGPtchEstGnDflt.Value = round(Kc2.*100)./100;
DomainBdyInfo_LQGPtchEstGnDflt.Max = 100;
DomainBdyInfo_LQGPtchEstGnDflt.Min = -100;
DomainBdyInfo_LQGPtchEstGnDflt.Description = 'Description goes here';
DomainBdyInfo_LQGPtchEstGnDflt.Unit = 'N/A';
DomainBdyInfo_LQGPtchEstGnDflt.DataType = 'double';

assignin('base','DomainBdyInfo_LQGPtchEstGnDflt',DomainBdyInfo_LQGPtchEstGnDflt);

DomainBdyInfo_LQGPtchEstA2Dflt = Simulink.Parameter;
DomainBdyInfo_LQGPtchEstA2Dflt.Value = round([-Ky/Iyy,-Cy/Iyy].*100)./100;
DomainBdyInfo_LQGPtchEstA2Dflt.Max = 100;
DomainBdyInfo_LQGPtchEstA2Dflt.Min = -100;
DomainBdyInfo_LQGPtchEstA2Dflt.Description = 'Description goes here';
DomainBdyInfo_LQGPtchEstA2Dflt.Unit = 'N/A';
DomainBdyInfo_LQGPtchEstA2Dflt.DataType = 'double';

assignin('base','DomainBdyInfo_LQGPtchEstA2Dflt',DomainBdyInfo_LQGPtchEstA2Dflt);


return

%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AC_Sensors.m ends here

