function [] = DomainWhlInfo_CalParams()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filename : DomainWhlInfo_CalParams.m
%
% Created : Friday 14 April 2017 - 9:11:21 PM (Eastern Daylight Time)
%
% Updated on: Tuesday 02 May 2017 - 8:12:59 PM (Eastern Daylight Time)
%
% Modified by: Juchirl Park
%
% Created By : Juchirl Park 
%
% Description : Sensor characterization and domain parameters are calculated
% and upload into SIMULINK model directly. This file is mostly called by the
% main m-function file, "Driver_Controls_Main" with appropriate variables.
%
% [] = DomainWhlInfo_CalParams
%	
% Usage example:
%
% DomainWhlInfo_CalParams;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Code Start here

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Domain - suspension status
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Resolution = 0.001; %1 mili-
Max = 3.0;
Min = -3.0;

% Motion ratio definition
RdHght = [0 25 50 75 100 125 150 175 200 225 250 275 300 325 350 375 400];
MtnRt_FL = [0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64 ...
            0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64];
MtnRt_FR = [0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64 ...
            0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64];
MtnRt_RL = [0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64 ...
            0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64];
MtnRt_RR = [0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64 ...
            0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64 0.64];

DomainWhlInfo_RdHght4MtnRt = Simulink.Parameter;
DomainWhlInfo_RdHght4MtnRt.Value = RdHght;
DomainWhlInfo_RdHght4MtnRt.Max = 1000;
DomainWhlInfo_RdHght4MtnRt.Min = 0;
DomainWhlInfo_RdHght4MtnRt.Description = 'Description goes here';
DomainWhlInfo_RdHght4MtnRt.Unit = 'mm';
DomainWhlInfo_RdHght4MtnRt.DataType = 'double';

assignin('base','DomainWhlInfo_RdHght4MtnRt',DomainWhlInfo_RdHght4MtnRt);

DomainWhlInfo_ShckMtnRtFL = Simulink.Parameter;
DomainWhlInfo_ShckMtnRtFL.Value = MtnRt_FL;
DomainWhlInfo_ShckMtnRtFL.Max = 10;
DomainWhlInfo_ShckMtnRtFL.Min = 0;
DomainWhlInfo_ShckMtnRtFL.Description = 'Description goes here';
DomainWhlInfo_ShckMtnRtFL.Unit = 'mm';
DomainWhlInfo_ShckMtnRtFL.DataType = 'double';

assignin('base','DomainWhlInfo_ShckMtnRtFL',DomainWhlInfo_ShckMtnRtFL);

DomainWhlInfo_ShckMtnRtFR = Simulink.Parameter;
DomainWhlInfo_ShckMtnRtFR.Value = MtnRt_FR;
DomainWhlInfo_ShckMtnRtFR.Max = 10;
DomainWhlInfo_ShckMtnRtFR.Min = 0;
DomainWhlInfo_ShckMtnRtFR.Description = 'Description goes here';
DomainWhlInfo_ShckMtnRtFR.Unit = 'mm';
DomainWhlInfo_ShckMtnRtFR.DataType = 'double';

assignin('base','DomainWhlInfo_ShckMtnRtFR',DomainWhlInfo_ShckMtnRtFR);

DomainWhlInfo_ShckMtnRtRL = Simulink.Parameter;
DomainWhlInfo_ShckMtnRtRL.Value = MtnRt_RL;
DomainWhlInfo_ShckMtnRtRL.Max = 10;
DomainWhlInfo_ShckMtnRtRL.Min = 0;
DomainWhlInfo_ShckMtnRtRL.Description = 'Description goes here';
DomainWhlInfo_ShckMtnRtRL.Unit = 'mm';
DomainWhlInfo_ShckMtnRtRL.DataType = 'double';

assignin('base','DomainWhlInfo_ShckMtnRtRL',DomainWhlInfo_ShckMtnRtRL);

DomainWhlInfo_ShckMtnRtRR = Simulink.Parameter;
DomainWhlInfo_ShckMtnRtRR.Value = MtnRt_RR;
DomainWhlInfo_ShckMtnRtRR.Max = 10;
DomainWhlInfo_ShckMtnRtRR.Min = 0;
DomainWhlInfo_ShckMtnRtRR.Description = 'Description goes here';
DomainWhlInfo_ShckMtnRtRR.Unit = 'mm';
DomainWhlInfo_ShckMtnRtRR.DataType = 'double';

assignin('base','DomainWhlInfo_ShckMtnRtRR',DomainWhlInfo_ShckMtnRtRR);

DomainWhlInfo_RdHghtSuspension = Simulink.Parameter;
DomainWhlInfo_RdHghtSuspension.Value = [500 500 500 500];
DomainWhlInfo_RdHghtSuspension.Max = 1000;
DomainWhlInfo_RdHghtSuspension.Min = 0;
DomainWhlInfo_RdHghtSuspension.Description = 'Suspension nominal ride height';
DomainWhlInfo_RdHghtSuspension.Unit = 'mm';
DomainWhlInfo_RdHghtSuspension.DataType = 'double';

assignin('base','DomainWhlInfo_RdHghtSuspension',DomainWhlInfo_RdHghtSuspension);

DomainWhlInfo_ShckVelHystThrs = Simulink.Parameter;
DomainWhlInfo_ShckVelHystThrs.Value = 20;
DomainWhlInfo_ShckVelHystThrs.Max = 50;
DomainWhlInfo_ShckVelHystThrs.Min = 0;
DomainWhlInfo_ShckVelHystThrs.Description = 'Description goes here';
DomainWhlInfo_ShckVelHystThrs.Unit = 'mm/s';
DomainWhlInfo_ShckVelHystThrs.DataType = 'double';

assignin('base','DomainWhlInfo_ShckVelHystThrs',DomainWhlInfo_ShckVelHystThrs);

DomainWhlInfo_ShckVelDCRmvlLPFCoef = Simulink.Parameter;
DomainWhlInfo_ShckVelDCRmvlLPFCoef.Value = 0.05;
DomainWhlInfo_ShckVelDCRmvlLPFCoef.Max = 50;
DomainWhlInfo_ShckVelDCRmvlLPFCoef.Min = 0;
DomainWhlInfo_ShckVelDCRmvlLPFCoef.Description = 'Description goes here';
DomainWhlInfo_ShckVelDCRmvlLPFCoef.Unit = 'Hz';
DomainWhlInfo_ShckVelDCRmvlLPFCoef.DataType = 'double';

assignin('base','DomainWhlInfo_ShckVelDCRmvlLPFCoef',DomainWhlInfo_ShckVelDCRmvlLPFCoef);

return

%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DomainWhlInfo_CalParams.m ends here

