function [] = Driver_Controls_Main(VehFlag,DispParam)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Filename : Driver_Controls_Main.m
%
% Created : Friday 14 April 2017 - 9:05:39 PM (Eastern Daylight Time)
%
% $Date: Sunday 07 May 2017 - 7:58:04 PM (Eastern Daylight Time)
%
% $Last Changed Author: park1j  $
%
% Created By : Juchirl Park 
% 
% Description : Generates the variables for the main simulink model
% incorporated with RTW and CarSim. It is also necessary load the
% parameters to run the simulink model. It displays the absolute maxima of the
% seat vertical accelerations and the absorbed power calculated at the end of
% the simulation
%
% VehFlag = Vehicle type defined in "Veh_parameters.m" - Curb, GVW, Dflt
% DispParam = Display the LQR gain into command window.
%
% Usage Example
%
% Driver_Controls_Main('Curb',1);
% Driver_Controls_Main('GVW',0);
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Code Start here

%Control variable for the Driver

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Normal operations - calculate the gains and load the values into
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Determine the sample time of the subsystem (model references)
assignin('base','Con_DomainWhlInfo_SampleTime',0.002);
assignin('base','Con_SensorProc_SampleTime',0.002);
assignin('base','Con_DomainBdyInfo_SampleTime',0.002);
assignin('base','Con_ScndryMtnCntrls_SampleTime',0.002);
assignin('base','Con_BdyMtnCntrls_SampleTime',0.002);
assignin('base','Con_WhlMtnCntrls_SampleTime',0.002);
assignin('base','Con_WhlSprngCntrls_SampleTime',0.002);
assignin('base','Con_LatBdyCntrls_SampleTime',0.002);
assignin('base','Con_LongBdyCntrls_SampleTime',0.002);
assignin('base','Con_ADFrcArbtn_SampleTime',0.002);

Cal_VehWghtMdSlctn = Simulink.Parameter;
Cal_VehWghtMdSlctn.Value = 2;
Cal_VehWghtMdSlctn.Max = 10;
Cal_VehWghtMdSlctn.Min = 0;
Cal_VehWghtMdSlctn.Description = 'Description goes here';
Cal_VehWghtMdSlctn.Unit = 'N/A';
Cal_VehWghtMdSlctn.DataType = 'uint8';

assignin('base','Cal_VehWghtMdSlctn',Cal_VehWghtMdSlctn);

Con_VehWghtMdCurb = Simulink.Parameter;
Con_VehWghtMdCurb.Value = 1;
Con_VehWghtMdCurb.Max = 1;
Con_VehWghtMdCurb.Min = 1;
Con_VehWghtMdCurb.Description = 'Description goes here';
Con_VehWghtMdCurb.Unit = 'N/A';
Con_VehWghtMdCurb.DataType = 'uint8';

assignin('base','Con_VehWghtMdCurb',Con_VehWghtMdCurb);

Con_VehWghtMdGVW = Simulink.Parameter;
Con_VehWghtMdGVW.Value = 2;
Con_VehWghtMdGVW.Max = 2;
Con_VehWghtMdGVW.Min = 2;
Con_VehWghtMdGVW.Description = 'Description goes here';
Con_VehWghtMdGVW.Unit = 'N/A';
Con_VehWghtMdGVW.DataType = 'uint8';

assignin('base','Con_VehWghtMdGVW',Con_VehWghtMdGVW);

Cal_DrvrInptMdSlctn = Simulink.Parameter;
Cal_DrvrInptMdSlctn.Value = 1;
Cal_DrvrInptMdSlctn.Max = 10;
Cal_DrvrInptMdSlctn.Min = 0;
Cal_DrvrInptMdSlctn.Description = 'Description goes here';
Cal_DrvrInptMdSlctn.Unit = 'N/A';
Cal_DrvrInptMdSlctn.DataType = 'uint8';

assignin('base','Cal_DrvrInptMdSlctn',Cal_DrvrInptMdSlctn);

Con_DrvrInptMdOffRoad = Simulink.Parameter;
Con_DrvrInptMdOffRoad.Value = 1;
Con_DrvrInptMdOffRoad.Max = 1;
Con_DrvrInptMdOffRoad.Min = 1;
Con_DrvrInptMdOffRoad.Description = 'Description goes here';
Con_DrvrInptMdOffRoad.Unit = 'N/A';
Con_DrvrInptMdOffRoad.DataType = 'uint8';

assignin('base','Con_DrvrInptMdOffRoad',Con_DrvrInptMdOffRoad);

Con_DrvrInptMdOnRoad = Simulink.Parameter;
Con_DrvrInptMdOnRoad.Value = 2;
Con_DrvrInptMdOnRoad.Max = 2;
Con_DrvrInptMdOnRoad.Min = 2;
Con_DrvrInptMdOnRoad.Description = 'Description goes here';
Con_DrvrInptMdOnRoad.Unit = 'N/A';
Con_DrvrInptMdOnRoad.DataType = 'uint8';

assignin('base','Con_DrvrInptMdOnRoad',Con_DrvrInptMdOnRoad);

Con_NoCntrlFrc = Simulink.Parameter;
Con_NoCntrlFrc.Value = 0;
Con_NoCntrlFrc.Max = 100000;
Con_NoCntrlFrc.Min = 0;
Con_NoCntrlFrc.Description = 'Description goes here';
Con_NoCntrlFrc.Unit = 'N';
Con_NoCntrlFrc.DataType = 'double';

assignin('base','Con_NoCntrlFrc',Con_NoCntrlFrc);

%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Load the parameters for simulink models
%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Perform the sensor characteristics']);

SensorProc_CalParams;

disp(['Perform the LQR gain calculation and load ',...
	'parameters into SIMULINK model']);

[K1,K2,K3] = LQR_Gain_Calc(1);
BdyMtnCntrls_CalParams(K1,K2,K3,VehFlag);
LongBdyCntrls_CalParams;
LatBdyCntrls_CalParams;

WhlMtnCntrls_CalParams(K1,K2,K3,VehFlag);
WhlSprngCntrls_CalParams(K1,K2,K3,VehFlag);

ADFrcArbtn_CalParams;

disp(['Load the sensor domain and characterization ',...
	'parameters into SIMULINK model']);

DomainBdyInfo_CalParams;
DomainWhlInfo_CalParams;
       

% 
%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Driver_Controls_Main.m ends here
