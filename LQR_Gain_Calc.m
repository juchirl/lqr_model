function [Ka1,Ka2,Ka3] = LQR_Gain_Calc(DisplayMode)

UseAccelIncluded = boolean(1);
R = eye(4);
Lambda = eye(14);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Curb vehicle weight mode
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%param = Veh_Parameters(Applctn,'Curb');  
[A1,Av,B1,Bv,C,Ca,D,E,param] = Veh_Full_Sys('Curb');      %  get the vehicle
Q1 = Ca'*Ca; %Active spring control including spring and damper
Q1(1:3,1:3) = Q1(1:3,1:3).*7.0e8;       %Body motion
Q1(8:11,8:11) = Q1(8:11,8:11).*7.0e7;    %Wheel Motion
Q1(12:14,12:14) = Q1(12:14,12:14).*8.0e7;    %Body position


[Pa1,La1,Ka1] = care(A1, B1, Q1);
Ka1 = R\B1'*Pa1*Lambda*Ca'/(Ca*Lambda*Ca');
Ka1 = round(Ka1);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GVW vehicle weight mode
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%param = Veh_Parameters(Applctn,'GVW');  
[A2,Av,B2,Bv,C,Ca2,D,E,param] = Veh_Full_Sys('GVW');
Q2 = Ca2'*Ca2;
Q2(1:3,1:3) = Q2(1:3,1:3).*9.5e9;       %Body motion
Q2(8:11,8:11) = Q2(8:11,8:11).*2.0e8;    %Wheel Motion
Q2(12:14,12:14) = Q2(12:14,12:14).*1.0e8;    %Body position

[Pa2,La2,Ka2] = care(A2, B2, Q2);
Ka2 = R\B2'*Pa2*Lambda*Ca2'/(Ca2*Lambda*Ca2'); % Acceleration Included
Ka2 = round(Ka2);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default vehicle weight mode
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%param = Veh_Parameters(Applctn,'Other');  
[A3,Av,B3,Bv,C,Ca3,D,E,param] = Veh_Full_Sys('Other');
Q3 = Ca3'*Ca3;
Q3(1:3,1:3) = Q3(1:3,1:3).*7.0e8;        %Body motion
Q3(8:11,8:11) = Q3(8:11,8:11).*7.0e7;    %Wheel motion
Q3(12:14,12:14) = Q3(12:14,12:14).*8.0e7;    %Body position

[Pa3,La3,Ka3] = care(A3, B3, Q3);      % Acceleration Included
Ka3 = R\B2'*Pa3*Lambda*Ca3'/(Ca3*Lambda*Ca3');
Ka3 = round(Ka3);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Eigen value calculation and display
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EigOri = eig(A1);
EigLQR(:,1) = La1;
EigLQR(:,2) = La2;
EigLQR(:,3) = La3;

if DisplayMode,                         % If user wants to display gain in command

	FID = 1;

	for j = 1:3,      % Assign the sign as char for displaying
  
		for i = 1:14,
  
			if imag(EigLQR(i,j)) >= 0,
				Signs(i,j) = '+';
			else
				Signs(i,j) = '-';
			end
    
		end
  
	end

	fprintf(FID,'\nEigenvalues of LQR are\n');
	fprintf(FID,'Curb weight Mode\t GVW weight Mode\t Default weight Mode\n');
	for i = 1:14,
  
		for j = 1:3,

			fprintf(FID,['%8.4f ',Signs(i,j),' %8.4fi,\t'],...
				real(EigLQR(i,j)),abs(imag(EigLQR(i,j))));
		end
  
		fprintf(FID,'\n');
  
	end

	[M,N] = size(Ka1);

	fprintf(FID,'\nCurb Mode Gains for minimization of the body accel. and vel.\n');

	for i = 1:M,

		for j = 1:N,

			fprintf(FID,'%8.0f\t',Ka1(i,j));

		end

		fprintf(FID,'\n');

	end

	fprintf(FID,'\nGVW Mode Gains for minimization of the body accel. and vel.\n');

	for i = 1:M,

		for j = 1:N,

			fprintf(FID,'%8.0f\t',Ka2(i,j));

		end

		fprintf(FID,'\n');

	end

	fprintf(FID,'\nOther Mode Gains for minimization of the body accel. and vel.\n');

	for i = 1:M,

		for j = 1:N,

			fprintf(FID,'%8.0f\t',Ka3(i,j));

		end

		fprintf(FID,'\n');

	end

end

