function [] = LatBdyCntrls_CalParams()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FileName : LatBdyCntrls_CalParams
%
% Created : Friday 14 April 2017 - 12:25:37 PM (Eastern Daylight Time)
%
% Updated on: Thursday 04 May 2017 - 11:05:26 AM (Eastern Daylight Time)
%
% Modified by: Juchirl Park
%
% Created By : Juchirl Park 
%
% Description : Load the human induced vehicle behavior control variables
% such as lateral control (Handling)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Code Start here
LatBdyCntrls_MaxCntrlFrc = Simulink.Parameter;
LatBdyCntrls_MaxCntrlFrc.Value = 50000;
LatBdyCntrls_MaxCntrlFrc.Max = 100000;
LatBdyCntrls_MaxCntrlFrc.Min = 0;
LatBdyCntrls_MaxCntrlFrc.Description = 'Description goes here';
LatBdyCntrls_MaxCntrlFrc.Unit = 'N';
LatBdyCntrls_MaxCntrlFrc.DataType = 'double';

assignin('base','LatBdyCntrls_MaxCntrlFrc',LatBdyCntrls_MaxCntrlFrc);

%Curb weight variables
LatBdyCntrls_HWAThrsCrb = Simulink.Parameter;
LatBdyCntrls_HWAThrsCrb.Value = 10;
LatBdyCntrls_HWAThrsCrb.Max = 50;
LatBdyCntrls_HWAThrsCrb.Min = 0;
LatBdyCntrls_HWAThrsCrb.Description = 'Description goes here';
LatBdyCntrls_HWAThrsCrb.Unit = 'deg';
LatBdyCntrls_HWAThrsCrb.DataType = 'double';

assignin('base','LatBdyCntrls_HWAThrsCrb',LatBdyCntrls_HWAThrsCrb);

LatBdyCntrls_HWARThrsCrb = Simulink.Parameter;
LatBdyCntrls_HWARThrsCrb.Value = 15;
LatBdyCntrls_HWARThrsCrb.Max = 50;
LatBdyCntrls_HWARThrsCrb.Min = 0;
LatBdyCntrls_HWARThrsCrb.Description = 'Description goes here';
LatBdyCntrls_HWARThrsCrb.Unit = 'deg/s';
LatBdyCntrls_HWARThrsCrb.DataType = 'double';

assignin('base','LatBdyCntrls_HWARThrsCrb',LatBdyCntrls_HWARThrsCrb);

LatBdyCntrls_YRThrsCrb = Simulink.Parameter;
LatBdyCntrls_YRThrsCrb.Value = 5.8;
LatBdyCntrls_YRThrsCrb.Max = 50;
LatBdyCntrls_YRThrsCrb.Min = 0;
LatBdyCntrls_YRThrsCrb.Description = 'Description goes here';
LatBdyCntrls_YRThrsCrb.Unit = 'deg/s';
LatBdyCntrls_YRThrsCrb.DataType = 'double';

assignin('base','LatBdyCntrls_YRThrsCrb',LatBdyCntrls_YRThrsCrb);

LatBdyCntrls_DlyTmCrb = Simulink.Parameter;
LatBdyCntrls_DlyTmCrb.Value = 0.2;
LatBdyCntrls_DlyTmCrb.Max = 5;
LatBdyCntrls_DlyTmCrb.Min = 0;
LatBdyCntrls_DlyTmCrb.Description = 'Description goes here';
LatBdyCntrls_DlyTmCrb.Unit = 'second';
LatBdyCntrls_DlyTmCrb.DataType = 'double';

assignin('base','LatBdyCntrls_DlyTmCrb',LatBdyCntrls_DlyTmCrb);

LatBdyCntrls_AcclGnCrbOffRd = Simulink.Parameter;
LatBdyCntrls_AcclGnCrbOffRd.Value = 0.3;
LatBdyCntrls_AcclGnCrbOffRd.Max = 5;
LatBdyCntrls_AcclGnCrbOffRd.Min = 0;
LatBdyCntrls_AcclGnCrbOffRd.Description = 'Description goes here';
LatBdyCntrls_AcclGnCrbOffRd.Unit = 'N/A';
LatBdyCntrls_AcclGnCrbOffRd.DataType = 'double';

assignin('base','LatBdyCntrls_AcclGnCrbOffRd',LatBdyCntrls_AcclGnCrbOffRd);

LatBdyCntrls_AcclGnCrbOnRd = Simulink.Parameter;
LatBdyCntrls_AcclGnCrbOnRd.Value = 0.9;
LatBdyCntrls_AcclGnCrbOnRd.Max = 5;
LatBdyCntrls_AcclGnCrbOnRd.Min = 0;
LatBdyCntrls_AcclGnCrbOnRd.Description = 'Description goes here';
LatBdyCntrls_AcclGnCrbOnRd.Unit = 'N/A';
LatBdyCntrls_AcclGnCrbOnRd.DataType = 'double';

assignin('base','LatBdyCntrls_AcclGnCrbOnRd',LatBdyCntrls_AcclGnCrbOnRd);

LatBdyCntrls_AcclGnCrbRst = Simulink.Parameter;
LatBdyCntrls_AcclGnCrbRst.Value = 1.2;
LatBdyCntrls_AcclGnCrbRst.Max = 5;
LatBdyCntrls_AcclGnCrbRst.Min = 0;
LatBdyCntrls_AcclGnCrbRst.Description = 'Description goes here';
LatBdyCntrls_AcclGnCrbRst.Unit = 'N/A';
LatBdyCntrls_AcclGnCrbRst.DataType = 'double';

assignin('base','LatBdyCntrls_AcclGnCrbRst',LatBdyCntrls_AcclGnCrbRst);

%GVW weight variables
LatBdyCntrls_HWAThrsGVW = Simulink.Parameter;
LatBdyCntrls_HWAThrsGVW.Value = 10;
LatBdyCntrls_HWAThrsGVW.Max = 50;
LatBdyCntrls_HWAThrsGVW.Min = 0;
LatBdyCntrls_HWAThrsGVW.Description = 'Description goes here';
LatBdyCntrls_HWAThrsGVW.Unit = 'deg';
LatBdyCntrls_HWAThrsGVW.DataType = 'double';

assignin('base','LatBdyCntrls_HWAThrsGVW',LatBdyCntrls_HWAThrsGVW);

LatBdyCntrls_HWARThrsGVW = Simulink.Parameter;
LatBdyCntrls_HWARThrsGVW.Value = 15;
LatBdyCntrls_HWARThrsGVW.Max = 50;
LatBdyCntrls_HWARThrsGVW.Min = 0;
LatBdyCntrls_HWARThrsGVW.Description = 'Description goes here';
LatBdyCntrls_HWARThrsGVW.Unit = 'deg/s';
LatBdyCntrls_HWARThrsGVW.DataType = 'double';

assignin('base','LatBdyCntrls_HWARThrsGVW',LatBdyCntrls_HWARThrsGVW);

LatBdyCntrls_YRThrsGVW = Simulink.Parameter;
LatBdyCntrls_YRThrsGVW.Value = 5.8;
LatBdyCntrls_YRThrsGVW.Max = 50;
LatBdyCntrls_YRThrsGVW.Min = 0;
LatBdyCntrls_YRThrsGVW.Description = 'Description goes here';
LatBdyCntrls_YRThrsGVW.Unit = 'deg/s';
LatBdyCntrls_YRThrsGVW.DataType = 'double';

assignin('base','LatBdyCntrls_YRThrsGVW',LatBdyCntrls_YRThrsGVW);

LatBdyCntrls_DlyTmGVW = Simulink.Parameter;
LatBdyCntrls_DlyTmGVW.Value = 0.2;
LatBdyCntrls_DlyTmGVW.Max = 5;
LatBdyCntrls_DlyTmGVW.Min = 0;
LatBdyCntrls_DlyTmGVW.Description = 'Description goes here';
LatBdyCntrls_DlyTmGVW.Unit = 'second';
LatBdyCntrls_DlyTmGVW.DataType = 'double';

assignin('base','LatBdyCntrls_DlyTmGVW',LatBdyCntrls_DlyTmGVW);

LatBdyCntrls_AcclGnGVWOffRd = Simulink.Parameter;
LatBdyCntrls_AcclGnGVWOffRd.Value = 0.3;
LatBdyCntrls_AcclGnGVWOffRd.Max = 5;
LatBdyCntrls_AcclGnGVWOffRd.Min = 0;
LatBdyCntrls_AcclGnGVWOffRd.Description = 'Description goes here';
LatBdyCntrls_AcclGnGVWOffRd.Unit = 'N/A';
LatBdyCntrls_AcclGnGVWOffRd.DataType = 'double';

assignin('base','LatBdyCntrls_AcclGnGVWOffRd',LatBdyCntrls_AcclGnGVWOffRd);

LatBdyCntrls_AcclGnGVWOnRd = Simulink.Parameter;
LatBdyCntrls_AcclGnGVWOnRd.Value = 0.5;
LatBdyCntrls_AcclGnGVWOnRd.Max = 5;
LatBdyCntrls_AcclGnGVWOnRd.Min = 0;
LatBdyCntrls_AcclGnGVWOnRd.Description = 'Description goes here';
LatBdyCntrls_AcclGnGVWOnRd.Unit = 'N/A';
LatBdyCntrls_AcclGnGVWOnRd.DataType = 'double';

assignin('base','LatBdyCntrls_AcclGnGVWOnRd',LatBdyCntrls_AcclGnGVWOnRd);

LatBdyCntrls_AcclGnGVWRst = Simulink.Parameter;
LatBdyCntrls_AcclGnGVWRst.Value = 0.6;
LatBdyCntrls_AcclGnGVWRst.Max = 5;
LatBdyCntrls_AcclGnGVWRst.Min = 0;
LatBdyCntrls_AcclGnGVWRst.Description = 'Description goes here';
LatBdyCntrls_AcclGnGVWRst.Unit = 'N/A';
LatBdyCntrls_AcclGnGVWRst.DataType = 'double';

assignin('base','LatBdyCntrls_AcclGnGVWRst',LatBdyCntrls_AcclGnGVWRst);

%Default weight variables
LatBdyCntrls_HWAThrsRst = Simulink.Parameter;
LatBdyCntrls_HWAThrsRst.Value = 10;
LatBdyCntrls_HWAThrsRst.Max = 50;
LatBdyCntrls_HWAThrsRst.Min = 0;
LatBdyCntrls_HWAThrsRst.Description = 'Description goes here';
LatBdyCntrls_HWAThrsRst.Unit = 'deg';
LatBdyCntrls_HWAThrsRst.DataType = 'double';

assignin('base','LatBdyCntrls_HWAThrsRst',LatBdyCntrls_HWAThrsRst);

LatBdyCntrls_HWARThrsRst = Simulink.Parameter;
LatBdyCntrls_HWARThrsRst.Value = 15;
LatBdyCntrls_HWARThrsRst.Max = 50;
LatBdyCntrls_HWARThrsRst.Min = 0;
LatBdyCntrls_HWARThrsRst.Description = 'Description goes here';
LatBdyCntrls_HWARThrsRst.Unit = 'deg/s';
LatBdyCntrls_HWARThrsRst.DataType = 'double';

assignin('base','LatBdyCntrls_HWARThrsRst',LatBdyCntrls_HWARThrsRst);

LatBdyCntrls_YRThrsRst = Simulink.Parameter;
LatBdyCntrls_YRThrsRst.Value = 5.8;
LatBdyCntrls_YRThrsRst.Max = 50;
LatBdyCntrls_YRThrsRst.Min = 0;
LatBdyCntrls_YRThrsRst.Description = 'Description goes here';
LatBdyCntrls_YRThrsRst.Unit = 'deg/s';
LatBdyCntrls_YRThrsRst.DataType = 'double';

assignin('base','LatBdyCntrls_YRThrsRst',LatBdyCntrls_YRThrsRst);

LatBdyCntrls_DlyTmRst = Simulink.Parameter;
LatBdyCntrls_DlyTmRst.Value = 0.2;
LatBdyCntrls_DlyTmRst.Max = 5;
LatBdyCntrls_DlyTmRst.Min = 0;
LatBdyCntrls_DlyTmRst.Description = 'Description goes here';
LatBdyCntrls_DlyTmRst.Unit = 'second';
LatBdyCntrls_DlyTmRst.DataType = 'double';

assignin('base','LatBdyCntrls_DlyTmRst',LatBdyCntrls_DlyTmRst);

LatBdyCntrls_LatAcclGnRst = Simulink.Parameter;
LatBdyCntrls_LatAcclGnRst.Value = 1.7;
LatBdyCntrls_LatAcclGnRst.Max = 5;
LatBdyCntrls_LatAcclGnRst.Min = 0;
LatBdyCntrls_LatAcclGnRst.Description = 'Description goes here';
LatBdyCntrls_LatAcclGnRst.Unit = 'N/A';
LatBdyCntrls_LatAcclGnRst.DataType = 'double';

assignin('base','LatBdyCntrls_LatAcclGnRst',LatBdyCntrls_LatAcclGnRst);

LatBdyCntrls_HWADCRmvlLPFCoef = Simulink.Parameter;
LatBdyCntrls_HWADCRmvlLPFCoef.Value = 0.07;
LatBdyCntrls_HWADCRmvlLPFCoef.Max = 85;
LatBdyCntrls_HWADCRmvlLPFCoef.Min = 0;
LatBdyCntrls_HWADCRmvlLPFCoef.Description = 'Description goes here';
LatBdyCntrls_HWADCRmvlLPFCoef.Unit = 'Hz';
LatBdyCntrls_HWADCRmvlLPFCoef.DataType = 'double';

assignin('base','LatBdyCntrls_HWADCRmvlLPFCoef',LatBdyCntrls_HWADCRmvlLPFCoef);

LatBdyCntrls_MinVehSpd = Simulink.Parameter;
LatBdyCntrls_MinVehSpd.Value = 0.27;
LatBdyCntrls_MinVehSpd.Max = 5;
LatBdyCntrls_MinVehSpd.Min = 0;
LatBdyCntrls_MinVehSpd.Description = 'Description goes here';
LatBdyCntrls_MinVehSpd.Unit = 'm/s';
LatBdyCntrls_MinVehSpd.DataType = 'double';

assignin('base','LatBdyCntrls_MinVehSpd',LatBdyCntrls_MinVehSpd);

LatBdyCntrls_UseRllRt = Simulink.Parameter;
LatBdyCntrls_UseRllRt.Value = 0;
LatBdyCntrls_UseRllRt.Max = 1;
LatBdyCntrls_UseRllRt.Min = 0;
LatBdyCntrls_UseRllRt.Description = 'Description goes here';
LatBdyCntrls_UseRllRt.Unit = 'N/A';
LatBdyCntrls_UseRllRt.DataType = 'boolean';

assignin('base','LatBdyCntrls_UseRllRt',LatBdyCntrls_UseRllRt);

LatBdyCntrls_UseYR = Simulink.Parameter;
LatBdyCntrls_UseYR.Value = 1;
LatBdyCntrls_UseYR.Max = 1;
LatBdyCntrls_UseYR.Min = 0;
LatBdyCntrls_UseYR.Description = 'Description goes here';
LatBdyCntrls_UseYR.Unit = 'N/A';
LatBdyCntrls_UseYR.DataType = 'boolean';

assignin('base','LatBdyCntrls_UseYR',LatBdyCntrls_UseYR);


return

%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AC_HmnIndcdCntrl.m ends here

