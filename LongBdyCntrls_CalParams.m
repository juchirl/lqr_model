function [] = LongBdyCntrls_CalParam()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FileName : LongBdyCntrls_CalParam
%
% Created : Friday 14 April 2017 - 12:25:37 PM (Eastern Daylight Time)
%
% Updated on: Thursday 04 May 2017 - 10:06:18 AM (Eastern Daylight Time)
%
% Modified by: Juchirl Park
%
% Created By : Juchirl Park 
%
% Description : Load the human induced vehicle behavior control variables
% such as longitudinal controls, lateral control (Handling)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Code Start here

LongBdyCntrls_MaxCntrlFrc = Simulink.Parameter;
LongBdyCntrls_MaxCntrlFrc.Value = 50000;
LongBdyCntrls_MaxCntrlFrc.Max = 100000;
LongBdyCntrls_MaxCntrlFrc.Min = 0;
LongBdyCntrls_MaxCntrlFrc.Description = 'Description goes here';
LongBdyCntrls_MaxCntrlFrc.Unit = 'N';
LongBdyCntrls_MaxCntrlFrc.DataType = 'double';

assignin('base','LongBdyCntrls_MaxCntrlFrc',LongBdyCntrls_MaxCntrlFrc);

%Curb weight variable

LongBdyCntrls_CntrlPtchThrsCrb = Simulink.Parameter;
LongBdyCntrls_CntrlPtchThrsCrb.Value = 0.05;
LongBdyCntrls_CntrlPtchThrsCrb.Max = 1;
LongBdyCntrls_CntrlPtchThrsCrb.Min = 0;
LongBdyCntrls_CntrlPtchThrsCrb.Description = 'Description goes here';
LongBdyCntrls_CntrlPtchThrsCrb.Unit = 'g';
LongBdyCntrls_CntrlPtchThrsCrb.DataType = 'double';

assignin('base','LongBdyCntrls_CntrlPtchThrsCrb',LongBdyCntrls_CntrlPtchThrsCrb);

LongBdyCntrls_CntrlLngThrsCrb = Simulink.Parameter;
LongBdyCntrls_CntrlLngThrsCrb.Value = 0.05;
LongBdyCntrls_CntrlLngThrsCrb.Max = 1;
LongBdyCntrls_CntrlLngThrsCrb.Min = 0;
LongBdyCntrls_CntrlLngThrsCrb.Description = 'Description goes here';
LongBdyCntrls_CntrlLngThrsCrb.Unit = 'g';
LongBdyCntrls_CntrlLngThrsCrb.DataType = 'double';

assignin('base','LongBdyCntrls_CntrlLngThrsCrb',LongBdyCntrls_CntrlLngThrsCrb);

LongBdyCntrls_PtchGnCrbOffRd = Simulink.Parameter;
LongBdyCntrls_PtchGnCrbOffRd.Value = 200;
LongBdyCntrls_PtchGnCrbOffRd.Max = 5000;
LongBdyCntrls_PtchGnCrbOffRd.Min = 0;
LongBdyCntrls_PtchGnCrbOffRd.Description = 'Description goes here';
LongBdyCntrls_PtchGnCrbOffRd.Unit = 'N/A';
LongBdyCntrls_PtchGnCrbOffRd.DataType = 'double';

assignin('base','LongBdyCntrls_PtchGnCrbOffRd',LongBdyCntrls_PtchGnCrbOffRd);

LongBdyCntrls_PtchGnCrbOnRd = Simulink.Parameter;
LongBdyCntrls_PtchGnCrbOnRd.Value = 300;
LongBdyCntrls_PtchGnCrbOnRd.Max = 5000;
LongBdyCntrls_PtchGnCrbOnRd.Min = 0;
LongBdyCntrls_PtchGnCrbOnRd.Description = 'Description goes here';
LongBdyCntrls_PtchGnCrbOnRd.Unit = 'N/A';
LongBdyCntrls_PtchGnCrbOnRd.DataType = 'double';

assignin('base','LongBdyCntrls_PtchGnCrbOnRd',LongBdyCntrls_PtchGnCrbOnRd);

LongBdyCntrls_PtchGnCrbRst = Simulink.Parameter;
LongBdyCntrls_PtchGnCrbRst.Value = 400;
LongBdyCntrls_PtchGnCrbRst.Max = 5000;
LongBdyCntrls_PtchGnCrbRst.Min = 0;
LongBdyCntrls_PtchGnCrbRst.Description = 'Description goes here';
LongBdyCntrls_PtchGnCrbRst.Unit = 'N/A';
LongBdyCntrls_PtchGnCrbRst.DataType = 'double';

assignin('base','LongBdyCntrls_PtchGnCrbRst',LongBdyCntrls_PtchGnCrbRst);

%For GVW

LongBdyCntrls_CntrlPtchThrsGVW = Simulink.Parameter;
LongBdyCntrls_CntrlPtchThrsGVW.Value = 0.05;
LongBdyCntrls_CntrlPtchThrsGVW.Max = 1;
LongBdyCntrls_CntrlPtchThrsGVW.Min = 0;
LongBdyCntrls_CntrlPtchThrsGVW.Description = 'Description goes here';
LongBdyCntrls_CntrlPtchThrsGVW.Unit = 'g';
LongBdyCntrls_CntrlPtchThrsGVW.DataType = 'double';

assignin('base','LongBdyCntrls_CntrlPtchThrsGVW',LongBdyCntrls_CntrlPtchThrsGVW);

LongBdyCntrls_CntrlLngThrsGVW = Simulink.Parameter;
LongBdyCntrls_CntrlLngThrsGVW.Value = 0.05;
LongBdyCntrls_CntrlLngThrsGVW.Max = 1;
LongBdyCntrls_CntrlLngThrsGVW.Min = 0;
LongBdyCntrls_CntrlLngThrsGVW.Description = 'Description goes here';
LongBdyCntrls_CntrlLngThrsGVW.Unit = 'g';
LongBdyCntrls_CntrlLngThrsGVW.DataType = 'double';

assignin('base','LongBdyCntrls_CntrlLngThrsGVW',LongBdyCntrls_CntrlLngThrsGVW);

LongBdyCntrls_PtchGnGVWOffRd = Simulink.Parameter;
LongBdyCntrls_PtchGnGVWOffRd.Value = 200;
LongBdyCntrls_PtchGnGVWOffRd.Max = 5000;
LongBdyCntrls_PtchGnGVWOffRd.Min = 0;
LongBdyCntrls_PtchGnGVWOffRd.Description = 'Description goes here';
LongBdyCntrls_PtchGnGVWOffRd.Unit = 'N/A';
LongBdyCntrls_PtchGnGVWOffRd.DataType = 'double';

assignin('base','LongBdyCntrls_PtchGnGVWOffRd',LongBdyCntrls_PtchGnGVWOffRd);

LongBdyCntrls_PtchGnGVWOnRd = Simulink.Parameter;
LongBdyCntrls_PtchGnGVWOnRd.Value = 300;
LongBdyCntrls_PtchGnGVWOnRd.Max = 5000;
LongBdyCntrls_PtchGnGVWOnRd.Min = 0;
LongBdyCntrls_PtchGnGVWOnRd.Description = 'Description goes here';
LongBdyCntrls_PtchGnGVWOnRd.Unit = 'N/A';
LongBdyCntrls_PtchGnGVWOnRd.DataType = 'double';

assignin('base','LongBdyCntrls_PtchGnGVWOnRd',LongBdyCntrls_PtchGnGVWOnRd);

LongBdyCntrls_PtchGnGVWRst = Simulink.Parameter;
LongBdyCntrls_PtchGnGVWRst.Value = 400;
LongBdyCntrls_PtchGnGVWRst.Max = 5000;
LongBdyCntrls_PtchGnGVWRst.Min = 0;
LongBdyCntrls_PtchGnGVWRst.Description = 'Description goes here';
LongBdyCntrls_PtchGnGVWRst.Unit = 'N/A';
LongBdyCntrls_PtchGnGVWRst.DataType = 'double';

assignin('base','LongBdyCntrls_PtchGnGVWRst',LongBdyCntrls_PtchGnGVWRst);

%Rest
LongBdyCntrls_CntrlPtchThrsRst = Simulink.Parameter;
LongBdyCntrls_CntrlPtchThrsRst.Value = 0.05;
LongBdyCntrls_CntrlPtchThrsRst.Max = 1;
LongBdyCntrls_CntrlPtchThrsRst.Min = 0;
LongBdyCntrls_CntrlPtchThrsRst.Description = 'Description goes here';
LongBdyCntrls_CntrlPtchThrsRst.Unit = 'g';
LongBdyCntrls_CntrlPtchThrsRst.DataType = 'double';

assignin('base','LongBdyCntrls_CntrlPtchThrsRst',LongBdyCntrls_CntrlPtchThrsRst);

LongBdyCntrls_CntrlLngThrsRst = Simulink.Parameter;
LongBdyCntrls_CntrlLngThrsRst.Value = 0.05;
LongBdyCntrls_CntrlLngThrsRst.Max = 1;
LongBdyCntrls_CntrlLngThrsRst.Min = 0;
LongBdyCntrls_CntrlLngThrsRst.Description = 'Description goes here';
LongBdyCntrls_CntrlLngThrsRst.Unit = 'g';
LongBdyCntrls_CntrlLngThrsRst.DataType = 'double';

assignin('base','LongBdyCntrls_CntrlLngThrsRst',LongBdyCntrls_CntrlLngThrsRst);

LongBdyCntrls_PtchGnRst = Simulink.Parameter;
LongBdyCntrls_PtchGnRst.Value = 500;
LongBdyCntrls_PtchGnRst.Max = 5000;
LongBdyCntrls_PtchGnRst.Min = 0;
LongBdyCntrls_PtchGnRst.Description = 'Description goes here';
LongBdyCntrls_PtchGnRst.Unit = 'N/A';
LongBdyCntrls_PtchGnRst.DataType = 'double';

assignin('base','LongBdyCntrls_PtchGnRst',LongBdyCntrls_PtchGnRst);

LongBdyCntrls_FrcFrntSgnMltplr = Simulink.Parameter;
LongBdyCntrls_FrcFrntSgnMltplr.Value = 1;
LongBdyCntrls_FrcFrntSgnMltplr.Max = 1;
LongBdyCntrls_FrcFrntSgnMltplr.Min = -1;
LongBdyCntrls_FrcFrntSgnMltplr.Description = 'Description goes here';
LongBdyCntrls_FrcFrntSgnMltplr.Unit = 'N/A';
LongBdyCntrls_FrcFrntSgnMltplr.DataType = 'double';

assignin('base','LongBdyCntrls_FrcFrntSgnMltplr',LongBdyCntrls_FrcFrntSgnMltplr);

LongBdyCntrls_CntrlMaxFrc = Simulink.Parameter;
LongBdyCntrls_CntrlMaxFrc.Value = 75000;
LongBdyCntrls_CntrlMaxFrc.Max = 100000;
LongBdyCntrls_CntrlMaxFrc.Min = 0;
LongBdyCntrls_CntrlMaxFrc.Description = 'Description goes here';
LongBdyCntrls_CntrlMaxFrc.Unit = 'N';
LongBdyCntrls_CntrlMaxFrc.DataType = 'double';

assignin('base','LongBdyCntrls_CntrlMaxFrc',LongBdyCntrls_CntrlMaxFrc);

return

%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LongBdyCntrls_CalParams.m ends here

