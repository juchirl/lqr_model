function [] = removeAttributes(Model)
%
%
%

AllBlks = find_system(Model,'Followlinks','on','LookUnderMasks','off','RegExp','on','Type','Block');

% set attribute for constant block
L = length(AllBlks);

for i = 1:L,

	set_param(AllBlks{i},'AttributesFormatString',' ');
	set_param(AllBlks{i},'ShowName','on');

end

return

