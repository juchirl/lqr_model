function [] = SensorProc_CalParams()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Filename : SensorProc_CalParams
%
% Created : Monday 17 April 2017 - 7:59:46 PM (Eastern Daylight Time)
%
% Date: Sunday 07 May 2017 - 7:58:46 PM (Eastern Daylight Time)
%
% Created By : Juchirl Park 
% 
% Usage Example
%
% SensorProc_CalParams;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SensorProc_SuspnDisp_Scale_FL = Simulink.Parameter;
SensorProc_SuspnDisp_Scale_FL.Value = 1;
SensorProc_SuspnDisp_Scale_FL.Max = 10;
SensorProc_SuspnDisp_Scale_FL.Min = 0;
SensorProc_SuspnDisp_Scale_FL.Description = 'Description goes here';
SensorProc_SuspnDisp_Scale_FL.Unit = 'N/A';
SensorProc_SuspnDisp_Scale_FL.DataType = 'double';

assignin('base','SensorProc_SuspnDisp_Scale_FL',SensorProc_SuspnDisp_Scale_FL);

SensorProc_SuspnDisp_Scale_FR = Simulink.Parameter;
SensorProc_SuspnDisp_Scale_FR.Value = 1;
SensorProc_SuspnDisp_Scale_FR.Max = 10;
SensorProc_SuspnDisp_Scale_FR.Min = 0;
SensorProc_SuspnDisp_Scale_FR.Description = 'Description goes here';
SensorProc_SuspnDisp_Scale_FR.Unit = 'N/A';
SensorProc_SuspnDisp_Scale_FR.DataType = 'double';

assignin('base','SensorProc_SuspnDisp_Scale_FR',SensorProc_SuspnDisp_Scale_FR);

SensorProc_SuspnDisp_Scale_RL = Simulink.Parameter;
SensorProc_SuspnDisp_Scale_RL.Value = 1;
SensorProc_SuspnDisp_Scale_RL.Max = 10;
SensorProc_SuspnDisp_Scale_RL.Min = 0;
SensorProc_SuspnDisp_Scale_RL.Description = 'Description goes here';
SensorProc_SuspnDisp_Scale_RL.Unit = 'N/A';
SensorProc_SuspnDisp_Scale_RL.DataType = 'double';

assignin('base','SensorProc_SuspnDisp_Scale_RL',SensorProc_SuspnDisp_Scale_RL);

SensorProc_SuspnDisp_Scale_RR = Simulink.Parameter;
SensorProc_SuspnDisp_Scale_RR.Value = 1;
SensorProc_SuspnDisp_Scale_RR.Max = 10;
SensorProc_SuspnDisp_Scale_RR.Min = 0;
SensorProc_SuspnDisp_Scale_RR.Description = 'Description goes here';
SensorProc_SuspnDisp_Scale_RR.Unit = 'N/A';
SensorProc_SuspnDisp_Scale_RR.DataType = 'double';

assignin('base','SensorProc_SuspnDisp_Scale_RR',SensorProc_SuspnDisp_Scale_RR);

SensorProc_SuspnDisp_Enable = Simulink.Parameter;
SensorProc_SuspnDisp_Enable.Value = 0;
SensorProc_SuspnDisp_Enable.Max = 1;
SensorProc_SuspnDisp_Enable.Min = 0;
SensorProc_SuspnDisp_Enable.Description = 'Description goes here';
SensorProc_SuspnDisp_Enable.Unit = 'N/A';
SensorProc_SuspnDisp_Enable.DataType = 'boolean';

assignin('base','SensorProc_SuspnDisp_Enable',SensorProc_SuspnDisp_Enable);

SensorProc_SuspnDisp_Offset_FL = Simulink.Parameter;
SensorProc_SuspnDisp_Offset_FL.Value = 0;
SensorProc_SuspnDisp_Offset_FL.Max = 100;
SensorProc_SuspnDisp_Offset_FL.Min = 0;
SensorProc_SuspnDisp_Offset_FL.Description = 'Description goes here';
SensorProc_SuspnDisp_Offset_FL.Unit = 'm';
SensorProc_SuspnDisp_Offset_FL.DataType = 'double';

assignin('base','SensorProc_SuspnDisp_Offset_FL',SensorProc_SuspnDisp_Offset_FL);

SensorProc_SuspnDisp_Offset_FR = Simulink.Parameter;
SensorProc_SuspnDisp_Offset_FR.Value = 0;
SensorProc_SuspnDisp_Offset_FR.Max = 100;
SensorProc_SuspnDisp_Offset_FR.Min = 0;
SensorProc_SuspnDisp_Offset_FR.Description = 'Description goes here';
SensorProc_SuspnDisp_Offset_FR.Unit = 'm';
SensorProc_SuspnDisp_Offset_FR.DataType = 'double';

assignin('base','SensorProc_SuspnDisp_Offset_FR',SensorProc_SuspnDisp_Offset_FR);

SensorProc_SuspnDisp_Offset_RL = Simulink.Parameter;
SensorProc_SuspnDisp_Offset_RL.Value = 0;
SensorProc_SuspnDisp_Offset_RL.Max = 100;
SensorProc_SuspnDisp_Offset_RL.Min = 0;
SensorProc_SuspnDisp_Offset_RL.Description = 'Description goes here';
SensorProc_SuspnDisp_Offset_RL.Unit = 'm';
SensorProc_SuspnDisp_Offset_RL.DataType = 'double';

assignin('base','SensorProc_SuspnDisp_Offset_RL',SensorProc_SuspnDisp_Offset_RL);

SensorProc_SuspnDisp_Offset_RR = Simulink.Parameter;
SensorProc_SuspnDisp_Offset_RR.Value = 0;
SensorProc_SuspnDisp_Offset_RR.Max = 100;
SensorProc_SuspnDisp_Offset_RR.Min = 0;
SensorProc_SuspnDisp_Offset_RR.Description = 'Description goes here';
SensorProc_SuspnDisp_Offset_RR.Unit = 'm';
SensorProc_SuspnDisp_Offset_RR.DataType = 'double';

assignin('base','SensorProc_SuspnDisp_Offset_RR',SensorProc_SuspnDisp_Offset_RR);

%Body acceleration

SensorProc_AccelFilterCoef = Simulink.Parameter;
SensorProc_AccelFilterCoef.Value = 4;
SensorProc_AccelFilterCoef.Max = 80;
SensorProc_AccelFilterCoef.Min = 0;
SensorProc_AccelFilterCoef.Description = 'Description goes here';
SensorProc_AccelFilterCoef.Unit = 'Hz';
SensorProc_AccelFilterCoef.DataType = 'double';

assignin('base','SensorProc_AccelFilterCoef',SensorProc_AccelFilterCoef);

SensorProc_BodyAccelAxOffset = Simulink.Parameter;
SensorProc_BodyAccelAxOffset.Value = 0.1;
SensorProc_BodyAccelAxOffset.Max = 1;
SensorProc_BodyAccelAxOffset.Min = 0;
SensorProc_BodyAccelAxOffset.Description = 'Description goes here';
SensorProc_BodyAccelAxOffset.Unit = 'g';
SensorProc_BodyAccelAxOffset.DataType = 'double';

assignin('base','SensorProc_BodyAccelAxOffset',SensorProc_BodyAccelAxOffset);

SensorProc_BodyAccelAyOffset = Simulink.Parameter;
SensorProc_BodyAccelAyOffset.Value = 0.1;
SensorProc_BodyAccelAyOffset.Max = 1;
SensorProc_BodyAccelAyOffset.Min = 0;
SensorProc_BodyAccelAyOffset.Description = 'Description goes here';
SensorProc_BodyAccelAyOffset.Unit = 'g';
SensorProc_BodyAccelAyOffset.DataType = 'double';

assignin('base','SensorProc_BodyAccelAyOffset',SensorProc_BodyAccelAyOffset);

SensorProc_BodyAccelAzOffset = Simulink.Parameter;
SensorProc_BodyAccelAzOffset.Value = 0.1;
SensorProc_BodyAccelAzOffset.Max = 1;
SensorProc_BodyAccelAzOffset.Min = 0;
SensorProc_BodyAccelAzOffset.Description = 'Description goes here';
SensorProc_BodyAccelAzOffset.Unit = 'g';
SensorProc_BodyAccelAzOffset.DataType = 'double';

assignin('base','SensorProc_BodyAccelAzOffset',SensorProc_BodyAccelAzOffset);

%Body IMU sensor information
SensorProc_BodyYawRateOffset = Simulink.Parameter;
SensorProc_BodyYawRateOffset.Value = 0.1;
SensorProc_BodyYawRateOffset.Max = 1;
SensorProc_BodyYawRateOffset.Min = 0;
SensorProc_BodyYawRateOffset.Description = 'Description goes here';
SensorProc_BodyYawRateOffset.Unit = 'deg/s';
SensorProc_BodyYawRateOffset.DataType = 'double';

assignin('base','SensorProc_BodyYawRateOffset',SensorProc_BodyYawRateOffset);

SensorProc_BodyRollRateOffset = Simulink.Parameter;
SensorProc_BodyRollRateOffset.Value = 0.1;
SensorProc_BodyRollRateOffset.Max = 1;
SensorProc_BodyRollRateOffset.Min = 0;
SensorProc_BodyRollRateOffset.Description = 'Description goes here';
SensorProc_BodyRollRateOffset.Unit = 'deg/s';
SensorProc_BodyRollRateOffset.DataType = 'double';

assignin('base','SensorProc_BodyRollRateOffset',SensorProc_BodyRollRateOffset);

SensorProc_BodyPitchRateOffset = Simulink.Parameter;
SensorProc_BodyPitchRateOffset.Value = 0.1;
SensorProc_BodyPitchRateOffset.Max = 1;
SensorProc_BodyPitchRateOffset.Min = 0;
SensorProc_BodyPitchRateOffset.Description = 'Description goes here';
SensorProc_BodyPitchRateOffset.Unit = 'deg/s';
SensorProc_BodyPitchRateOffset.DataType = 'double';

assignin('base','SensorProc_BodyPitchRateOffset',SensorProc_BodyPitchRateOffset);

SensorProc_BodyYawRate_LPFCoef = Simulink.Parameter;
SensorProc_BodyYawRate_LPFCoef.Value = 5;
SensorProc_BodyYawRate_LPFCoef.Max = 85;
SensorProc_BodyYawRate_LPFCoef.Min = 0;
SensorProc_BodyYawRate_LPFCoef.Description = 'Description goes here';
SensorProc_BodyYawRate_LPFCoef.Unit = 'Hz';
SensorProc_BodyYawRate_LPFCoef.DataType = 'double';

assignin('base','SensorProc_BodyYawRate_LPFCoef',SensorProc_BodyYawRate_LPFCoef);

SensorProc_BodyRollRate_LPFCoef = Simulink.Parameter;
SensorProc_BodyRollRate_LPFCoef.Value = 5;
SensorProc_BodyRollRate_LPFCoef.Max = 85;
SensorProc_BodyRollRate_LPFCoef.Min = 0;
SensorProc_BodyRollRate_LPFCoef.Description = 'Description goes here';
SensorProc_BodyRollRate_LPFCoef.Unit = 'Hz';
SensorProc_BodyRollRate_LPFCoef.DataType = 'double';

assignin('base','SensorProc_BodyRollRate_LPFCoef',SensorProc_BodyRollRate_LPFCoef);

SensorProc_BodyPitchRate_LPFCoef = Simulink.Parameter;
SensorProc_BodyPitchRate_LPFCoef.Value = 5;
SensorProc_BodyPitchRate_LPFCoef.Max = 85;
SensorProc_BodyPitchRate_LPFCoef.Min = 0;
SensorProc_BodyPitchRate_LPFCoef.Description = 'Description goes here';
SensorProc_BodyPitchRate_LPFCoef.Unit = 'Hz';
SensorProc_BodyPitchRate_LPFCoef.DataType = 'double';

assignin('base','SensorProc_BodyPitchRate_LPFCoef',SensorProc_BodyPitchRate_LPFCoef);

SensorProc_BodyRate_DeadZoneThd = Simulink.Parameter;
SensorProc_BodyRate_DeadZoneThd.Value = 3;
SensorProc_BodyRate_DeadZoneThd.Max = 85;
SensorProc_BodyRate_DeadZoneThd.Min = 0;
SensorProc_BodyRate_DeadZoneThd.Description = 'Description goes here';
SensorProc_BodyRate_DeadZoneThd.Unit = 'deg/s';
SensorProc_BodyRate_DeadZoneThd.DataType = 'double';

assignin('base','SensorProc_BodyRate_DeadZoneThd',SensorProc_BodyRate_DeadZoneThd);

%Acceleration information
SensorProc_SprungAccel_Offset_FL = Simulink.Parameter;
SensorProc_SprungAccel_Offset_FL.Value = 0;
SensorProc_SprungAccel_Offset_FL.Max = 100;
SensorProc_SprungAccel_Offset_FL.Min = 0;
SensorProc_SprungAccel_Offset_FL.Description = 'Description goes here';
SensorProc_SprungAccel_Offset_FL.Unit = 'g';
SensorProc_SprungAccel_Offset_FL.DataType = 'double';

assignin('base','SensorProc_SprungAccel_Offset_FL',SensorProc_SprungAccel_Offset_FL);

SensorProc_SprungAccel_Offset_FR = Simulink.Parameter;
SensorProc_SprungAccel_Offset_FR.Value = 0;
SensorProc_SprungAccel_Offset_FR.Max = 100;
SensorProc_SprungAccel_Offset_FR.Min = 0;
SensorProc_SprungAccel_Offset_FR.Description = 'Description goes here';
SensorProc_SprungAccel_Offset_FR.Unit = 'g';
SensorProc_SprungAccel_Offset_FR.DataType = 'double';

assignin('base','SensorProc_SprungAccel_Offset_FR',SensorProc_SprungAccel_Offset_FR);

SensorProc_SprungAccel_Offset_RL = Simulink.Parameter;
SensorProc_SprungAccel_Offset_RL.Value = 0;
SensorProc_SprungAccel_Offset_RL.Max = 100;
SensorProc_SprungAccel_Offset_RL.Min = 0;
SensorProc_SprungAccel_Offset_RL.Description = 'Description goes here';
SensorProc_SprungAccel_Offset_RL.Unit = 'g';
SensorProc_SprungAccel_Offset_RL.DataType = 'double';

assignin('base','SensorProc_SprungAccel_Offset_RL',SensorProc_SprungAccel_Offset_RL);

SensorProc_SprungAccel_Offset_RR = Simulink.Parameter;
SensorProc_SprungAccel_Offset_RR.Value = 0;
SensorProc_SprungAccel_Offset_RR.Max = 100;
SensorProc_SprungAccel_Offset_RR.Min = 0;
SensorProc_SprungAccel_Offset_RR.Description = 'Description goes here';
SensorProc_SprungAccel_Offset_RR.Unit = 'g';
SensorProc_SprungAccel_Offset_RR.DataType = 'double';

assignin('base','SensorProc_SprungAccel_Offset_RR',SensorProc_SprungAccel_Offset_RR);

SensorProc_SprungAccel_LPFCoef = Simulink.Parameter;
SensorProc_SprungAccel_LPFCoef.Value = 5;
SensorProc_SprungAccel_LPFCoef.Max = 100;
SensorProc_SprungAccel_LPFCoef.Min = 0;
SensorProc_SprungAccel_LPFCoef.Description = 'Description goes here';
SensorProc_SprungAccel_LPFCoef.Unit = 'Hz';
SensorProc_SprungAccel_LPFCoef.DataType = 'double';

assignin('base','SensorProc_SprungAccel_LPFCoef',SensorProc_SprungAccel_LPFCoef);

%Unsprung mass acceleration
SensorProc_UnsprungAccel_Offset_FL = Simulink.Parameter;
SensorProc_UnsprungAccel_Offset_FL.Value = 0;
SensorProc_UnsprungAccel_Offset_FL.Max = 100;
SensorProc_UnsprungAccel_Offset_FL.Min = 0;
SensorProc_UnsprungAccel_Offset_FL.Description = 'Description goes here';
SensorProc_UnsprungAccel_Offset_FL.Unit = 'g';
SensorProc_UnsprungAccel_Offset_FL.DataType = 'double';

assignin('base','SensorProc_UnsprungAccel_Offset_FL',SensorProc_UnsprungAccel_Offset_FL);

SensorProc_UnsprungAccel_Offset_FR = Simulink.Parameter;
SensorProc_UnsprungAccel_Offset_FR.Value = 0;
SensorProc_UnsprungAccel_Offset_FR.Max = 100;
SensorProc_UnsprungAccel_Offset_FR.Min = 0;
SensorProc_UnsprungAccel_Offset_FR.Description = 'Description goes here';
SensorProc_UnsprungAccel_Offset_FR.Unit = 'g';
SensorProc_UnsprungAccel_Offset_FR.DataType = 'double';

assignin('base','SensorProc_UnsprungAccel_Offset_FR',SensorProc_UnsprungAccel_Offset_FR);

SensorProc_UnsprungAccel_Offset_RL = Simulink.Parameter;
SensorProc_UnsprungAccel_Offset_RL.Value = 0;
SensorProc_UnsprungAccel_Offset_RL.Max = 100;
SensorProc_UnsprungAccel_Offset_RL.Min = 0;
SensorProc_UnsprungAccel_Offset_RL.Description = 'Description goes here';
SensorProc_UnsprungAccel_Offset_RL.Unit = 'g';
SensorProc_UnsprungAccel_Offset_RL.DataType = 'double';

assignin('base','SensorProc_UnsprungAccel_Offset_RL',SensorProc_UnsprungAccel_Offset_RL);

SensorProc_UnsprungAccel_Offset_RR = Simulink.Parameter;
SensorProc_UnsprungAccel_Offset_RR.Value = 0;
SensorProc_UnsprungAccel_Offset_RR.Max = 100;
SensorProc_UnsprungAccel_Offset_RR.Min = 0;
SensorProc_UnsprungAccel_Offset_RR.Description = 'Description goes here';
SensorProc_UnsprungAccel_Offset_RR.Unit = 'g';
SensorProc_UnsprungAccel_Offset_RR.DataType = 'double';

assignin('base','SensorProc_UnsprungAccel_Offset_RR',SensorProc_UnsprungAccel_Offset_RR);

SensorProc_UnsprungAccel_LPFCoef = Simulink.Parameter;
SensorProc_UnsprungAccel_LPFCoef.Value = 5;
SensorProc_UnsprungAccel_LPFCoef.Max = 100;
SensorProc_UnsprungAccel_LPFCoef.Min = 0;
SensorProc_UnsprungAccel_LPFCoef.Description = 'Description goes here';
SensorProc_UnsprungAccel_LPFCoef.Unit = 'Hz';
SensorProc_UnsprungAccel_LPFCoef.DataType = 'double';

assignin('base','SensorProc_UnsprungAccel_LPFCoef',SensorProc_UnsprungAccel_LPFCoef);

