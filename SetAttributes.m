function [] = SetAttributes(Model)
%
%
%

AllConBlks = find_system(Model,'Followlinks','on','LookUnderMasks','off','RegExp','on','BlockType','Constant');
AllBacklashBlks = find_system(Model,'Followlinks','on','LookUnderMasks','off','RegExp','on','BlockType','Backlash');
AllDeadZoneBlks = find_system(Model,'Followlinks','on','LookUnderMasks','off','RegExp','on','BlockType','DeadZone');
AllRateLimiterBlks = find_system(Model,'Followlinks','on','LookUnderMasks','off','RegExp','on','BlockType','RateLimiter');
AllRelayBlks = find_system(Model,'Followlinks','on','LookUnderMasks','off','RegExp','on','BlockType','Relay');
AllSaturateBlks = find_system(Model,'Followlinks','on','LookUnderMasks','off','RegExp','on','BlockType','Saturate');
AllHitCrossBlks = find_system(Model,'Followlinks','on','LookUnderMasks','off','RegExp','on','BlockType','HitCross');
AllGainBlks = find_system(Model,'Followlinks','on','LookUnderMasks','off','RegExp','on','BlockType','Gain');
AllComparetoConstantBlks = find_system(Model,'Followlinks','on','LookUnderMasks','off','RegExp','on','BlockType','SubSystem','Mask','on','MaskType','Compare To Constant');

% set attribute for constant block
L = length(AllConBlks);

for i = 1:L,

		Val = get_param(AllConBlks{i},'Value');
	
		IsSimParam =  IsSimParam_func(Val);

		BckColor = 'White';

		if IsSimParam,

			ValNum = evalin('base',[Val,'.Value;']);
			ValStr = ConvertNumtoString(ValNum);

			if length(ValNum) > 1,
				AttrStr = [Val,' = \n',ValStr];
			else
				AttrStr = [Val,' = ',ValStr];
			end

			BckColor = 'Green';

			set_param(AllConBlks{i},'AttributesFormatString',AttrStr);
			set_param(AllConBlks{i},'ShowName','off');
			set_param(AllConBlks{i},'BackgroundColor',BckColor);

		end

end

% set attribute for backlash block
L = length(AllBacklashBlks);

for i = 1:L,

		Val = get_param(AllBacklashBlks{i},'BacklashWidth');
		
		IsSimParam =  IsSimParam_func(Val);

		BckColor = 'White';

		if IsSimParam,

			ValNum = evalin('base',[Val,'.Value;']);
			ValStr = ConvertNumtoString(ValNum);

			if length(ValNum) > 1,
				AttrStr = ['Deadband Width = \n',ValStr];
			else
				AttrStr = ['Deadband Width = ',ValStr];
			end
			BckColor = 'Green';

		else

			AttrStr = ['Deadband Width = ',Val];
			BckColor = 'White';

		end

		set_param(AllBacklashBlks{i},'AttributesFormatString',AttrStr);
		set_param(AllBacklashBlks{i},'ShowName','off');
		set_param(AllBacklashBlks{i},'BackgroundColor',BckColor);

end

% set attribute for DeadZone block
L = length(AllDeadZoneBlks);

for i = 1:L,

	UpprVal = get_param(AllDeadZoneBlks{i},'UpperValue');
	LowrVal = get_param(AllDeadZoneBlks{i},'LowerValue');

	IsSimParamU =  IsSimParam_func(UpprVal);
	IsSimParamL =  IsSimParam_func(LowrVal);

	BckColor = 'White';

	if IsSimParamU,

		ValNum = evalin('base',[UpprVal,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);
		AttrStr = ['<UpperValue> = ',ValStr,'\n'];
		BckColor = 'Green';

	else

		AttrStr = ['<UpperValue> = ',UpprVal,'\n'];

	end

	if IsSimParamL,

		ValNum = evalin('base',[LowrVal,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);
		AttrStr = [AttrStr,'<LowerValue> = ',ValStr];
		BckColor = 'Green';

	else

		AttrStr = [AttrStr,'<LowerValue> = ',LowrVal];

	end

	set_param(AllDeadZoneBlks{i},'AttributesFormatString',AttrStr);
	set_param(AllDeadZoneBlks{i},'ShowName','off');
	set_param(AllDeadZoneBlks{i},'BackgroundColor',BckColor);

end

% set attribute for RateLimiter block
L = length(AllRateLimiterBlks);

for i = 1:L,

	UpprVal = get_param(AllRateLimiterBlks{i},'RisingSlewLimit');
	LowrVal = get_param(AllRateLimiterBlks{i},'FallingSlewLimit');

	IsSimParamU =  IsSimParam_func(UpprVal);
	IsSimParamL =  IsSimParam_func(LowrVal);

	BckColor = 'White';

	if IsSimParamU,

		ValNum = evalin('base',[UpprVal,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);
		AttrStr = ['<Rising Slew Limit> = ',ValStr,'\n'];
		BckColor = 'Green';

	else

		AttrStr = ['<Rising Slew Limit> = ',UpprVal,'\n'];

	end

	if IsSimParamL,

		ValNum = evalin('base',[LowrVal,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);
		AttrStr = [AttrStr,'<Falling Slew Limit> = ',ValStr];
		BckColor = 'Green';

	else

		AttrStr = [AttrStr,'<Falling Slew Limit> = ',LowrVal];

	end

	set_param(AllRateLimiterBlks{i},'AttributesFormatString',AttrStr);
	set_param(AllRateLimiterBlks{i},'ShowName','off');
	set_param(AllRateLimiterBlks{i},'BackgroundColor',BckColor);

end

% set attribute for Relay block
L = length(AllRelayBlks);

for i = 1:L,

	OnVal = get_param(AllRelayBlks{i},'OnSwitchValue');
	OnOutVal = get_param(AllRelayBlks{i},'OnOutputValue');
	OffVal = get_param(AllRelayBlks{i},'OffSwitchValue');
	OffOutVal = get_param(AllRelayBlks{i},'OffOutputValue');

	IsSimParamOnV =  IsSimParam_func(OnVal);
	IsSimParamOnOut =  IsSimParam_func(OnOutVal);
	IsSimParamOffV =  IsSimParam_func(OffVal);
	IsSimParamOffOut =  IsSimParam_func(OffOutVal);

	BckColor = 'White';

	if IsSimParamOnV,

		ValNum = evalin('base',[OnVal,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);
		AttrStr = ['<On Switch Value> = ',ValStr,'\n'];
		BckColor = 'Green';

	else

		AttrStr = ['<On Switch Value> = ',OnVal,'\n'];

	end

	if IsSimParamOnOut,

		ValNum = evalin('base',[OnOutVal,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);
		AttrStr = [AttrStr,'<On Output Value> = ',ValStr,'\n'];
		BckColor = 'Green';

	else

		AttrStr = [AttrStr,'<On Output Value> = ',OnOutVal,'\n'];

	end

	if IsSimParamOffV,

		ValNum = evalin('base',[OffVal,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);
		AttrStr = [AttrStr,'<Off Switch Value> = ',ValStr,'\n'];
		BckColor = 'Green';

	else

		AttrStr = [AttrStr,'<Off Switch Value> = ',OffVal,'\n'];

	end

	if IsSimParamOffOut,

		ValNum = evalin('base',[OffOutVal,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);
		AttrStr = [AttrStr,'<Off Output Value> = ',ValStr,'\n'];
		BckColor = 'Green';

	else

		AttrStr = [AttrStr,'<Off Output Value> = ',OffOutVal,'\n'];

	end

	set_param(AllRelayBlks{i},'AttributesFormatString',AttrStr);
	set_param(AllRelayBlks{i},'ShowName','off');
	set_param(AllRelayBlks{i},'BackgroundColor',BckColor);

end

% set attribute for Saturate block
L = length(AllSaturateBlks);

for i = 1:L,

	UpprVal = get_param(AllSaturateBlks{i},'UpperLimit');
	LowrVal = get_param(AllSaturateBlks{i},'LowerLimit');

	IsSimParamU =  IsSimParam_func(UpprVal);
	IsSimParamL =  IsSimParam_func(LowrVal);

	BckColor = 'White';

	if IsSimParamU,

		ValNum = evalin('base',[UpprVal,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);
		AttrStr = ['<Upper Limit> = ',ValStr,'\n'];
		BckColor = 'Green';

	else

		AttrStr = ['<Upper Limit> = ',UpprVal,'\n'];

	end

	if IsSimParamL,

		ValNum = evalin('base',[LowrVal,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);
		AttrStr = [AttrStr,'<Lower Limit> = ',ValStr];
		BckColor = 'Green';

	else

		AttrStr = [AttrStr,'<Lower Limit> = ',LowrVal];

	end

	set_param(AllSaturateBlks{i},'AttributesFormatString',AttrStr);
	set_param(AllSaturateBlks{i},'ShowName','off');
	set_param(AllSaturateBlks{i},'BackgroundColor',BckColor);

end

% set attribute for HitCrossstant block
L = length(AllHitCrossBlks);

for i = 1:L,

	Val = get_param(AllHitCrossBlks{i},'HitCrossingOffset');

	IsSimParam =  IsSimParam_func(Val);

	BckColor = 'White';

	if IsSimParam,

		ValNum = evalin('base',[Val,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);

		if length(ValNum) > 1,
			AttrStr = ['Hit Crossing Offset = \n',ValStr];
		else
			AttrStr = ['Hit Crossing Offset = ',ValStr];
		end

		BckColor = 'Green';

	else

		AttrStr = ['Hit Crossing Offset = ',Val];
		BckColor = 'White';

	end

	set_param(AllHitCrossBlks{i},'AttributesFormatString',AttrStr);
	set_param(AllHitCrossBlks{i},'ShowName','off');
	set_param(AllHitCrossBlks{i},'BackgroundColor',BckColor);

end

% set attribute for gain block
L = length(AllGainBlks);

for i = 1:L,

	Val = get_param(AllGainBlks{i},'Gain');

	IsSimParam =  IsSimParam_func(Val);

	BckColor = 'White';

	if IsSimParam,

		ValNum = evalin('base',[Val,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);

		if length(ValNum) > 1,
			AttrStr = [Val,' = \n',ValStr];
		else
			AttrStr = [Val,' = ',ValStr];
		end

		BckColor = 'Green';

		set_param(AllGainBlks{i},'AttributesFormatString',AttrStr);
		set_param(AllGainBlks{i},'ShowName','off');
		set_param(AllGainBlks{i},'BackgroundColor',BckColor);

	end

end

% set attribute for Comparte to Constant block
L = length(AllComparetoConstantBlks);

for i = 1:L,

	Val = get_param(AllComparetoConstantBlks{i},'const');

	IsSimParam =  IsSimParam_func(Val);
		
	BckColor = 'White';

	if IsSimParam,

		RelOp = get_param(AllComparetoConstantBlks{i},'relop');
		ValNum = evalin('base',[Val,'.Value;']);
		ValStr = ConvertNumtoString(ValNum);

		AttrStr = [Val,' ',RelOp,' ',ValStr];

		BckColor = 'Green';

		set_param(AllComparetoConstantBlks{i},'AttributesFormatString',AttrStr);
		set_param(AllComparetoConstantBlks{i},'ShowName','off');
		set_param(AllComparetoConstantBlks{i},'BackgroundColor',BckColor);

	end

end

return


function IsSimParam =  IsSimParam_func(ValStr)

IsExist = evalin('base',['exist(','''',ValStr,'''',');']);

if IsExist,

	IsSimParam = evalin('base',['isa(',ValStr,',','''','Simulink.Parameter','''',');']);

else

	IsSimParam = 0;

end

