function [A,Av,B,Bv,C,C_Actv,D,E,param] = Veh_Full_Sys(VehFlag)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FileName : Veh_Full_Sys.m
%
% Created: Friday 14 April 2017 - 9:26:38 PM (Eastern Daylight Time)
%
% Updated on: Friday 14 April 2017 - 9:26:44 PM (Eastern Daylight Time)
%
% Modified by: Juchirl Park
%
% Created By : Juchirl Park 
%
% Description : Calculate additional vehicle parameters from basic known
% vehicle parameters. It generates the state space model parameters A, B, C,
% and D.
%
% Usage example
%
% [A,Av,B,Bv,C,Ca,D,E,param] = Veh_Full_Sys(param,'Curb');
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Code Start here

param = Veh_Parameters(VehFlag);  

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % assign the vehicle parameters to local variable
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ms = param.ms;
muf = param.muf;                        % sprung mass on front
mur = param.mur;                        % sprung mass on rear
ry = param.ry;                         % gyration rate
rx = param.rx;
rz = param.rz;
a = param.a;
b = param.b;
c = param.c;
d = param.d;
Jy = ms*ry*ry;
Jx = ms*rx*rx;
Jz = ms*rz*rz;
ksfl = param.ksfl;
ksfr = param.ksfr;
ksrl = param.ksrl;
ksrr = param.ksrr;

ktfl = param.ktfl;
ktfr = param.ktfr;
ktrl = param.ktrl;
ktrr = param.ktrr;
Cf = param.Cf;
Cr = param.Cr;
RatioSWA = param.SWARatio;

g = 9.8;

Bf = param.Bf;
Br = param.Br;

%x = [dz(x1), dtheta(x2), dzeta(x3), 
%    z_1 - z_FL(x4), z_2 - z_FR(x5), z_3 - z_RL(x6), z_4 - z_RR(x7),
%     dz_FL(x8), dz_RF(x9), dz_RL(x10), dz_RR(x11), 
%     z(x12), theta(x13), zeta(x14)];

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Calculate the matrices for state space model, A, B, C, D
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A = zeros(14);

%Heave Motion
A(1,1) = -(Bf + Bf + Br + Br)/ms;
A(1,2) = (a*Bf + a*Bf - b*Br - b*Br)/ms;
A(1,3) = (c*Bf - d*Bf + c*Br - d*Br)/ms;

A(1,4) = -ksfl/ms;
A(1,5) = -ksfr/ms;
A(1,6) = -ksrl/ms;
A(1,7) = -ksrr/ms;

A(1,8) = Bf/ms;
A(1,9) = Bf/ms;
A(1,10) = Br/ms;
A(1,11) = Br/ms;

%Pitch Motion
A(2,1) = (a*Bf + a*Bf - b*Br - b*Br)/Jy;
A(2,2) = -(a^2*Bf + a^2*Bf + b^2*Br + b^2*Br)/Jy;
A(2,3) = (-a*c*Bf + a*d*Bf + b*c*Br - b*d*Br)/Jy;

A(2,4) = a*ksfl/Jy;
A(2,5) = a*ksfr/Jy;
A(2,6) = -b*ksrl/Jy;
A(2,7) = -b*ksrr/Jy;

A(2,8) = -a*Bf/Jy;
A(2,9) = -a*Bf/Jy;
A(2,10) = b*Br/Jy;
A(2,11) = b*Br/Jy;

%Roll Motion
A(3,1) = (c*Bf - d*Bf + c*Br -d*Br)/Jx;
A(3,2) = (-a*c*Bf + a*d*Bf + b*c*Br - b*d*Br)/Jx;
A(3,3) = -(c^2*Bf + d^2*Bf + c^2*Br + d^2*Br)/Jx;

A(3,4) = c*ksfl/Jx;
A(3,5) = -d*ksfr/Jx;
A(3,6) = c*ksrl/Jx;
A(3,7) = -d*ksrr/Jx;

A(3,8) = -c*Bf/Jx;
A(3,9) = d*Bf/Jx;
A(3,10) = -c*Br/Jx;
A(3,11) = d*Br/Jx;

%Wheel states
A(4,:)  = [1,-a,-c,0, 0, 0, 0,-1, 0, 0, 0,0,0,0];
A(5,:)  = [1,-a, d,0, 0, 0, 0, 0,-1, 0, 0,0,0,0];
A(6,:)  = [1, b,-c,0, 0, 0, 0, 0, 0,-1, 0,0,0,0];
A(7,:)  = [1, b, d,0, 0, 0, 0, 0, 0, 0,-1,0,0,0];


A(8,:)  = [Bf/muf,-a*Bf/muf,-c*Bf/muf,(ktfl + ksfl)/muf,0,0,0,-Bf/muf,0,0,0,...
           -ktfl/muf,a*ktfl/muf,c*ktfl/muf];

A(9,:) = [Bf/muf,-a*Bf/muf, d*Bf/muf,0,(ktfr + ksfr)/muf,0,0,0,-Bf/muf,0,0,...
           -ktfr/muf,a*ktfr/muf,-d*ktfr/muf];

A(10,:)  = [Br/mur, b*Br/mur,-c*Br/mur,0,0,(ktrl + ksrl)/mur,0,0,0,-Br/mur,0,...
           -ktrl/mur,-b*ktrl/mur,c*ktrl/mur];

A(11,:) = [Br/mur, b*Br/mur, d*Br/mur,0,0,0,(ktrr + ksrr)/mur,0,0,0,-Br/mur,...
           -ktrr/mur,-b*ktrr/mur,-c*ktrr/mur];

%Body position changes
A(12,1) = 1;
A(13,2) = 1;
A(14,3) = 1;
Ax = A(1:11,1:11);

Bx = zeros(11,4);
B = zeros(14,4);

B(1,:) = [-1/ms,-1/ms,-1/ms,-1/ms];
B(2,:) = [a/Jy,a/Jy,-b/Jy,-b/Jy];
B(3,:) = [c/Jx,-d/Jx,c/Jx,-d/Jx];
B(8,1) = 1/muf;
B(9,2) = 1/muf;
B(10,3) = 1/mur;
B(11,4) = 1/mur;

Bx = B(1:11,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C = zeros(10,14);
C(1,1) = 1;
C(2,2) = 1;
C(3,3) = 1;
C(4,8) = 1;
C(5,9) = 1;
C(6,10) = 1;
C(7,11) = 1;
C(8,12) = 1;
C(9,13) = 1;
C(10,14) = 1;

Ca = zeros(10,14);
Ca(1,:) = A(1,:);
Ca(2,:) = A(2,:);
Ca(3,:) = A(3,:);
Ca(4,1) = 1;
Ca(5,2) = 1;
Ca(6,3) = 1;
Ca(7,8) = 1;
Ca(8,9) = 1;
Ca(9,10) = 1;
Ca(10,11) = 1;

C_Actv = eye(14,14);

D = zeros(10,4);

E = zeros(11,1);

E(5,1) = -g;
E(6,1) = -g;
E(7,1) = -g;
E(8,1) = -g;
E(9,1) = -g;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bicycle model parameters. Av, Bv, and Cv matrices
Av = zeros(3,4);
Bv = zeros(3,1);
Cv = eye(3);

Av(1,1) = -(Cf + Cr)/ms;
Av(1,2) = -(a*Cf - b*Cr)/ms;
Av(1,3) = -1;

Av(2,1) = -(a*Cf - b*Cr)/Jz;
Av(2,2) = -(a^2*Cf + b^2*Cr)/Jz;

Av(3,1) = Cf/ms;
Av(3,2) = a*Cf/ms;
Av(3,4) = 1;

Bv(1,1) = Cf/ms;
Bv(2,1) = a*Cf/Jz;
Bv(3,1) = -Cf/ms;

return

% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Veh_Full_Sys.m ends here

