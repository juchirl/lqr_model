function param = Veh_Parameters(VehFlag)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FileName : Veh_Parameters.m
%
% Created: TUESDAY OCTOBER 21, 2008 17:02:03 PM
%
% Updated on: Monday 16 November 2015 - 7:51:27 PM (Eastern Standard Time)
%
% Modified by: Juchirl Park
%
% Created By : Juchirl Park 
%
% Description : Basic vehicle parameters defined here.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Code Start here

switch VehFlag,
      
	case 'Curb',                                % For JLTV
        
		param.ms = 3479;     % Total Sprung mass in kg
        
		param.muf = 340;      % Front unsprung mass in kg
		param.mur = 340;      % Rear unsprung mass in kg
        
		param.ry = 3.257;      % radius of gyration in m
		param.rx = 1.844;
		param.rz = 3.091;
		param.a = 1.999;          % Distance between front axle and center of gravity in m
		param.b = 4.267 - param.a; % Distance between rear axle and center of gravity in m
		param.c = 1.003;
		param.d = 1.003;

		param.ksfl = 153000;     % Front left spring stiffness in N/m
		param.ksfr = 153000;     % Front right spring stiffness in N/m
        
		param.ksrl = 153000;     % Rear left spring stiffness in N/m
		param.ksrr = 153000;     % Rear right spring stiffness in N/m
        
		param.ktfl = 438000;    % Front Left Tire spring rate in N/m
		param.ktfr = 438000;    % Front Right Tire spring rate in N/m
        
		param.ktrl = 438000;    % Rear Tire spring rate in N/m
		param.ktrr = 438000;    % Rear Tire spring rate in N/m
        
		param.Bf = 9000/2;        % Front wheel damping in N/m/s
		param.Br = 9000/2;        % Rear wheel damping in N/m/s
        
		param.Cf = 170000;                     % Tire Front cornering Stiffness
		param.Cr = 100000;                     % Tire Rear Cornering Stiffness
		param.SWARatio = 21.7;                % Steering ratio
		param.WhlBase = 4.267;                % Wheelbase of vehicle meters
		param.DamperLongDist = 4.267;         % Damper Longitudinal distance
		param.TrackWidthFront = 2.0061;       % Track width front for damper
		param.TrackWidthRear = 2.0061;        % Track width rear for damper
        
		param.Accel_CG = [1.999  -1.003  1.0;              % Four-corner sensors
		1.999  1.003  1.0;
		-2.268 -1.003  1.0;
		-2.268 1.003  1.0];
        
	case 'GVW',
        
		param.ms = 6955;     % Total Sprung mass in kg
        
		param.muf = 544;      % Front unsprung mass in kg
		param.mur = 544;      % Rear unsprung mass in kg

		param.ry = 1.638;      % radius of gyration in m
		param.rx = 0.878;
		param.rz = 1.638;
		param.a = 2.353;          % Distance between front axle and center of gravity in m
		param.b = 4.039 - param.a; % Distance between rear axle and center of gravity in m
		param.c = 1.047;
		param.d = 1.047;
        
		param.ksfl = 2830;     % Front left spring stiffness in N/m
		param.ksfr = 2830;     % Front right spring stiffness in N/m
        
		param.ksrl = 2830;     % Rear left spring stiffness in N/m
		param.ksrr = 2830;     % Rear right spring stiffness in N/m
        
		param.ktfl = 613125;    % Front Left Tire spring rate in N/m
		param.ktfr = 613125;    % Front Right Tire spring rate in N/m
        
		param.ktrl = 613125;    % Rear Tire spring rate in N/m
		param.ktrr = 613125;    % Rear Tire spring rate in N/m
        
		param.Bf = 9000/2;        % Front wheel damping in N/m/s
		param.Br = 9000/2;        % Rear wheel damping in N/m/s
        
		param.Cf = 180000;                     % Tire Front cornering Stiffness
		param.Cr = 100000;                     % Tire Rear Cornering Stiffness
		param.SWARatio = 21.7;                % Steering ratio
		param.WhlBase = 4.2672;                % Wheelbase of vehicle meters
		param.DamperLongDist = 4.267;         % Damper Longitudinal distance
		param.TrackWidthFront = 2.091;       % Track width front for damper
		param.TrackWidthRear = 2.096;        % Track width rear for damper

		param.Accel_CG = [2.358  -1.047  1.0;              % Four-corner sensors
		2.358  1.047  1.0;
		-1.686 -1.047  1.0;
		-1.686 1.047  1.0];
        
	otherwise,                             % Default vehicles (passive)
        
		param.ms = 5225;     % Total Sprung mass in kg

		param.muf = 340;      % Front unsprung mass in kg
		param.mur = 340;      % Rear unsprung mass in kg
        
		param.ry = 2.882;      % radius of gyration in m
		param.rx = 1.666;
		param.rz = 2.736;
		param.a = 2.537;          % Distance between front axle and center of gravity in m
		param.b = 4.267 - param.a; % Distance between rear axle and center of gravity in m
		param.c = 1.033;
		param.d = 1.033;
        
		param.ksfl = 153000;     % Front left spring stiffness in N/m
		param.ksfr = 153000;     % Front right spring stiffness in N/m
        
		param.ksrl = 153000;     % Rear left spring stiffness in N/m
		param.ksrr = 153000;     % Rear right spring stiffness in N/m
        
		param.ktfl = 596000;    % Front Left Tire spring rate in N/m
		param.ktfr = 596000;    % Front Right Tire spring rate in N/m
        
		param.ktrl = 596000;    % Rear Tire spring rate in N/m
		param.ktrr = 596000;    % Rear Tire spring rate in N/m
        
		param.Bf = 6000/2;        % Front wheel damping in N/m/s
		param.Br = 6000/2;        % Rear wheel damping in N/m/s
        
		param.Cf = 120000;                     % Tire Front cornering Stiffness
		param.Cr = 80000;                     % Tire Rear Cornering Stiffness
		param.SWARatio = 21.7;                % Steering ratio
		param.WhlBase = 4.2672;                % Wheelbase of vehicle meters
		param.DamperLongDist = 4.267;         % Damper Longitudinal distance
		param.TrackWidthFront = 2.0061;       % Track width front for damper
		param.TrackWidthRear = 2.0066;        % Track width rear for damper

		param.Accel_CG = [2.537  -1.003  1.0;              % Four-corner sensors
											2.537  1.003  1.0;
											-2.135 -1.003  1.0;
											-2.135 1.003  1.0];
        
end
    
    
param.Accel_DS = [1.63   -0.578  1.0;
									1.63    1.578  1.0;
									-2.508 -0.578  1.0;
									-2.508  1.578  1.0];
    
param.Accel_MS = [2.42   -0.637  1.0;
                  2.42    1.637  1.0;
									-1.718 -0.637  1.0;
									-1.718  1.637  1.0];
    
param.Accel_RS = [2.851 -0.298  1.0;
                  2.851  1.298  1.0;
									-1.287 -0.298  1.0;
									-1.287  1.298  1.0];
     

return

%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Veh_Parameters.m ends here

