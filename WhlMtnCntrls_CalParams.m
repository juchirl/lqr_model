function [] = WhlMtnCntrls_CalParams(Ka1,Ka2,Ka3,VehFlag)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filename : WhlMtnCntrls_CalParams
%
% Created : Sunday 09 April 2017 - 9:26:07 PM (Eastern Daylight Time)
%
% Updated on: Sunday 21 May 2017 - 5:55:54 PM (Eastern Daylight Time)
%
% Created By : Juchirl Park 
%
% Description : Adaptive Damping tuning variables reside here. The control
% gains are calculated based on Linear Quadratic Regulator and state space
% model of the vehicle with 7-degree of freedom. These gains can be
% displayed on the MATLAB command window or stored in the excel file user
% needs to be define. It have 4 input variable with no return variable.
% Usually, this program will be called by "Driver_Controls_Main".
%
% [] = WhlyMtnCntrls_CalParams(Ka1,Ka2,Ka3,VehFlag)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MaxRatio = 1.5;

WhlMtnCntrls_MaxCntrlFrc = Simulink.Parameter;
WhlMtnCntrls_MaxCntrlFrc.Value = 45000;
WhlMtnCntrls_MaxCntrlFrc.Max = 100000;
WhlMtnCntrls_MaxCntrlFrc.Min = 0;
WhlMtnCntrls_MaxCntrlFrc.Description = 'Maximum control force for wheel motion controls';
WhlMtnCntrls_MaxCntrlFrc.Unit = 'N';
WhlMtnCntrls_MaxCntrlFrc.DataType = 'double';

assignin('base','WhlMtnCntrls_MaxCntrlFrc',WhlMtnCntrls_MaxCntrlFrc);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wheel Control Gains for Curb Mode
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
WhlMtnCntrls_HldFltrCoef = Simulink.Parameter;
WhlMtnCntrls_HldFltrCoef.Value = 5;
WhlMtnCntrls_HldFltrCoef.Max = 80;
WhlMtnCntrls_HldFltrCoef.Min = 0;
WhlMtnCntrls_HldFltrCoef.Description = 'Description goes here';
WhlMtnCntrls_HldFltrCoef.Unit = 'Hz';
WhlMtnCntrls_HldFltrCoef.DataType = 'double';

assignin('base','WhlMtnCntrls_HldFltrCoef',WhlMtnCntrls_HldFltrCoef);

WhlMtnCntrls_EnableWhlCntrls = Simulink.Parameter;
WhlMtnCntrls_EnableWhlCntrls.Value = 1;
WhlMtnCntrls_EnableWhlCntrls.Max = 1;
WhlMtnCntrls_EnableWhlCntrls.Min = 0;
WhlMtnCntrls_EnableWhlCntrls.Description = 'Description goes here';
WhlMtnCntrls_EnableWhlCntrls.Unit = 'N/A';
WhlMtnCntrls_EnableWhlCntrls.DataType = 'boolean';

assignin('base','WhlMtnCntrls_EnableWhlCntrls',WhlMtnCntrls_EnableWhlCntrls);

Res = 100;
FLGain = round(Ka1(1,8)/Res)*Res;
FRGain = round(Ka1(2,9)/Res)*Res;
RLGain = round(Ka1(3,10)/Res)*Res;
RRGain = round(Ka1(4,11)/Res)*Res;

WhlMtnCntrls_GnCrbFL = Simulink.Parameter;
WhlMtnCntrls_GnCrbFL.Value = FLGain;
WhlMtnCntrls_GnCrbFL.Max = round((max(FLGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnCrbFL.Min = 0;
WhlMtnCntrls_GnCrbFL.Description = 'Description goes here';
WhlMtnCntrls_GnCrbFL.Unit = 'N/A';
WhlMtnCntrls_GnCrbFL.DataType = 'double';

assignin('base','WhlMtnCntrls_GnCrbFL',WhlMtnCntrls_GnCrbFL);

WhlMtnCntrls_GnCrbFR = Simulink.Parameter;
WhlMtnCntrls_GnCrbFR.Value = FRGain;
WhlMtnCntrls_GnCrbFR.Max = round((max(FRGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnCrbFR.Min = 0;
WhlMtnCntrls_GnCrbFR.Description = 'Description goes here';
WhlMtnCntrls_GnCrbFR.Unit = 'N/A';
WhlMtnCntrls_GnCrbFR.DataType = 'double';

assignin('base','WhlMtnCntrls_GnCrbFR',WhlMtnCntrls_GnCrbFR);

WhlMtnCntrls_GnCrbRL = Simulink.Parameter;
WhlMtnCntrls_GnCrbRL.Value = RLGain;
WhlMtnCntrls_GnCrbRL.Max = round((max(RLGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnCrbRL.Min = 0;
WhlMtnCntrls_GnCrbRL.Description = 'Description goes here';
WhlMtnCntrls_GnCrbRL.Unit = 'N/A';
WhlMtnCntrls_GnCrbRL.DataType = 'double';

assignin('base','WhlMtnCntrls_GnCrbRL',WhlMtnCntrls_GnCrbRL);

WhlMtnCntrls_GnCrbRR = Simulink.Parameter;
WhlMtnCntrls_GnCrbRR.Value = RRGain;
WhlMtnCntrls_GnCrbRR.Max = round((max(RRGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnCrbRR.Min = 0;
WhlMtnCntrls_GnCrbRR.Description = 'Description goes here';
WhlMtnCntrls_GnCrbRR.Unit = 'N/A';
WhlMtnCntrls_GnCrbRR.DataType = 'double';

assignin('base','WhlMtnCntrls_GnCrbRR',WhlMtnCntrls_GnCrbRR);

WhlMtnCntrls_RebRtCrbOffRd = Simulink.Parameter;
WhlMtnCntrls_RebRtCrbOffRd.Value = [0.0, 1.0, 0.6];
WhlMtnCntrls_RebRtCrbOffRd.Max = 10;
WhlMtnCntrls_RebRtCrbOffRd.Min = 0;
WhlMtnCntrls_RebRtCrbOffRd.Description = 'Description goes here';
WhlMtnCntrls_RebRtCrbOffRd.Unit = 'N/A';
WhlMtnCntrls_RebRtCrbOffRd.DataType = 'double';

assignin('base','WhlMtnCntrls_RebRtCrbOffRd',WhlMtnCntrls_RebRtCrbOffRd);

WhlMtnCntrls_CmpRtCrbOffRd = Simulink.Parameter;
WhlMtnCntrls_CmpRtCrbOffRd.Value = [0.0, 0.2, 1.0];
WhlMtnCntrls_CmpRtCrbOffRd.Max = 10;
WhlMtnCntrls_CmpRtCrbOffRd.Min = 0;
WhlMtnCntrls_CmpRtCrbOffRd.Description = 'Description goes here';
WhlMtnCntrls_CmpRtCrbOffRd.Unit = 'N/A';
WhlMtnCntrls_CmpRtCrbOffRd.DataType = 'double';

assignin('base','WhlMtnCntrls_CmpRtCrbOffRd',WhlMtnCntrls_CmpRtCrbOffRd);

WhlMtnCntrls_RebRtCrbOnRd = Simulink.Parameter;
WhlMtnCntrls_RebRtCrbOnRd.Value = [0.0, 1.5, 0.8];
WhlMtnCntrls_RebRtCrbOnRd.Max = 10;
WhlMtnCntrls_RebRtCrbOnRd.Min = 0;
WhlMtnCntrls_RebRtCrbOnRd.Description = 'Description goes here';
WhlMtnCntrls_RebRtCrbOnRd.Unit = 'N/A';
WhlMtnCntrls_RebRtCrbOnRd.DataType = 'double';

assignin('base','WhlMtnCntrls_RebRtCrbOnRd',WhlMtnCntrls_RebRtCrbOnRd);

WhlMtnCntrls_CmpRtCrbOnRd = Simulink.Parameter;
WhlMtnCntrls_CmpRtCrbOnRd.Value = [0.0, 0.2, 1.5];
WhlMtnCntrls_CmpRtCrbOnRd.Max = 10;
WhlMtnCntrls_CmpRtCrbOnRd.Min = 0;
WhlMtnCntrls_CmpRtCrbOnRd.Description = 'Description goes here';
WhlMtnCntrls_CmpRtCrbOnRd.Unit = 'N/A';
WhlMtnCntrls_CmpRtCrbOnRd.DataType = 'double';

assignin('base','WhlMtnCntrls_CmpRtCrbOnRd',WhlMtnCntrls_CmpRtCrbOnRd);

WhlMtnCntrls_RebRtCrbRst = Simulink.Parameter;
WhlMtnCntrls_RebRtCrbRst.Value = [0.0, 1.0, 0.6];
WhlMtnCntrls_RebRtCrbRst.Max = 10;
WhlMtnCntrls_RebRtCrbRst.Min = 0;
WhlMtnCntrls_RebRtCrbRst.Description = 'Description goes here';
WhlMtnCntrls_RebRtCrbRst.Unit = 'N/A';
WhlMtnCntrls_RebRtCrbRst.DataType = 'double';

assignin('base','WhlMtnCntrls_RebRtCrbRst',WhlMtnCntrls_RebRtCrbRst);

WhlMtnCntrls_CmpRtCrbRst = Simulink.Parameter;
WhlMtnCntrls_CmpRtCrbRst.Value = [0.0, 0.2, 1.0];
WhlMtnCntrls_CmpRtCrbRst.Max = 10;
WhlMtnCntrls_CmpRtCrbRst.Min = 0;
WhlMtnCntrls_CmpRtCrbRst.Description = 'Description goes here';
WhlMtnCntrls_CmpRtCrbRst.Unit = 'N/A';
WhlMtnCntrls_CmpRtCrbRst.DataType = 'double';

assignin('base','WhlMtnCntrls_CmpRtCrbRst',WhlMtnCntrls_CmpRtCrbRst);

%WhlMtnCntrls_NCyclsCrb = Simulink.Parameter;
%WhlMtnCntrls_NCyclsCrb.Value = 3;
%WhlMtnCntrls_NCyclsCrb.Max = 10;
%WhlMtnCntrls_NCyclsCrb.Min = 0;
%WhlMtnCntrls_NCyclsCrb.Description = 'Description goes here';
%WhlMtnCntrls_NCyclsCrb.Unit = 'N/A';
%WhlMtnCntrls_NCyclsCrb.DataType = 'double';
%
%assignin('base','WhlMtnCntrls_NCyclsCrb',WhlMtnCntrls_NCyclsCrb);

WhlMtnCntrls_SVStrtThrsCrb = Simulink.Parameter;
WhlMtnCntrls_SVStrtThrsCrb.Value = [300, 300, 300, 300];
WhlMtnCntrls_SVStrtThrsCrb.Max = 1000;
WhlMtnCntrls_SVStrtThrsCrb.Min = 0;
WhlMtnCntrls_SVStrtThrsCrb.Description = 'Description goes here';
WhlMtnCntrls_SVStrtThrsCrb.Unit = 'N/A';
WhlMtnCntrls_SVStrtThrsCrb.DataType = 'double';

assignin('base','WhlMtnCntrls_SVStrtThrsCrb',WhlMtnCntrls_SVStrtThrsCrb);

WhlMtnCntrls_SVMaxThrsCrb = Simulink.Parameter;
WhlMtnCntrls_SVMaxThrsCrb.Value = [600, 600, 600, 600];
WhlMtnCntrls_SVMaxThrsCrb.Max = 1000;
WhlMtnCntrls_SVMaxThrsCrb.Min = 0;
WhlMtnCntrls_SVMaxThrsCrb.Description = 'Description goes here';
WhlMtnCntrls_SVMaxThrsCrb.Unit = 'N/A';
WhlMtnCntrls_SVMaxThrsCrb.DataType = 'double';

assignin('base','WhlMtnCntrls_SVMaxThrsCrb',WhlMtnCntrls_SVMaxThrsCrb);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wheel Control for GVW Mode
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Res = 100;
FLGain = round(Ka2(1,8)/Res)*Res;
FRGain = round(Ka2(2,9)/Res)*Res;
RLGain = round(Ka2(3,10)/Res)*Res;
RRGain = round(Ka2(4,11)/Res)*Res;

WhlMtnCntrls_GnGVWFL = Simulink.Parameter;
WhlMtnCntrls_GnGVWFL.Value = FLGain;
WhlMtnCntrls_GnGVWFL.Max = round((max(FLGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnGVWFL.Min = 0;
WhlMtnCntrls_GnGVWFL.Description = 'Description goes here';
WhlMtnCntrls_GnGVWFL.Unit = 'N/A';
WhlMtnCntrls_GnGVWFL.DataType = 'double';

assignin('base','WhlMtnCntrls_GnGVWFL',WhlMtnCntrls_GnGVWFL);

WhlMtnCntrls_GnGVWFR = Simulink.Parameter;
WhlMtnCntrls_GnGVWFR.Value = FRGain;
WhlMtnCntrls_GnGVWFR.Max = round((max(FRGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnGVWFR.Min = 0;
WhlMtnCntrls_GnGVWFR.Description = 'Description goes here';
WhlMtnCntrls_GnGVWFR.Unit = 'N/A';
WhlMtnCntrls_GnGVWFR.DataType = 'double';

assignin('base','WhlMtnCntrls_GnGVWFR',WhlMtnCntrls_GnGVWFR);

WhlMtnCntrls_GnGVWRL = Simulink.Parameter;
WhlMtnCntrls_GnGVWRL.Value = RLGain;
WhlMtnCntrls_GnGVWRL.Max = round((max(RLGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnGVWRL.Min = 0;
WhlMtnCntrls_GnGVWRL.Description = 'Description goes here';
WhlMtnCntrls_GnGVWRL.Unit = 'N/A';
WhlMtnCntrls_GnGVWRL.DataType = 'double';

assignin('base','WhlMtnCntrls_GnGVWRL',WhlMtnCntrls_GnGVWRL);

WhlMtnCntrls_GnGVWRR = Simulink.Parameter;
WhlMtnCntrls_GnGVWRR.Value = RRGain;
WhlMtnCntrls_GnGVWRR.Max = round((max(RRGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnGVWRR.Min = 0;
WhlMtnCntrls_GnGVWRR.Description = 'Description goes here';
WhlMtnCntrls_GnGVWRR.Unit = 'N/A';
WhlMtnCntrls_GnGVWRR.DataType = 'double';

assignin('base','WhlMtnCntrls_GnGVWRR',WhlMtnCntrls_GnGVWRR);

WhlMtnCntrls_RebRtGVWOffRd = Simulink.Parameter;
WhlMtnCntrls_RebRtGVWOffRd.Value = [0.4, 1.0, 1.3];
WhlMtnCntrls_RebRtGVWOffRd.Max = 10;
WhlMtnCntrls_RebRtGVWOffRd.Min = 0;
WhlMtnCntrls_RebRtGVWOffRd.Description = 'Description goes here';
WhlMtnCntrls_RebRtGVWOffRd.Unit = 'N/A';
WhlMtnCntrls_RebRtGVWOffRd.DataType = 'double';

assignin('base','WhlMtnCntrls_RebRtGVWOffRd',WhlMtnCntrls_RebRtGVWOffRd);

WhlMtnCntrls_CmpRtGVWOffRd = Simulink.Parameter;
WhlMtnCntrls_CmpRtGVWOffRd.Value = [0.5, 1.0, 1.0];
WhlMtnCntrls_CmpRtGVWOffRd.Max = 10;
WhlMtnCntrls_CmpRtGVWOffRd.Min = 0;
WhlMtnCntrls_CmpRtGVWOffRd.Description = 'Description goes here';
WhlMtnCntrls_CmpRtGVWOffRd.Unit = 'N/A';
WhlMtnCntrls_CmpRtGVWOffRd.DataType = 'double';

assignin('base','WhlMtnCntrls_CmpRtGVWOffRd',WhlMtnCntrls_CmpRtGVWOffRd);

WhlMtnCntrls_RebRtGVWOnRd = Simulink.Parameter;
WhlMtnCntrls_RebRtGVWOnRd.Value = [0.0, 1.5, 1.8];
WhlMtnCntrls_RebRtGVWOnRd.Max = 10;
WhlMtnCntrls_RebRtGVWOnRd.Min = 0;
WhlMtnCntrls_RebRtGVWOnRd.Description = 'Description goes here';
WhlMtnCntrls_RebRtGVWOnRd.Unit = 'N/A';
WhlMtnCntrls_RebRtGVWOnRd.DataType = 'double';

assignin('base','WhlMtnCntrls_RebRtGVWOnRd',WhlMtnCntrls_RebRtGVWOnRd);

WhlMtnCntrls_CmpRtGVWOnRd = Simulink.Parameter;
WhlMtnCntrls_CmpRtGVWOnRd.Value = [0.0, 0.2, 1.5];
WhlMtnCntrls_CmpRtGVWOnRd.Max = 10;
WhlMtnCntrls_CmpRtGVWOnRd.Min = 0;
WhlMtnCntrls_CmpRtGVWOnRd.Description = 'Description goes here';
WhlMtnCntrls_CmpRtGVWOnRd.Unit = 'N/A';
WhlMtnCntrls_CmpRtGVWOnRd.DataType = 'double';

assignin('base','WhlMtnCntrls_CmpRtGVWOnRd',WhlMtnCntrls_CmpRtGVWOnRd);

WhlMtnCntrls_RebRtGVWRst = Simulink.Parameter;
WhlMtnCntrls_RebRtGVWRst.Value = [0.0, 1.0, 0.6];
WhlMtnCntrls_RebRtGVWRst.Max = 10;
WhlMtnCntrls_RebRtGVWRst.Min = 0;
WhlMtnCntrls_RebRtGVWRst.Description = 'Description goes here';
WhlMtnCntrls_RebRtGVWRst.Unit = 'N/A';
WhlMtnCntrls_RebRtGVWRst.DataType = 'double';

assignin('base','WhlMtnCntrls_RebRtGVWRst',WhlMtnCntrls_RebRtGVWRst);

WhlMtnCntrls_CmpRtGVWRst = Simulink.Parameter;
WhlMtnCntrls_CmpRtGVWRst.Value = [0.0, 0.2, 1.0];
WhlMtnCntrls_CmpRtGVWRst.Max = 10;
WhlMtnCntrls_CmpRtGVWRst.Min = 0;
WhlMtnCntrls_CmpRtGVWRst.Description = 'Description goes here';
WhlMtnCntrls_CmpRtGVWRst.Unit = 'N/A';
WhlMtnCntrls_CmpRtGVWRst.DataType = 'double';

assignin('base','WhlMtnCntrls_CmpRtGVWRst',WhlMtnCntrls_CmpRtGVWRst);

%WhlMtnCntrls_NCyclsGVW = Simulink.Parameter;
%WhlMtnCntrls_NCyclsGVW.Value = 7;
%WhlMtnCntrls_NCyclsGVW.Max = 10;
%WhlMtnCntrls_NCyclsGVW.Min = 0;
%WhlMtnCntrls_NCyclsGVW.Description = 'Description goes here';
%WhlMtnCntrls_NCyclsGVW.Unit = 'N/A';
%WhlMtnCntrls_NCyclsGVW.DataType = 'double';
%
%assignin('base','WhlMtnCntrls_NCyclsGVW',WhlMtnCntrls_NCyclsGVW);

WhlMtnCntrls_SVStrtThrsGVW = Simulink.Parameter;
WhlMtnCntrls_SVStrtThrsGVW.Value = [300, 300, 300, 300];
WhlMtnCntrls_SVStrtThrsGVW.Max = 1000;
WhlMtnCntrls_SVStrtThrsGVW.Min = 0;
WhlMtnCntrls_SVStrtThrsGVW.Description = 'Description goes here';
WhlMtnCntrls_SVStrtThrsGVW.Unit = 'N/A';
WhlMtnCntrls_SVStrtThrsGVW.DataType = 'double';

assignin('base','WhlMtnCntrls_SVStrtThrsGVW',WhlMtnCntrls_SVStrtThrsGVW);

WhlMtnCntrls_SVMaxThrsGVW = Simulink.Parameter;
WhlMtnCntrls_SVMaxThrsGVW.Value = [600, 600, 600, 600];
WhlMtnCntrls_SVMaxThrsGVW.Max = 1000;
WhlMtnCntrls_SVMaxThrsGVW.Min = 0;
WhlMtnCntrls_SVMaxThrsGVW.Description = 'Description goes here';
WhlMtnCntrls_SVMaxThrsGVW.Unit = 'N/A';
WhlMtnCntrls_SVMaxThrsGVW.DataType = 'double';

assignin('base','WhlMtnCntrls_SVMaxThrsGVW',WhlMtnCntrls_SVMaxThrsGVW);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wheel Control for Other Mode
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Res = 100;
FLGain = round(Ka3(1,8)/Res)*Res;
FRGain = round(Ka3(2,9)/Res)*Res;
RLGain = round(Ka3(3,10)/Res)*Res;
RRGain = round(Ka3(4,11)/Res)*Res;

WhlMtnCntrls_GnRstFL = Simulink.Parameter;
WhlMtnCntrls_GnRstFL.Value = FLGain;
WhlMtnCntrls_GnRstFL.Max = round((max(FLGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnRstFL.Min = 0;
WhlMtnCntrls_GnRstFL.Description = 'Description goes here';
WhlMtnCntrls_GnRstFL.Unit = 'N/A';
WhlMtnCntrls_GnRstFL.DataType = 'double';

assignin('base','WhlMtnCntrls_GnRstFL',WhlMtnCntrls_GnRstFL);

WhlMtnCntrls_GnRstFR = Simulink.Parameter;
WhlMtnCntrls_GnRstFR.Value = FRGain;
WhlMtnCntrls_GnRstFR.Max = round((max(FRGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnRstFR.Min = 0;
WhlMtnCntrls_GnRstFR.Description = 'Description goes here';
WhlMtnCntrls_GnRstFR.Unit = 'N/A';
WhlMtnCntrls_GnRstFR.DataType = 'double';

assignin('base','WhlMtnCntrls_GnRstFR',WhlMtnCntrls_GnRstFR);

WhlMtnCntrls_GnRstRL = Simulink.Parameter;
WhlMtnCntrls_GnRstRL.Value = RLGain;
WhlMtnCntrls_GnRstRL.Max = round((max(RLGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnRstRL.Min = 0;
WhlMtnCntrls_GnRstRL.Description = 'Description goes here';
WhlMtnCntrls_GnRstRL.Unit = 'N/A';
WhlMtnCntrls_GnRstRL.DataType = 'double';

assignin('base','WhlMtnCntrls_GnRstRL',WhlMtnCntrls_GnRstRL);

WhlMtnCntrls_GnRstRR = Simulink.Parameter;
WhlMtnCntrls_GnRstRR.Value = RRGain;
WhlMtnCntrls_GnRstRR.Max = round((max(RRGain)*MaxRatio)./Res).*Res+Res; 
WhlMtnCntrls_GnRstRR.Min = 0;
WhlMtnCntrls_GnRstRR.Description = 'Description goes here';
WhlMtnCntrls_GnRstRR.Unit = 'N/A';
WhlMtnCntrls_GnRstRR.DataType = 'double';

assignin('base','WhlMtnCntrls_GnRstRR',WhlMtnCntrls_GnRstRR);

WhlMtnCntrls_RebRtRst = Simulink.Parameter;
WhlMtnCntrls_RebRtRst.Value = [0.0, 1.0, 0.6];
WhlMtnCntrls_RebRtRst.Max = 10;
WhlMtnCntrls_RebRtRst.Min = 0;
WhlMtnCntrls_RebRtRst.Description = 'Description goes here';
WhlMtnCntrls_RebRtRst.Unit = 'N/A';
WhlMtnCntrls_RebRtRst.DataType = 'double';

assignin('base','WhlMtnCntrls_RebRtRst',WhlMtnCntrls_RebRtRst);

WhlMtnCntrls_CmpRtRst = Simulink.Parameter;
WhlMtnCntrls_CmpRtRst.Value = [0.0, 0.2, 1.0];
WhlMtnCntrls_CmpRtRst.Max = 10;
WhlMtnCntrls_CmpRtRst.Min = 0;
WhlMtnCntrls_CmpRtRst.Description = 'Description goes here';
WhlMtnCntrls_CmpRtRst.Unit = 'N/A';
WhlMtnCntrls_CmpRtRst.DataType = 'double';

assignin('base','WhlMtnCntrls_CmpRtRst',WhlMtnCntrls_CmpRtRst);

%WhlMtnCntrls_NCyclsRst = Simulink.Parameter;
%WhlMtnCntrls_NCyclsRst.Value = 3;
%WhlMtnCntrls_NCyclsRst.Max = 10;
%WhlMtnCntrls_NCyclsRst.Min = 0;
%WhlMtnCntrls_NCyclsRst.Description = 'Description goes here';
%WhlMtnCntrls_NCyclsRst.Unit = 'N/A';
%WhlMtnCntrls_NCyclsRst.DataType = 'double';
%
%assignin('base','WhlMtnCntrls_NCyclsRst',WhlMtnCntrls_NCyclsRst);

WhlMtnCntrls_SVStrtThrsRst = Simulink.Parameter;
WhlMtnCntrls_SVStrtThrsRst.Value = [300, 300, 300, 300];
WhlMtnCntrls_SVStrtThrsRst.Max = 1000;
WhlMtnCntrls_SVStrtThrsRst.Min = 0;
WhlMtnCntrls_SVStrtThrsRst.Description = 'Description goes here';
WhlMtnCntrls_SVStrtThrsRst.Unit = 'N/A';
WhlMtnCntrls_SVStrtThrsRst.DataType = 'double';

assignin('base','WhlMtnCntrls_SVStrtThrsRst',WhlMtnCntrls_SVStrtThrsRst);

WhlMtnCntrls_SVMaxThrsRst = Simulink.Parameter;
WhlMtnCntrls_SVMaxThrsRst.Value = [600, 600, 600, 600];
WhlMtnCntrls_SVMaxThrsRst.Max = 1000;
WhlMtnCntrls_SVMaxThrsRst.Min = 0;
WhlMtnCntrls_SVMaxThrsRst.Description = 'Description goes here';
WhlMtnCntrls_SVMaxThrsRst.Unit = 'N/A';
WhlMtnCntrls_SVMaxThrsRst.DataType = 'double';

assignin('base','WhlMtnCntrls_SVMaxThrsRst',WhlMtnCntrls_SVMaxThrsRst);

%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Dynamic max damping force determination
%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

WhlMtnCntrls_EnableADCntrl = Simulink.Parameter;
WhlMtnCntrls_EnableADCntrl.Value = 1;
WhlMtnCntrls_EnableADCntrl.Max = 1;
WhlMtnCntrls_EnableADCntrl.Min = 0;
WhlMtnCntrls_EnableADCntrl.Description = 'Description goes here';
WhlMtnCntrls_EnableADCntrl.Unit = 'N/A';
WhlMtnCntrls_EnableADCntrl.DataType = 'boolean';

assignin('base','WhlMtnCntrls_EnableADCntrl',WhlMtnCntrls_EnableADCntrl);

WhlMtnCntrls_AbsMaxLimitforActuator = Simulink.Parameter;
WhlMtnCntrls_AbsMaxLimitforActuator.Value = 50000;
WhlMtnCntrls_AbsMaxLimitforActuator.Max = 100000;
WhlMtnCntrls_AbsMaxLimitforActuator.Min = 0;
WhlMtnCntrls_AbsMaxLimitforActuator.Description = 'Description goes here';
WhlMtnCntrls_AbsMaxLimitforActuator.Unit = 'N';
WhlMtnCntrls_AbsMaxLimitforActuator.DataType = 'double';

assignin('base','WhlMtnCntrls_AbsMaxLimitforActuator',WhlMtnCntrls_AbsMaxLimitforActuator);

WhlMtnCntrls_CmpDmpMaxLmtGnOffRd = Simulink.Parameter;
WhlMtnCntrls_CmpDmpMaxLmtGnOffRd.Value = 0.75;
WhlMtnCntrls_CmpDmpMaxLmtGnOffRd.Max = 2;
WhlMtnCntrls_CmpDmpMaxLmtGnOffRd.Min = 0;
WhlMtnCntrls_CmpDmpMaxLmtGnOffRd.Description = 'Description goes here';
WhlMtnCntrls_CmpDmpMaxLmtGnOffRd.Unit = 'N/A';
WhlMtnCntrls_CmpDmpMaxLmtGnOffRd.DataType = 'double';

assignin('base','WhlMtnCntrls_CmpDmpMaxLmtGnOffRd',WhlMtnCntrls_CmpDmpMaxLmtGnOffRd);

WhlMtnCntrls_RebDmpMaxLmtGnOffRd = Simulink.Parameter;
WhlMtnCntrls_RebDmpMaxLmtGnOffRd.Value = 0.5;
WhlMtnCntrls_RebDmpMaxLmtGnOffRd.Max = 2;
WhlMtnCntrls_RebDmpMaxLmtGnOffRd.Min = 0;
WhlMtnCntrls_RebDmpMaxLmtGnOffRd.Description = 'Description goes here';
WhlMtnCntrls_RebDmpMaxLmtGnOffRd.Unit = 'N/A';
WhlMtnCntrls_RebDmpMaxLmtGnOffRd.DataType = 'double';

assignin('base','WhlMtnCntrls_RebDmpMaxLmtGnOffRd',WhlMtnCntrls_RebDmpMaxLmtGnOffRd);

WhlMtnCntrls_CmpDmpMaxLmtGnOnRd = Simulink.Parameter;
WhlMtnCntrls_CmpDmpMaxLmtGnOnRd.Value = 1.0;
WhlMtnCntrls_CmpDmpMaxLmtGnOnRd.Max = 2;
WhlMtnCntrls_CmpDmpMaxLmtGnOnRd.Min = 0;
WhlMtnCntrls_CmpDmpMaxLmtGnOnRd.Description = 'Description goes here';
WhlMtnCntrls_CmpDmpMaxLmtGnOnRd.Unit = 'N/A';
WhlMtnCntrls_CmpDmpMaxLmtGnOnRd.DataType = 'double';

assignin('base','WhlMtnCntrls_CmpDmpMaxLmtGnOnRd',WhlMtnCntrls_CmpDmpMaxLmtGnOnRd);

WhlMtnCntrls_RebDmpMaxLmtGnOnRd = Simulink.Parameter;
WhlMtnCntrls_RebDmpMaxLmtGnOnRd.Value = 0.75;
WhlMtnCntrls_RebDmpMaxLmtGnOnRd.Max = 2;
WhlMtnCntrls_RebDmpMaxLmtGnOnRd.Min = 0;
WhlMtnCntrls_RebDmpMaxLmtGnOnRd.Description = 'Description goes here';
WhlMtnCntrls_RebDmpMaxLmtGnOnRd.Unit = 'N/A';
WhlMtnCntrls_RebDmpMaxLmtGnOnRd.DataType = 'double';

assignin('base','WhlMtnCntrls_RebDmpMaxLmtGnOnRd',WhlMtnCntrls_RebDmpMaxLmtGnOnRd);

WhlMtnCntrls_CmpDmpMaxLmtGnRst = Simulink.Parameter;
WhlMtnCntrls_CmpDmpMaxLmtGnRst.Value = 1.25;
WhlMtnCntrls_CmpDmpMaxLmtGnRst.Max = 2;
WhlMtnCntrls_CmpDmpMaxLmtGnRst.Min = 0;
WhlMtnCntrls_CmpDmpMaxLmtGnRst.Description = 'Description goes here';
WhlMtnCntrls_CmpDmpMaxLmtGnRst.Unit = 'N/A';
WhlMtnCntrls_CmpDmpMaxLmtGnRst.DataType = 'double';

assignin('base','WhlMtnCntrls_CmpDmpMaxLmtGnRst',WhlMtnCntrls_CmpDmpMaxLmtGnRst);

WhlMtnCntrls_RebDmpMaxLmtGnRst = Simulink.Parameter;
WhlMtnCntrls_RebDmpMaxLmtGnRst.Value = 1.0;
WhlMtnCntrls_RebDmpMaxLmtGnRst.Max = 2;
WhlMtnCntrls_RebDmpMaxLmtGnRst.Min = 0;
WhlMtnCntrls_RebDmpMaxLmtGnRst.Description = 'Description goes here';
WhlMtnCntrls_RebDmpMaxLmtGnRst.Unit = 'N/A';
WhlMtnCntrls_RebDmpMaxLmtGnRst.DataType = 'double';

assignin('base','WhlMtnCntrls_RebDmpMaxLmtGnRst',WhlMtnCntrls_RebDmpMaxLmtGnRst);


return

% 
%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% AD_LQR.m ends here

