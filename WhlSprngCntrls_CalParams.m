function [] = WhlSprngCntrls_CalParams(Ka1,Ka2,Ka3,VehFlag)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filename : WhlSprngCntrls_CalParams
%
% Created : Sunday 09 April 2017 - 9:26:07 PM (Eastern Daylight Time)
%
% Updated on: Tuesday 23 May 2017 - 8:43:27 PM (Eastern Daylight Time)
%
% Created By : Juchirl Park 
%
% Description : Adaptive Damping tuning variables reside here. The control
% gains are calculated based on Linear Quadratic Regulator and state space
% model of the vehicle with 7-degree of freedom. These gains can be
% displayed on the MATLAB command window or stored in the excel file user
% needs to be define. It have 4 input variable with no return variable.
% Usually, this program will be called by "Driver_Controls_Main".
%
% [] = WhlyMtnCntrls_CalParams(Ka1,Ka2,Ka3,VehFlag)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MaxRatio = 1.5;

WhlSprngCntrls_MaxCntrlFrc = Simulink.Parameter;
WhlSprngCntrls_MaxCntrlFrc.Value = 75000;
WhlSprngCntrls_MaxCntrlFrc.Max = 100000;
WhlSprngCntrls_MaxCntrlFrc.Min = 0;
WhlSprngCntrls_MaxCntrlFrc.Description = 'Maximum control force for wheel motion controls';
WhlSprngCntrls_MaxCntrlFrc.Unit = 'N';
WhlSprngCntrls_MaxCntrlFrc.DataType = 'double';

assignin('base','WhlSprngCntrls_MaxCntrlFrc',WhlSprngCntrls_MaxCntrlFrc);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wheel Control Gains for Curb Mode
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
WhlSprngCntrls_HldFltrCoef = Simulink.Parameter;
WhlSprngCntrls_HldFltrCoef.Value = 5;
WhlSprngCntrls_HldFltrCoef.Max = 80;
WhlSprngCntrls_HldFltrCoef.Min = 0;
WhlSprngCntrls_HldFltrCoef.Description = 'Description goes here';
WhlSprngCntrls_HldFltrCoef.Unit = 'Hz';
WhlSprngCntrls_HldFltrCoef.DataType = 'double';

assignin('base','WhlSprngCntrls_HldFltrCoef',WhlSprngCntrls_HldFltrCoef);

WhlSprngCntrls_EnableWhlCntrls = Simulink.Parameter;
WhlSprngCntrls_EnableWhlCntrls.Value = 1;
WhlSprngCntrls_EnableWhlCntrls.Max = 1;
WhlSprngCntrls_EnableWhlCntrls.Min = 0;
WhlSprngCntrls_EnableWhlCntrls.Description = 'Description goes here';
WhlSprngCntrls_EnableWhlCntrls.Unit = 'N/A';
WhlSprngCntrls_EnableWhlCntrls.DataType = 'boolean';

assignin('base','WhlSprngCntrls_EnableWhlCntrls',WhlSprngCntrls_EnableWhlCntrls);

WhlSprngCntrls_SuspRdHghtGain = Simulink.Parameter;
WhlSprngCntrls_SuspRdHghtGain.Value = [25000 25000 25000 25000];
WhlSprngCntrls_SuspRdHghtGain.Max = 50000;
WhlSprngCntrls_SuspRdHghtGain.Min = -50000;
WhlSprngCntrls_SuspRdHghtGain.Description = 'Suspension ride height gain for spring force';
WhlSprngCntrls_SuspRdHghtGain.Unit = 'N/A';
WhlSprngCntrls_SuspRdHghtGain.DataType = 'double';

assignin('base','WhlSprngCntrls_SuspRdHghtGain',WhlSprngCntrls_SuspRdHghtGain);

Res = 100;
FLGain = round(Ka1(1,4)/Res)*Res;
FRGain = round(Ka1(2,5)/Res)*Res;
RLGain = round(Ka1(3,6)/Res)*Res;
RRGain = round(Ka1(4,7)/Res)*Res;

WhlSprngCntrls_GnCrbFL = Simulink.Parameter;
WhlSprngCntrls_GnCrbFL.Value = FLGain;
WhlSprngCntrls_GnCrbFL.Max = 0;
WhlSprngCntrls_GnCrbFL.Min = round((min(FLGain)*MaxRatio)./Res).*Res+Res;
WhlSprngCntrls_GnCrbFL.Description = 'Description goes here';
WhlSprngCntrls_GnCrbFL.Unit = 'N/A';
WhlSprngCntrls_GnCrbFL.DataType = 'double';

assignin('base','WhlSprngCntrls_GnCrbFL',WhlSprngCntrls_GnCrbFL);

WhlSprngCntrls_GnCrbFR = Simulink.Parameter;
WhlSprngCntrls_GnCrbFR.Value = FRGain;
WhlSprngCntrls_GnCrbFR.Max = 0;
WhlSprngCntrls_GnCrbFR.Min = round((min(FRGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnCrbFR.Description = 'Description goes here';
WhlSprngCntrls_GnCrbFR.Unit = 'N/A';
WhlSprngCntrls_GnCrbFR.DataType = 'double';

assignin('base','WhlSprngCntrls_GnCrbFR',WhlSprngCntrls_GnCrbFR);

WhlSprngCntrls_GnCrbRL = Simulink.Parameter;
WhlSprngCntrls_GnCrbRL.Value = RLGain;
WhlSprngCntrls_GnCrbRL.Max = 0;
WhlSprngCntrls_GnCrbRL.Min = round((min(RLGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnCrbRL.Description = 'Description goes here';
WhlSprngCntrls_GnCrbRL.Unit = 'N/A';
WhlSprngCntrls_GnCrbRL.DataType = 'double';

assignin('base','WhlSprngCntrls_GnCrbRL',WhlSprngCntrls_GnCrbRL);

WhlSprngCntrls_GnCrbRR = Simulink.Parameter;
WhlSprngCntrls_GnCrbRR.Value = RRGain;
WhlSprngCntrls_GnCrbRR.Max = 0;
WhlSprngCntrls_GnCrbRR.Min = round((min(RRGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnCrbRR.Description = 'Description goes here';
WhlSprngCntrls_GnCrbRR.Unit = 'N/A';
WhlSprngCntrls_GnCrbRR.DataType = 'double';

assignin('base','WhlSprngCntrls_GnCrbRR',WhlSprngCntrls_GnCrbRR);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wheel Control for GVW Mode
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Res = 100;
FLGain = round(Ka2(1,4)/Res)*Res;
FRGain = round(Ka2(2,5)/Res)*Res;
RLGain = round(Ka2(3,6)/Res)*Res;
RRGain = round(Ka2(4,7)/Res)*Res;

WhlSprngCntrls_GnGVWFL = Simulink.Parameter;
WhlSprngCntrls_GnGVWFL.Value = FLGain;
WhlSprngCntrls_GnGVWFL.Max = 0;
WhlSprngCntrls_GnGVWFL.Min = round((min(FLGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnGVWFL.Description = 'Description goes here';
WhlSprngCntrls_GnGVWFL.Unit = 'N/A';
WhlSprngCntrls_GnGVWFL.DataType = 'double';

assignin('base','WhlSprngCntrls_GnGVWFL',WhlSprngCntrls_GnGVWFL);

WhlSprngCntrls_GnGVWFR = Simulink.Parameter;
WhlSprngCntrls_GnGVWFR.Value = FRGain;
WhlSprngCntrls_GnGVWFR.Max = 0;
WhlSprngCntrls_GnGVWFL.Min = round((min(FRGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnGVWFR.Description = 'Description goes here';
WhlSprngCntrls_GnGVWFR.Unit = 'N/A';
WhlSprngCntrls_GnGVWFR.DataType = 'double';

assignin('base','WhlSprngCntrls_GnGVWFR',WhlSprngCntrls_GnGVWFR);

WhlSprngCntrls_GnGVWRL = Simulink.Parameter;
WhlSprngCntrls_GnGVWRL.Value = RLGain;
WhlSprngCntrls_GnGVWRL.Max = 0;
WhlSprngCntrls_GnGVWRL.Min = round((min(RLGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnGVWRL.Description = 'Description goes here';
WhlSprngCntrls_GnGVWRL.Unit = 'N/A';
WhlSprngCntrls_GnGVWRL.DataType = 'double';

assignin('base','WhlSprngCntrls_GnGVWRL',WhlSprngCntrls_GnGVWRL);

WhlSprngCntrls_GnGVWRR = Simulink.Parameter;
WhlSprngCntrls_GnGVWRR.Value = RRGain;
WhlSprngCntrls_GnGVWRR.Max = 0;
WhlSprngCntrls_GnGVWRR.Min = round((min(RRGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnGVWRR.Description = 'Description goes here';
WhlSprngCntrls_GnGVWRR.Unit = 'N/A';
WhlSprngCntrls_GnGVWRR.DataType = 'double';

assignin('base','WhlSprngCntrls_GnGVWRR',WhlSprngCntrls_GnGVWRR);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wheel Control for Other Mode
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Res = 100;
FLGain = round(Ka3(1,4)/Res)*Res;
FRGain = round(Ka3(2,5)/Res)*Res;
RLGain = round(Ka3(3,6)/Res)*Res;
RRGain = round(Ka3(4,7)/Res)*Res;

WhlSprngCntrls_GnRstFL = Simulink.Parameter;
WhlSprngCntrls_GnRstFL.Value = FLGain;
WhlSprngCntrls_GnRstFL.Max = 0;
WhlSprngCntrls_GnRstFL.Min = round((min(FLGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnRstFL.Description = 'Description goes here';
WhlSprngCntrls_GnRstFL.Unit = 'N/A';
WhlSprngCntrls_GnRstFL.DataType = 'double';

assignin('base','WhlSprngCntrls_GnRstFL',WhlSprngCntrls_GnRstFL);

WhlSprngCntrls_GnRstFR = Simulink.Parameter;
WhlSprngCntrls_GnRstFR.Value = FRGain;
WhlSprngCntrls_GnRstFR.Max = 0;
WhlSprngCntrls_GnRstFR.Min = round((min(FRGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnRstFR.Description = 'Description goes here';
WhlSprngCntrls_GnRstFR.Unit = 'N/A';
WhlSprngCntrls_GnRstFR.DataType = 'double';

assignin('base','WhlSprngCntrls_GnRstFR',WhlSprngCntrls_GnRstFR);

WhlSprngCntrls_GnRstRL = Simulink.Parameter;
WhlSprngCntrls_GnRstRL.Value = RLGain;
WhlSprngCntrls_GnRstRL.Max = 0;
WhlSprngCntrls_GnRstRL.Min = round((min(RLGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnRstRL.Description = 'Description goes here';
WhlSprngCntrls_GnRstRL.Unit = 'N/A';
WhlSprngCntrls_GnRstRL.DataType = 'double';

assignin('base','WhlSprngCntrls_GnRstRL',WhlSprngCntrls_GnRstRL);

WhlSprngCntrls_GnRstRR = Simulink.Parameter;
WhlSprngCntrls_GnRstRR.Value = RRGain;
WhlSprngCntrls_GnRstRR.Max = 0;
WhlSprngCntrls_GnRstRR.Min = round((min(RRGain)*MaxRatio)./Res).*Res+Res; 
WhlSprngCntrls_GnRstRR.Description = 'Description goes here';
WhlSprngCntrls_GnRstRR.Unit = 'N/A';
WhlSprngCntrls_GnRstRR.DataType = 'double';

assignin('base','WhlSprngCntrls_GnRstRR',WhlSprngCntrls_GnRstRR);

return

% 
%%% =========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% WhlSprngCntrls_CalParams.m ends here

